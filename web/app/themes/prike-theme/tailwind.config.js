const globalStyles = ({ addBase, config }) => {
  addBase({
    a: {
      color: config('theme.colors.red'),
      textDecoration: 'none',
      borderBottom: '1px solid transparent',
      transition: '0.2s ease',
      listStyleType: 'none',
    },
    'a:hover': {
      borderColor: config('theme.borderColor.primary'),
    },
  });
};

module.exports = {
  purge: {
    content: ['./app/**/*.php', './resources/**/*.{php,vue,js}'],
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    backgroundImage: (theme) => ({
      'expert-bg-gray': "url('../images/backgrounds/expert-bg-gray.svg')",
      'expert-bg-gray-mob': "url('../images/backgrounds/expert-bg-gray-mob.svg')",
      'expert-bg-pink-mob': "url('../images/backgrounds/expert-bg-pink-mob.svg')",
    }),
    container: {
      center: true,
      padding: '1rem',
    },
    flexGrow: {
      0.5: 0.5,
      1: 1,
    },
    extend: {
      colors: {
        white: '#ffffff',
        'gray-1': '#171617',
        'gray-2': '#d2d2d2',
        line: '#cdcdcc',
        'line-2': '#363436',
        'secondary-1': '#D5C1A5',
        'secondary-2': '#e6e0d8',
        red: '#9e1635',
        green: { DEFAULT: '#008E93', light: '#D9F4E5' },
        title: '#171617',
        regular: '#474548',
        'regular-2': '#797979',
        'regular-3': '#bdbdbd',
        'regular-4': '#505050',
        bg1: '#fbf8f4',
        bg2: 'white',
        bg3: '#f4f4f4',
        'dark-theme-2': '#212021',
        gold: '#E4BD71',
        'gold-2': '#c99238',
        black: '#000000',
        'gray-special': '#282828',
        'temp-frontpage': '#575151',
        pink: '#FAD5CF',
        'beige-1': '#e6e0d8',
        'beige-2': '#eee9e5',
        'beige-3': '#dadada',
      },
      fontFamily: {
        playfair: ['Playfair Display', 'serif'],
        poppins: ['Poppins', 'sans-serif'],
        epilogue: ['Epilogue', 'sans-serif'],
        roboto: ['Roboto', 'sans-serif'],
        federo: ['Federo', 'sans-serif'],
      },
      fontSize: {
        xxs: '0.75rem', //12px
        xs: '0.8125rem', //13px
        sm: '0.875rem', //14px
        smm: '0.9375rem', //15px
        base: '1rem', //16px
        'base-plus': '1.0625rem', //17px
        lg: '1.125rem', //18px
        xl: '1.25rem', //20px
        '2xl': '1.375rem',
        '2.25xl': '1.5rem', // 24px
        '2.5xl': '1.75rem', //28px
        '3xl': '2rem', //32px
        '3.5xl': '2.25rem', //36px
        '4xl': '2.5rem', //40px
      },
      gridTemplateColumns: {
        'header-responsive': 'max-content max-content auto max-content max-content',
      },
      gridColumnStart: {
        'neg-1': '-1',
        'neg-4': '-4',
      },
      height: {
        '3px': '0.1875rem', // 3px
        4.25: '1.25rem', //20px
        6.25: '1.5625rem', // 25px
        7: '1.75rem', // 28 px
        34: '8.5rem', //136px
        92.5: '23.125rem', // 370px
        112.5: '28.125rem', // 450px
        122.5: '30.625rem', // 490px
        132: '33rem', // 528px
        145: '36.25rem', // 580px
        157.5: '39.375rem', // 630px
        167.5: '41.875rem', // 670px
        205: '51.25rem', // 820 px,
        180: '45rem', // 720px
        // Heights based on images
        'product-card-img': '280px',
      },
      lineHeight: {
        xs: '0.875rem',
        1: '1rem',
        2: '1.0625rem',
        3: '1.125rem',
        4: '1.25rem',
        5: '1.375rem',
        6: '1.4375rem',
        7: '1.5rem',
        8: '1.625rem',
        9: '1.75rem',
        10: '2.25rem',
        11: '2.75rem',
        12: '3rem',
        'x1.1': '1.1',
        'x1.2': '1.2',
        'x1.4': '1.4',
        'x1.6': '1.6',
      },
      maxWidth: {
        '1/4': '25%',
        '1/2': '50%',
      },
      ringOffsetWidth: {
        4.8: '4.8px',
      },
      shadows: {
        outline: '0 0 0 3px rgba(82,93,220,0.3)',
      },
      scale: {
        120: '1.2',
      },
      spacing: {
        1.75: '0.4375rem', // 7px
        2.25: '0.5625rem', // 9px
        2.5: '0.625rem', // 10px
        3.25: '0.8125rem', // 13px
        3.75: '0.9375rem', //15px
        4.25: '1.0625rem', //17px
        4.5: '1.125rem', //18px
        4.75: '1.1875rem', //19px
        5.5: '1.375rem', //22px
        5.75: '1.4375rem', //23px
        6.5: '1.625rem', // 26px
        8.75: '2.1875rem', //35px
        9.5: '2.375rem', // 38px
        13: '3.25rem', // 52px
        14: '3.5rem', //56px
        18: '4.5rem', // 72px
        19: '4.75rem', //76px
        21: '5.25rem', //84px
        22: '5.5rem', // 88px
        23: '5.75rem', // 92px
        25: '6.25rem', // 100px
        35: '8.75rem', //140px
        50: '12.5rem', // 200px
        75: '18.75rem', // 300px
        90: '22.5rem', // 360px
        132: '33rem', //528px
        155: '38.75rem', // 620px
        '3px': '3px',
        '10px': '10px',
        '50px': '50px',
        '75px': '75px',
        '1/3': '33.33333%',
        '1/2': '50%',
        '2/12': '16.666667%',
      },
      borderRadius: {
        100: '100px',
      },
      borderWidth: {
        3: '3px',
      },
      dropShadow: {
        mdx: '0px 4px 4px rgba(0, 0, 0, 0.25)',
        type1: '0px -4px 12px rgba(0, 0, 0, 0.08)',
      },
    },
  },
  variants: {
    extend: {
      display: ['focus', 'group-hover'],
      textColor: ['hover', 'active', 'disabled', 'checked', 'focus'],
      borderWidth: ['hover', 'active', 'disabled', 'first', 'last'],
      borderColor: ['hover', 'disabled', 'checked', 'focus', 'first', 'last'],
      borderOpacity: ['first', 'last'],
      backgroundColor: ['hover', 'active', 'disabled', 'checked', 'focus', 'first'],
      transitionDuration: ['hover', 'focus', 'active'],
      translate: ['active', 'checked'],
      outline: ['hover'],
      ringWidth: ['hover', 'focus', 'checked'],
      ringColor: ['hover', 'active', 'checked', 'focus'],
      ringOffsetWidth: ['hover', 'active', 'checked'],
      ringOffsetColor: ['hover', 'active', 'checked'],
      scale: ['group-hover'],
      transform: ['group-hover'],
    },
  },
  plugins: [require('@tailwindcss/typography'), require('@tailwindcss/forms'), globalStyles],
};
