<article @php(post_class())>
  <div>
    <?php
    $categories = get_the_terms( get_the_ID(), 'category' );
    ?>
    <div class="product_thumb rounded-md border relative">
      <img style="height: 100%; width: 100%; margin: auto; object-fit: cover;" src="<?php echo the_post_thumbnail_url(); ?>">
      <div class="absolute bottom-5 left-5 rounded-full bg-gray-1 text-white text-md font-semibold py-2 px-3 block">Red wine</div>
    </div>
    <div class="product-data flex justify-between mt-2">
      <h3><a class="font-poppins font-semibold text-lg leading-4 text-gray-1" href="<?php the_permalink(); ?>"> <?php the_title(); ?> </a></h3>
      <div class="font-poppins font-medium text-sm leading-4 text-regular-2 mt-2">
        2 min / Easy
      </div>
    </div>
    <div>
      <ul class="mt-2 flex flex-wrap">
        <?php 
        if($categories){
          foreach ($categories as $category) { ?>
            <li class="mr-2 my-1"><a href="#" class="border border-regular-2 rounded-full text-regular-2 text-sm font-medium py-0.5 px-3 block hover:bg-gray-1 hover:border-gray-1 hover:text-white"><?php echo $category->name; ?></a></li>
          <?php }  
        } ?>
      </ul>
    </div>
  </div>
</article>