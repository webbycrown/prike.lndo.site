{{--
  Template Name: UI Showcase
--}}

@extends('layouts.app')

@section('content')
<div class="container">
  <div class="py-4">
    <h1>H1 text</h1>
  </div>
  <div class="py-4">
    <h2>H2 text</h2>
  </div>
  <div class="py-4">
    <h3>H3 text</h3>
  </div>
  <div class="py-4">
    <h4>H4 text</h4>
  </div>
  <div class="py-4">
    <h5>H5 text</h5>
  </div>
  <div class="py-4"><a href="#">Link text</a></div>



  <x-general.alert>Test</x-general.alert>

  <div class="grid grid-cols-1 gap-y-5">
    <div class="flex flex-wrap py-4">
      <div class="w-1/2">
        <x-controls.button>Default button</x-controls.button>
      </div>
      <div class="w-1/2">
        <x-controls.button disabled>Default - disabled</x-controls.button>
      </div>
    </div>
    <div class="flex flex-wrap py-4">
      <div class="w-1/2">
        <x-controls.button variant="black">Default button</x-controls.button>
      </div>
      <div class="w-1/2">
        <x-controls.button variant="black" disabled>Default - disabled</x-controls.button>
      </div>
    </div>
    <div class="flex flex-wrap py-4">
      <div class="w-1/2">
        <x-controls.button variant="gold">Default button</x-controls.button>
      </div>
      <div class="w-1/2">
        <x-controls.button variant="gold" disabled>Default - disabled</x-controls.button>
      </div>
    </div>

    <div class="flex flex-wrap py-4">
      <div class="w-1/2">
        <x-controls.button variant="gold" narrow>Default button</x-controls.button>
      </div>
      <div class="w-1/2">
        <x-controls.button variant="gold" disabled narrow>Default - disabled</x-controls.button>
      </div>
    </div>


    <div class="flex flex-wrap space-x-4">
      <x-controls.button-circle>T</x-controls.button-circle>
      <x-controls.button-circle>T</x-controls.button-circle>
    </div>
    <div class="flex flex-wrap space-x-4">
      <x-controls.checkbox />
      <x-controls.toggler />
      <x-controls.radio />
    </div>
    <div class="grid grid-cols-1 md:grid-cols-2 gap-4">
      <x-controls.input-text />
      <x-controls.input-text placeholder="test" label="label" />
      <x-controls.input-text placeholder="Placeholder multiple" description="test description" label="label" />
      <x-controls.input-text placeholder="Disabled" description="Disabled" label="label" disabled />
      <x-controls.input-text placeholder="Alert" description="Alert" label="label" variant="alert" />
      <x-controls.input-text placeholder="Alert" description="Alert" label="label" variant="alert" type="password" />
    </div>
    <div class="grid grid-cols-1 md:grid-cols-3 gap-4">
      <x-controls.custom-select />
    </div>
    <x-elements.tag fill="secondary-1" text="black">NEW</x-elements.tag>
    <div class="grid grid-cols-2 gap-2">
      <x-elements.tag fill="secondary-1" border="secondary-1" text="black">NEW</x-elements.tag>
      <x-elements.tag>NEW</x-elements.tag>
    </div>

    <div class="grid grid-cols-1 md:grid-cols-2 gap-4">
      <div class="gradient-gold h-10 w-full"></div>
      <div class="gradient-silver h-10 w-full"></div>
      <div class="gradient-green h-10 w-full"></div>
      <div class="gradient-pink h-10 w-full"></div>
    </div>
    <>
      <x-elements.notification>Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro cumque, eligendi natus </x-elements.notification>
  </div>
  <div class="grid grid-cols-12 py-20">
    <div class="col-span-4">
      <x-product.product-card />
    </div>
    <div class="col-span-4" outOfStock>
      <x-product.product-card />
    </div>
  </div>
  <div class="h-80">
    Empty
  </div>
</div>


</div>
@endsection