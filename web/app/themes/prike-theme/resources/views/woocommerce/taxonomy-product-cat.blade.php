@extends('layouts.app')
@section('content')
  @php
    $category_slides = ['/images/mock-data/hero-slider-image.png', '/images/mock-data/hero-slider-image.png', '/images/mock-data/hero-slider-image.png', '/images/mock-data/hero-slider-image.png'];
    // do_action('woocommerce_archive_description')
  @endphp
  {{-- @if ($category_header['hero'])
  <x-category.jumbotron imgSrc="{{ $category_header['imgSrc'] }}" title="{{ $category_header['title'] }}" 
                        imgAlt="{{ $category_header['imgAlt'] }}" description="{{ $category_header['description'] }}" />
  @endif --}}

  <x-category-slider.category-slider slides="$category_slides" />
  <x-category.filters />

  
  <div class="section container pt-3">
    @php
      do_action('woocommerce_before_main_content');
    @endphp
    
    <div class="hidden">
      <x-category.form-ordering />
    </div>
    
  
    {{-- <header class="woocommerce-products-header">
      @if (apply_filters('woocommerce_show_page_title', true))
        <h1 class="woocommerce-products-header__title page-title">{!! woocommerce_page_title(false) !!}</h1>
      @endif
  
      @php
      @endphp
    </header> --}}

  
  
    {{-- @if (woocommerce_product_loop())
      @php
        do_action('woocommerce_before_shop_loop');
        woocommerce_product_loop_start();
      @endphp
  
      @if (wc_get_loop_prop('total'))
        @while (have_posts())
          @php
            the_post();
            do_action('woocommerce_shop_loop');
            wc_get_template_part('content', 'product');
          @endphp
        @endwhile
      @endif
  
      @php
        woocommerce_product_loop_end();
        do_action('woocommerce_after_shop_loop');
      @endphp
    @else
      @php
        do_action('woocommerce_no_products_found')
      @endphp
    @endif --}}
    {{-- category header --}}

    <x-category.tags />
    {{-- category header --}}

    {{-- <div class="flex justify-end">
      <x-category.layout-switch />
    </div> --}}
    <div class="mb-14">
      <ul class="mb-9 flex flex-wrap items-center justify-center  md:justify-between -mx-2.5">
        @for ($i = 0; $i < 12; $i++)
        <li class="w-full max-w-sm sm:w-1/2 flex-grow flex-shrink lg:w-1/3 xl:w-1/4 lg:pt-5 lg:pb-6 py-5 px-2.5">
          <x-product.cat-product />
        </li>    
        @endfor
      </ul>
    </div>
  </div>
  <div class="section container mb-10">
    <x-expert.cat-expert />
  </div>
    
  <div class="section container mb-14">

    <ul class="mb-9 flex flex-wrap items-center justify-center  md:justify-between -mx-2.5">
      @for ($i = 0; $i < 12; $i++)
      <li class="w-full max-w-sm sm:w-1/2 flex-grow flex-shrink lg:w-1/3 xl:w-1/4 lg:pt-5 lg:pb-6 py-5 px-2.5">
        <x-product.cat-product cart tag/>
      </li>    
      @endfor
    </ul>
  </div>

    
  <div class="section container my-16 pt-1">
    <x-category.pagination />
  </div>

    {{-- @php
      do_action('woocommerce_after_main_content');
      do_action('get_sidebar', 'shop');
      do_action('get_footer', 'shop');
    @endphp --}}
  
  <div class="section bg-secondary-2">
    <x-recipes.recipes-slider title="Red Wine Cocktails to Try Now"></x-recipes.recipes-slider>
  </div>
  <x-category.filters-mobile />
@endsection