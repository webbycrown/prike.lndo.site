@extends('layouts.app')

@section('content')
    {{-- @php
    global $product;
    $attachment_ids = $product->get_gallery_attachment_ids();
    do_action('get_header', 'shop');
  @endphp --}}

    {{-- Modify breadcrubms --}}
    @php
    $breadcrumbs_args = [
        'delimiter' => '>',
        'wrap_before' => '<div class="mb-0 text-sm text-regular-2">',
        'wrap_after' => '</div>',
    ];
    @endphp

    {{-- @while (have_posts())
    @php
      the_post();
      wc_get_template_part('content', 'single-product');
    @endphp 
  @endwhile

  @php
    do_action('woocommerce_after_main_content');
    do_action('get_sidebar', 'shop');
    do_action('get_footer', 'shop');
  @endphp --}}
    <div class="bg-bg3">
        <div class="product-breadcrumbs container p-4">
            {{ woocommerce_breadcrumb($breadcrumbs_args) }}
        </div>
    </div>


    <div class="xl:hidden">
        @include('partials.single-product-info-mob')
    </div>
    <div class="hidden xl:block">
        @include('partials.single-product-info')
    </div>
    <div class="bg-secondary-2">
        <x-recipes.recipes-slider>
            <x-recipes.recipes-slide />
            <x-recipes.recipes-slide />
            <x-recipes.recipes-slide />
        </x-recipes.recipes-slider>
    </div>
    </div>


@endsection
