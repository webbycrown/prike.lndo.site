<div class="overflow-hidden bg-bg3">
  <a class="sr-only focus:not-sr-only" href="#main">
    {{ __('Skip to content') }}
  </a>

  @include('partials.header')
  {{-- When neccessary, add prose class back to here --}}
  <main id="main" class="main">@yield('content')</main>

  @hasSection('sidebar')
  <aside class="sidebar">@yield('sidebar')</aside>
  @endif @include('partials.footer')
</div>
