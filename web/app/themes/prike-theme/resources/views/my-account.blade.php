{{--
  Template Name: My Account
--}}
@extends('layouts.app') @section('content')

    <div class="section pb-10 pt-12 max-w-full lg:w-full lg:px-475 relative flex items-start lg:flex-row">
      @include('partials.my-account.user-menu', ['activeMenuItem' => 'box'])
      @include('partials.my-account.my-orders')
    </div>

{{--@section('content')--}}
{{--  @include('partials.page-header')--}}
{{--  @include('partials.content-page')--}}
@endsection
