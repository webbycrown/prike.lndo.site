{{--
  Template Name: Brands
--}}

@extends('layouts.app') @section('content')
<div class="container pt-3 xl:pt-23 xl:pb-13">
  <h1 class="mb-1">Brands</h1>
  <p class="text-regular font-normal">Epic brands you know and love.</p>
</div>

<div id="filters-name" class="container pt-3 pr-0 overflow-x-visible">
  <p class="caption text-regular mb-4">Name</p>
  <div id="name-tags" class="flex space-x-3.25 mb-6 overflow-x-scroll">
    <x-brands.filter-tag active>Favorites</x-brands.filter-tag>
    <x-brands.filter-tag>A</x-brands.filter-tag>
    <x-brands.filter-tag>B</x-brands.filter-tag>
    <x-brands.filter-tag>C</x-brands.filter-tag>
    <x-brands.filter-tag>D</x-brands.filter-tag>
    <x-brands.filter-tag>E</x-brands.filter-tag>
    <x-brands.filter-tag>F</x-brands.filter-tag>
    <x-brands.filter-tag>G</x-brands.filter-tag>
  </div>
</div>
<div id="filters-category" class="container pt-3 pr-0 overflow-x-visible">
  <p class="caption text-regular mb-4">Name</p>
  <div id="category-tags" class="flex space-x-3.25 mb-6 overflow-x-scroll">
    <x-brands.filter-tag active>All</x-brands.filter-tag>
    <x-brands.filter-tag>Wine</x-brands.filter-tag>
    <x-brands.filter-tag>Cahmpagne</x-brands.filter-tag>
    <x-brands.filter-tag>Whiskey</x-brands.filter-tag>
    <x-brands.filter-tag>Beer</x-brands.filter-tag>
  </div>
</div>
<x-brands.brands-block />
<x-brands.brands-block />
<x-brands.brands-block />
<x-brands.brands-block />
@endsection
