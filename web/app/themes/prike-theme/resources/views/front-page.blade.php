@extends('layouts.app') @section('content')

    <!-- Get data to pass for components -->
    @php
    $hero_slider_slides = ['/images/mock-data/hero-slider-image.png', '/images/mock-data/hero-slider-image.png', '/images/mock-data/hero-slider-image.png', '/images/mock-data/hero-slider-image.png', '/images/mock-data/hero-slider-image.png'];
    @endphp
    <div class="frontpage">
        <div class="section">
            <x-hero-slider.hero-slider :slides="$hero_slider_slides" />
        </div>
        <div id="list-box">
            <x-frontpage.list-box></x-frontpage.list-box>
        </div>
        <div class="section container py-4">
            <x-discount.discount-slider />
        </div>

        <div class="section container">
            <x-frontpage.open-account />
        </div>
        <div class="section categories container pb-10 pt-12 relative">
            <h2 class="text-gray-1 mb-6 font-size-2 lg:w-39.5 font-thin">Avasta Prike suurejooneline joogivalik ning avarda
                maitsemeeli</h2>
            <img class="absolute top-0 lg:-top-16 -right-32 lg:right-0"
                src="@asset('/images/mock-data/category-section-bg.png')" style="z-index: -1;" />
            <div class="categories-container grid grid-cols-1 gap-y-4 lg:gap-5 lg:grid-cols-12 ">
                <x-category.category-tab title="Viski" subtitle="Üle 120 toote" type="whiskey" />
                <x-category.category-tab title="Veinid" subtitle="Üle 120 toote" variant="red" type="whiskey" />
                <x-category.category-tab title="Dzinnid" subtitle="Üle 120 toote" variant="green" type="whiskey" narrow />
                <x-category.category-tab title="Õlu" subtitle="Üle 120 toote" variant="orange" type="whiskey" narrow />
                <x-category.category-tab title="šampanja" subtitle="Üle 120 toote" variant="champagne" type="whiskey"
                    narrow />
            </div>
        </div>
        <div class="section container py-10 relative">
            <x-product.product-slider title="Uued tooted" link="#" />
        </div>
        <div class="section announcement container py-20 relative">
            <x-frontpage.announcement />
        </div>
        <div class="section container py-10 relative">
            <x-product.product-slider title="Uued tooted" link="#" />
        </div>
        <div class="section container py-10 relative">
            <x-product.product-slider title="Trendikad" link="#" />
        </div>
        <div class="section container py-10 relative">
            <x-expert.expert />
        </div>
        <div class="section relative">
            <x-news.news-slider />
        </div>
        <div class="section bg-secondary-2  pl-2 relative">
            <x-recipes.recipes-slider title="Retseptid" />
        </div>
    </div>

@endsection
