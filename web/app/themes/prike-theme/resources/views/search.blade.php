@extends('layouts.app')
@section('content')
<div class="container mt-10">
  @include('partials.page-header')

  @if (! have_posts())
  <x-general.alert type="warning">
    {!! __('Sorry, no results were found.', 'sage') !!}
  </x-general.alert>
  {!! get_search_form(false) !!}
  @endif
  <div class="grid gap-x-4 gap-y-12 mt-10 grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4">
    <?php $count = array(); ?>
    @while(have_posts()) @php(the_post())
    <?php $count[] = get_the_ID(); ?>
    @includeWhen(get_post_type() === 'product', 'partials.content-search-product')
    @endwhile
  </div>
  <section class="hidden my-16 px-8 xl:px-21 pt-10 lg:pt-21 md:flex items-end justify-between" style="background-image:url(@asset('/images/backgrounds/expert-bg-gray.svg'));">
    <div class="max-w-sm mb-21 relative">
      <h2 class="text-3xl lg:text-4xl font-playfair mb-2 font-normal">Drink expert</h2>
      <p class=" text-sm lg:text-lg mb-4">If you have a problem choosing a drink, see sugestion from our expert</p>
      <a href="" class="px-3 lg:px-9 w-max py-3 lg:py-5 block w-full border-2 active:border-0  bg-black text-white border-black hover:text-gold active:text-white disabled:bg-gray-2 disabled:text-white disabled:border-gray-2 rounded transform transition-all duration-300 text-sm lg:text-base font-poppins font-semibold font-title leading-none">View Suggestions</a>
      <ul class="mt-4 lg:mt-9 flex flex-wrap">
        <li class="mr-2 my-1 lg:mr-4 lg:my-2"><a href="#" class="border-2 border-regular-2 rounded-full text-regular-2 text-sm font-medium py-0.5 px-3 block hover:bg-gray-1 hover:border-gray-1 hover:text-white">Wine</a></li>
        <li class="mr-2 my-1 lg:mr-4 lg:my-2"><a href="#" class="border-2 border-regular-2 rounded-full text-regular-2 text-sm font-medium py-0.5 px-3 block hover:bg-gray-1 hover:border-gray-1 hover:text-white">Cocktails</a></li>
        <li class="mr-2 my-1 lg:mr-4 lg:my-2"><a href="#" class="border-2 border-regular-2 rounded-full text-regular-2 text-sm font-medium py-0.5 px-3 block hover:bg-gray-1 hover:border-gray-1 hover:text-white">Gin</a></li>
        <li class="mr-2 my-1 lg:mr-4 lg:my-2"><a href="#" class="border-2 border-regular-2 rounded-full text-regular-2 text-sm font-medium py-0.5 px-3 block hover:bg-gray-1 hover:border-gray-1 hover:text-white">Box offer</a></li>
      </ul>
    </div>
    <div class="-mx-20 lg:-mt-21">
      <img src="@asset('/images/backgrounds/Image.png')" alt="">
    </div>
    <div class="max-w-xs mb-56">
      <h2 class="text-sm lg:text-lg font-poppins mb-3 font-semibold">Kristjan Peäske</h2>
      <p class="text-sm lg:text-lg mb-4 text-regular-2">Host of Lore Bistro and Bread Restaurant, Triple Estonian and Double Baltic Master Sommelier</p>
      <ul class="flex flex-wrap">
        <li class="mr-2 my-1 lg:mr-4 lg:my-2"><a href="#"><img src="@asset('/images/backgrounds/logo-icon1.png')" alt=""></a></li>
        <li class="mr-2 my-1 lg:mr-4 lg:my-2"><a href="#"><img src="@asset('/images/backgrounds/logo-icon2.png')" alt=""></a></li>
        <li class="mr-2 my-1 lg:mr-4 lg:my-2"><a href="#"><img src="@asset('/images/backgrounds/logo-icon3.png')" alt=""></a></li>
        <li class="mr-2 my-1 lg:mr-4 lg:my-2"><a href="#"><img src="@asset('/images/backgrounds/logo-icon4.png')" alt=""></a></li>
      </ul>
    </div>
  </section>
  <?php  ?>
  <section>
    <div class="_header mt-10 md:mt-20 flex justify-between pb-3">
      <div class="font-poppins">
        <span class="text-lg text-gray-1 mb-4 font-bold">News</span><span class="result_count bg-secondary-2 ml-2 text-regular-2 rounded-full py-1 px-3">5</span></div>
        <a href="#" target="_blank" class="result_view_all font-medium text-gold-2 text-sm">View All</a>
      </div>
      <div class="grid gap-x-4 gap-y-8 mt-5 md:mt-5 md:gap-y-12 mb-10 md:mb-20 grid-cols-1 sm:grid-cols-2 lg:grid-cols-3">
        @while(have_posts()) @php(the_post()) 
        <?php 
        $categories = get_the_terms( get_the_ID(), 'category' );
        $news = 0;
        $recipies = 0;
        if($categories){
          foreach ($categories as $category) {
            $cat_arr[] = $category->slug;
            if( $category->slug == 'news' ){
              $news = 1;
            }
          }  
        }
      // print_r($cat_arr);
      // @includeWhen(get_post_type() === 'post', 'partials.content-search')
        if( $news == 1 ){ 
          ?>
          <article @php(post_class())>
            <div class="product_thumb"><img style="height: 300px; object-fit: cover;" src="<?php the_post_thumbnail_url(); ?>" class="w-full"></div>
            <div class="mt-4">
              <h2 class="entry-title font-poppins font-semibold text-xl leading-4 text-gray-1">
                <a class="text-gray-1 entry-title font-poppins font-semibold text-xl leading-4 text-gray-1"  href="<?php echo get_permalink() ?>"> <?php the_title(); ?> </a>
              </h2>
              <div class="font-poppins mt-2 font-extralight text-regular text-sm leading-4"><?php echo strip_tags(get_the_excerpt()); ?></div>
            </div>
          </article>
          <?php 
        } ?>
        @endwhile
      </div>
    </section>

    <section>
      <div class="_header flex mt-10 md:mt-20 justify-between pb-3">
        <div class="font-poppins">
          <span class="text-lg text-gray-1 mb-1 font-bold">Recipies</span><span class="result_count bg-secondary-2 ml-2 text-regular-2 rounded-full py-1 px-3">5</span></div>
          <a href="#" target="_blank" class="result_view_all font-medium text-gold-2 text-sm">View All</a>
        </div>
        <div class="grid gap-x-4 gap-y-8 md:gap-y-12 mt-4 md:mt-5 mb-10 md:mb-20 grid-cols-1 sm:grid-cols-2 lg:grid-cols-3">
          @while(have_posts()) @php(the_post()) 
          <?php 
          $categories = get_the_terms( get_the_ID(), 'category' );
          $news = 0;
          $recipies = 0;
          if($categories){
            foreach ($categories as $category) {
              $cat_arr[] = $category->slug;
              if( $category->slug == 'recipies' ){
                $recipies = 1;
              }
            }  
          }
      // print_r($cat_arr);
      // @includeWhen(get_post_type() === 'post', 'partials.content-search')
          if( $recipies == 1 ){ 
            ?>
            <article @php(post_class())>
              <div>
                <?php
                $categories = get_the_terms( get_the_ID(), 'category' );
                $tags = get_the_terms( get_the_ID(), 'post_tag' );
                ?>
                <div class="product_thumb rounded-md border relative">
                  <img style="height: 100%; width: 100%; margin: auto; object-fit: cover;" src="<?php echo the_post_thumbnail_url(); ?>">
                  <?php 
                  if( $tags ){
                    foreach ($tags as $tag) { ?>
                      <div class="absolute bottom-5 left-5 rounded-full bg-gray-1 text-white text-md font-semibold py-2 px-3 block"><?php echo $tag->name; ?></div>
                    <?php }   
                  } ?>

                </div>
                <div class="product-data flex justify-between mt-2">
                  <h3><a class="font-poppins font-semibold text-lg leading-4 text-gray-1" href="<?php the_permalink(); ?>"> <?php the_title(); ?> </a></h3>
                  <div class="font-poppins font-medium text-sm leading-4 text-regular-2 mt-2">
                    2 min / Easy
                  </div>
                </div>
                <div>
                  <ul class="mt-2 flex flex-wrap">
                    <?php 
                    if($categories){
                      foreach ($categories as $category) { ?>
                        <li class="mr-2 my-1"><a href="#" class="border border-regular-2 rounded-full text-regular-2 text-sm font-medium py-0.5 px-3 block hover:bg-gray-1 hover:border-gray-1 hover:text-white"><?php echo $category->name; ?></a></li>
                      <?php }  
                    } ?>
                  </ul>
                </div>
              </div>
            </article>
            <?php 
          } ?>
          @endwhile
        </div>
      </section>
      @endsection