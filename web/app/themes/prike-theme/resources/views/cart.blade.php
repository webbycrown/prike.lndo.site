{{--
  Template Name: Cart 
--}}

@extends('layouts.app')
@section('content')
<div class="woocommerce pt-12">
<div class="py-4">
  <x-cart.progress-mobile />
  <x-cart.progress-desktop />
</div>
  <div class="section container">
    
    <div class="flex justify-between lg:flex-row flex-wrap flex-col lg:space-x-8 items-start">
      <div class="flex-1 w-full">
        <form class="woocommerce-cart-form mb-8" action="#">
          <div class="justify-between items-center mb-6 hidden lg:flex">
            <div class="cart-title flex items-center">
              <h3 class="font-semibold text-gray1 text-xl capitalize">Your cart</h3>
              <span class="p-2 px-3 rounded-full text-title bg-bg3 ml-3 flex items-center leading-none text-sm font-medium">3</span>
            </div>
            <div class="share">
              <a href="#" data-pmodal="#modal-share-cart">
                <img src="@asset('/images/icons/icon-share-gold.svg')" class="inline" alt="">
                <span>Share your cart</span>
              </a>
            </div>
          </div>
          <x-cart.cart-item variant="spirit"></x-cart.cart-item>
          <x-cart.cart-item variant="ticket"></x-cart.cart-item>
          <x-cart.cart-item variant="virtual"></x-cart.cart-item>
          
        </form>
        <x-cart.gift></x-cart.gift>
      </div>
      <div class="lg:w-4/12  w-full" style="min-width:25rem">
        <div class="cart-collaterals border-0 md:border  border-line rounded  pt-5.5 pb-2 px-5 bg-bg1 md:bg-transparent">
          <div class="hidden md:block">

            <h3 class="text-title font-semibold text-xl mb-3">Summary</h3>
            <x-cart.delivery-notice type="guest" />
          </div>
          <div class="subtotal md:pb-5 border-dashed boreder-line md:border-b border-b-0 space-y-1">
              <h5 class=" flex justify-between font-semibold mb-2">
                Subtotal
                <span>218.98<span class="woocommerce-currency">€</span></span>
              </h5>
              <p class="taxes flex justify-between text-sm text-regular">
                <span>Taxes</span>
                <span>12.75<span class="woocommerce-currency">€</span></span>
              </p>
              <p class="flex justify-between text-sm">
                <span class=" text-regular">Delivery</span>
                <span class="text-green uppercase">Free</span>
              </p>
          </div>
          {{-- <div class="total flex justify-between pb-3 pt-5 border-b border-line leading-snug">
            <div class="flex items-center">
              <h5 class="mr-3 font-semibold">Total</h5>
              <a href="#" class="">Add promocode</a> 
            </div>
            <div class="total-price text-title font-semibold text-base">
              <p>357.97<span class="woocommerce-currency">€</span></p>
            </div>

          </div>
          <form action="" class="checkout">
            <div class="flex justify-between py-4">
              <div class="form-field w-full px-1.5">
                <label class="">
                  <x-controls.radio name="type" variant="checkout" />
                  <span class="ml-3">Buy as a guest</span>
                </label>
              </div>
              <div class="form-field w-full pl-1.5">
                <label>
                  <x-controls.radio name="type" variant="checkout" />
                  <span class="ml-3">Buy as a member</span>
                </label>
                <p class="text-green text-sm pl-8">Save -25% or more</p>
              </div>
            </div>
            <x-controls.button variant="gold">Proceed to checkout</x-controls.button>
          </form> --}}
          <x-cart.form />
        </div>
        <div class="delivery-notice flex pb-5 pt-10 px-5">
          <span class="icon-delivery text-gray-1 pr-4 text-5xl -ml-5"></span>
          <div class="notice">
            <h4 class="mb-4 font-semibold text-lg">Free delivery & returns</h4>
            <p class="text-relular text-sm">Congratulations, your order qualifies to free delivery and that is exactly what you get. <a href="#" class="font-medium text-sm">Read more</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="section container mt-6 mb-12">
    <div class="cat-section-heading flex justify-between items-center pb-2">
      <h2 class="text-title font-normal text-2.5xl leading-x1.2">Also buy with these products</h2>
    </div>
    <x-category.carousel id="1"/> 
  </div>
</div>

<x-general.modal id="share-cart">
  <x-cart.form-share-cart />
</x-general.modal>
<x-general.modal id="cart-promocode">
  <x-cart.form-promocode />
</x-general.modal>
{{-- <div class="flex p-10 ">
  <div class="w-132 m-auto">
    <x-cart.form-share-cart />

  </div>
</div> --}}
@endsection