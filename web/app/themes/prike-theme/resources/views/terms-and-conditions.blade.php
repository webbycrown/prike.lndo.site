{{--
  Template Name: Terms and Conditions
--}}

@extends('layouts.app')

@section('content')
    @include('partials.page-header')
    @include('partials.content-page')
@endsection
