<header class="fixed top-0 left-0 w-full z-10 lg:static header bg-gray-1 lg:pt-4">
  <div class="container grid grid-cols-header-responsive lg:grid-cols-12 lg:grid-rows-auto lg:gap-x-4 md:gap-x-6 auto-col-fr">

    <div class="navigation py-3 lg:row-start-1 lg:row-end-2 lg:col-span-12 lg:block lg:w-full flex items-center">
      <button id="responsive-navigation-toggle" class="lg:hidden  focus:outline-none" type="button">
        <img src="@asset('/images/icons/menu-light.svg')" class="md:mr-0 mr-6" />
      </button>
      <div class="nav hidden lg:flex lg:justify-between lg:w-full">
        <x-header-nav />
        <div class="hidden xl:block">

          <x-header-nav-light />
        </div>
      </div>
    </div>

    <div class="search-box py-3 lg:col-start-3 lg:col-end-11 xl:col-end-9 lg:block lg:w-full flex items-center ">
      <button class="lg:hidden focus:outline-none ">
        <img src="@asset('/images/icons/search-light.svg')" />
      </button>
      <div class="hidden lg:block">
        <div class="xl:-ml-8 2xl:-ml-16 2xl:pr-8 lg:-ml-6 max-w-2xl">

          <x-elements.search-input />
        </div>
      </div>
      <div class="wp_ajax_result_mobile rounded-t-2xl lg:hidden top-full transition-all fixed top-0 left-0 bg-secondary-2 pt-3 pb-6 z-10" style="width: 100%;">
        <div class="pb-6 mb-3">
          <div class="bg-gray-1 w-12 h-1.5 block mx-auto cursor-pointer wc-drawer-btn rounded-full"></div>
        </div>

        <div class="overflow-y-scroll overflow-auto px-5" style="height:calc(100vh - 137px)">
          <div class="search-input relative mb-8">
            <span class="absolute inset-y-0 left-0 flex items-center pl-2">
              <button type="submit" class="p-1 focus:outline-none focus:shadow-outline">
                <svg
                fill="none"
                stroke="#bdbdbd"
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                viewBox="0 0 24 24"
                class="w-6 h-6"
                >
                <path d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path>
              </svg>
            </button>
          </span>
          <input class="form-input w-full rounded bg-line-1 border-gray-1 focus:border-gold focus:outline-none pl-12 border-2 py-2.5 text-gray-1" type="text" placeholder="Otsi midagi suurepärast">  
        </div>
        <div class="ajax_result">
          <h1 class="query_title text-lg text-gray-1 mb-4 font-poppins font-bold"> Suggested Queries </h1>
          <ul class="result_taxonomy w-full  whitespace-nowrap overflow-auto items-center pb-2 mb-6">
            <?php
            $args = array(
              'taxonomy'   => "product_cat",
            );
            $product_categories = get_terms($args);
            $i = 0;
      // print_r($product_categories);
            foreach ($product_categories as $product_cat) {
              $i++;
              $category_link = get_category_link($product_cat->term_id);
              ?>
              <li class="result_tax_item mr-2 inline-block"><a class="border-2 whitespace-nowrap border-gray-1 rounded-full text-gray-1 text-sm font-medium py-1.5 px-4 block hover:bg-gray-1 hover:text-white" href="<?php echo $category_link; ?>"><?php echo $product_cat->name; ?></a></li>  
              <?php 
              if( $i == 5 ){
                break;
              }
            } ?>
          </ul>
        </div>
        <div class="top_categories_wrapper">
          <h1 class="query_title text-lg text-gray-1 mb-4 font-poppins font-bold"> TOP kategooriad </h1>
          <ul class="top_categories">
            <?php
            $args = array(
              'taxonomy'   => "product_cat",
              'orderby'       =>  'term_id',
              'order'         =>  'ASC',
            );
            $product_cat = get_terms($args);
            $i = 0;

            foreach ( array_reverse($product_cat) as $product_cat) { 
              $term_thumb_id = get_term_meta( $product_cat->term_id, 'thumbnail_id', true );
              $term_url  = get_term_link($product_cat->term_id, 'product_cat');
              $i++;
              ?>
              <li class="px-5 py-14 mb-4 rounded-md relative" style="background:url('<?php echo wp_get_attachment_url( $term_thumb_id ); ?>'); background-repeat: no-repeat; background-size: cover;"> 
                <a href="<?php echo $term_url; ?>" class="absolute w-full h-full top-0 left-0"></a>
                <h2 class="text-white font-playfair text-3xl font-normal"><?php echo $product_cat->name; ?></h2>
              </li>
              <?php
              if( $i == 6 ){
                break;
              }
            } ?>
          </ul>
        </div>
        <div class="product_result_wrapper hidden">
          <div class="font-poppins product_header">
            <span class="text-lg text-gray-1 mb-4 font-bold">Products</span><span class="result_count bg-white ml-2 text-regular-2 rounded-full py-1 px-3">5</span>
          </div>
          <ul class="product_result grid gap-x-4 gap-y-7 mt-10 grid-cols-2 sm:grid-cols-2 md:grid-cols-3">
          </ul>
        </div>
        <div class="mobile_blogs_result blogs_result hidden mt-10">
          <div class="blogs_result_wrapper"> 
            <?php 
            $p_categories = get_categories( array(
              'orderby' => 'name',
              'order'   => 'ASC',
            ) );
            if( $p_categories ){
             foreach ($p_categories as $p_category) {
              if( $p_category ){
                ?>
                <div class="blogs_item mt-10">
                  <div class="_header flex justify-between pb-3 font-poppins">
                    <h3 class="text-2xl font-playfair text-gray-1 mb-4 font-normal"><?php echo $p_category->name; ?>  </h3>
                    <a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>" target="_blank" class="result_view_all result_view_all font-medium text-gold-2 text-sm">View All</a>
                  </div>
                  <div class="_body grid gap-8 sm:gap-2 font-poppins grid-cols-1 sm:grid-cols-2">
                    <?php 
                    $args = array(
                      'post_type' => 'post',
                      'post_status' => 'publish',
                      'posts_per_page' => 2,
                      'tax_query' => array(
                        array(
                          'taxonomy' => 'category',
                          'field' => 'term_id',
                          'terms' => $p_category->term_id,
                        )
                      )
                    );
                    $the_query = new WP_Query( $args );
                    if ( $the_query->have_posts() ) {
                      while ( $the_query->have_posts() ) { $the_query->the_post();
                        $posttags = get_the_tags();
                        $categories = get_the_terms( get_the_ID(), 'category' );
                        ?>
                        <div class="blogs relative">
                          <a href="<?php the_permalink(); ?>" class="absolute top-0 bottom-0 w-full h-full"></a>
                          <div class="post_thumb relative"><?php the_post_thumbnail(); ?>
                          <?php
                          if ($posttags) {
                            foreach($posttags as $tag) { ?>
                              <span class="post_badge bg-gray-1 absolute bottom-0 left-0 ml-5 mb-5 text-white rounded-full py-1 px-3"><?php echo $tag->name; ?></span>
                            <?php }
                          }
                          ?>

                        </div>
                        <div class="post_main_wrap">
                          <?php 
                          if( $p_category->term_id == 31 ){
                            ?>
                            <div class="font-poppins font-medium text-sm leading-4 text-regular-2 mt-2">
                              2 min / Easy
                            </div>
                            <?php
                          }
                          ?>
                          <div class="post_title mt-1 font-poppins text-xl font-semibold"><?php the_title(); ?></div>
                          <?php 
                          if( $p_category->term_id != 31 ){
                            ?>
                            <div class="font-poppins mt-1 font-extralight text-regular text-sm leading-4"><?php echo strip_tags(get_the_excerpt()); ?></div>
                            <?php  
                          }
                          if( $p_category->term_id == 31 ){
                            ?>
<ul class="mt-2 flex flex-wrap">
                            <?php 
                            if($categories){
                              foreach ($categories as $category) { ?>
                                <li class="mr-2 my-1"><a href="#" class="border border-regular-2 rounded-full text-regular-2 text-sm font-medium py-0.5 px-3 block hover:bg-gray-1 hover:border-gray-1 hover:text-white"><?php echo $category->name; ?></a></li>
                              <?php }  
                            } ?>
                          </ul>
                            <?php
                          }
                          ?>
                          
                        </div>
                        

                      </div>
                    <?php } ?>
                    <?php wp_reset_postdata(); }
                    ?> 
                  </div>
                </div>
                <?php
              }
            } 
          }
          ?>
        </div>
      </div>
      <div class="hidden lg:block">
        <div class="xl:-ml-8 2xl:-ml-16 2xl:pr-8 lg:-ml-6 max-w-2xl">

          <x-elements.search-input />
        </div>
      </div>
    </div>
  </div>

</div>

<div class="logo lg:col-start-1 lg:col-end-3 lg:row-start-auto lg:row-end-1 flex items-center justify-center lg:justify-start lg:p-0 sm:py-1.5">
  <a href="{{ get_home_url() }}" class="">
    <img src="@asset('/images/logo/logo.svg')" alt="Prike homepage" class="lg:h-16 ms:h-14 h-11 block"/>
  </a>
</div>

<div class="user mr-5 lg:hidden flex items-center">
  <button>
    <img src="@asset('/images/icons/user-light.svg')" />
  </button>
</div>

<div class="cart flex py-3 items-center lg:hidden">
  <button>
    <img src="@asset('/images/icons/bag-empty.svg')" />
    <img class="absolute -mt-3 ml-3" src="@asset('/images/icons/bag-mark.svg')" />
  </button>
</div>

<div class="user-area hidden lg:flex items-center justify-end lg:col-start-auto lg:col-end-13 space-x-10">
  <button>
    <span class="icon-acc text-line text-2xl"></span>
  </button>
  <button>
    <span class="icon-wishlist text-line text-2xl"></span>
  </button>
  <button>
    <span class="icon-bag-1 text-line text-2xl"></span>
  </button>
</div>
</div>
</header>
<div class="h-px gradient-gold-black lg:hidden" style=""></div>

@include('partials/responsive-navigation')
