<article @php(post_class())>
  <div class="product_thumb"><img style="height: 300px; object-fit: cover;" src="<?php the_post_thumbnail_url(); ?>" class="w-full"></div>
  <div class="mt-4">
    <h2 class="entry-title font-poppins font-semibold text-xl leading-4 text-gray-1">
      <a class="text-gray-1 entry-title font-poppins font-semibold text-xl leading-4 text-gray-1"  href="<?php echo get_permalink() ?>"> <?php the_title(); ?> </a>
    </h2>
    <div class="font-poppins mt-2 font-extralight text-regular text-sm leading-4"><?php echo strip_tags(get_the_excerpt()); ?></div>
  </div>
</article>