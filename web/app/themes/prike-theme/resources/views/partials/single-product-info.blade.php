<div class="bg-bg3">
    <div class="container flex flex-wrap">
        <div class="xl:w-1/12 pt-3">
            <div id="thumbnails" class="flex flex-col flex-wrap space-y-5  mb-14">
                <x-product.gallery-thumbnail active />
                <x-product.gallery-thumbnail />
                <x-product.gallery-thumbnail />
            </div>
            <div id="brand overflow-visible">
                <img src="@asset('images/mock-data/single-product-brand.png')" alt="">
            </div>
        </div>
        <div class="xl:w-6/12 flex justify-center items-center">
            <img src="@asset('images/mock-data/single-product-image-desktop.png')" alt="" class="h-auto">
        </div>
        <div class="xl:w-5/12 pt-10">
            <div id="title" class="mb-2">
                <h1>Zonin Primitivo Puglia</h1>
            </div>
            <div id="rating" class="mb-3">
                <x-product.rating />
            </div>
            <div id="description" class="mb-4">
                <p>The wine represents the typical Marlborough Sauvignon Blanc: with good acidity, vibrating ... <a
                        href="#">Read more</a></p>
            </div>
            <div>
                <x-product.bottle-sizes />
            </div>
            <div id="add-to-cart" class="mb-10">
                <x-product.add-to-cart />
            </div>
            <div>
                <x-product.delivery-description />
            </div>
        </div>
    </div>
    <x-product.data />

</div>
