<x-product.single-product-slider />

<div id="product-headings" class="container pt-4">
    <p class="caption mb-2 text-regular-2 text-sm font-medium leading-x1.2">Sampanja</p>
    <h1 class="mb-3 font-normal leading-x1.1 text-gray-1">Dom Perignon P2 2002 Kinkekarbiga</h1>
    <x-product.rating />
    <div class="description mb-8">
        <p class="body-2 mb-4">Vein kehtestab end kohe oma rikkaliku, täieliku ja ulatusliku kohaloluga.</p>
        <a href="http://" class="text-gold-2">Loe edasi</a>
    </div>
</div>
<div class="container">
    <x-product.bottle-sizes />

    <div id="notification-discount" class="mb-8">
        <x-general.card bg="green-light">
            <p class="text-regular"><span class="body-2 font-semibold">Jey John,</span> your discount <span
                    class="body-2 font-semibold">-25%</span> will
                be displayed in the shopping cart.</p>
        </x-general.card>
    </div>
    <div id="notification-delivery" noDefaultBackground class="mb-8">
        <x-general.card noDefaultBackground class="gradient-green">
            <div></div>
            <div></div>
            <p class="text-title">You get free shipping today 6.06.2021 and even better you also GET IT TODAY (if
                you order before 14:00 etc)</p>
        </x-general.card>
    </div>
    <div class="mb-16">
        <x-general.card padding="0">

            <x-product.delivery-description />
        </x-general.card>
    </div>
</div>
<div class="container">
    <x-general.accordion title="Toote kirjeldus">
        <x-product.data-description />
    </x-general.accordion>
    <x-general.accordion title="Ajalugu">
        <img src="@asset('/images/mock-data/single-product-history.png')" class="w-full mb-4" />
        <h4 class="mb-4">Siia on vaja kirjutada kena ajaloo kirjeldus mis lööb pahviks</h4>
        <p class="mb-4">Piedmont is one of the most important wine-producing regions of Italy. Its
            geographical situation is not ideal for viticulture and winemaking: almost half of the region is
            inaccessible for vine cultivation, as it is located in the mountains. Most of the vineyards are located on
            the Langhe and Monferrato hills. Piedmont is located at the foot of the mountains: the Alps on one side and
            the Apennines on the other. The climate is characterized by short dry and hot summers, long autumns and
            harsh winters with heavy snowfalls. Another peculiarity of the region is the high humidity, which is
            contributed by the dense fogs with heavy cloudiness. Some winemakers even try to fight the clouds with
            cannons to allow the berries to get their dose of sun and ripen properly.</p>
        <p class="mb-4">Terroir is the main reason for the great variety of wines produced in this area.
            Despite the rather small production volume, there are more DOC or DOCG wines here than in any other region
            of Italy. The production of wine in Piedmont dates back to ancient times and the vineyards of the Italian
            kings and nobles, but in particular the wine making was promoted by the French at the beginning of the 18th
            century.</p>
    </x-general.accordion>
    <x-general.accordion title="Arvustused">
        <x-product.review />
        <x-product.review />
        <x-product.review />
        <div class="review-buttons flex space-x-4 pb-5 border-b border-line">
            <x-controls.button narrow><span class="body-2 text-gray-1 text-sm font-normal">Veel arvustusi</span>
            </x-controls.button>
            <x-controls.button narrow variant="gray-1"><span class="body-2 text-white text-sm font-normal">Kirjuta
                    arvustus</span></x-controls.button>
        </div>
    </x-general.accordion>
</div>
<div class="container">
    <div class="py-16 border-t border-line">
        <div class="px-4 py-6 gradient-silver-card rounded-lg">
            <p class="mb-8">One of the best representatives of sauvignons from New Zealand. Bright and intense
                flavor and aroma will not leave anyone indifferent. The wine you want to come back for. The only
                drawback: it runs out quickly, so I advise you to take at least two bottles at once.</p>
            <div class="flex space-x-2">
                <img src="@asset('/images/mock-data/single-product-expert-avatar.png')" alt="Expert name">
                <div class="flex flex-col justify-between">
                    <h4 class="text-title">Kristjan Peäske</h4>
                    <p class="body-2 text-regular-2">Awarded Drink Expert</p>
                </div>
            </div>
        </div>
    </div>
</div>
