<article @php(post_class())>
  <?php $product = wc_get_product(get_the_ID()); 
  $tag_ids = $product->get_tag_ids();
  $tags = get_the_terms( get_the_ID(), 'product_tag' );
  $tags_arr = array();
  foreach ($tags as $tag) {
    $tags_arr[] = $tag->name;
  }
  $tags_arr = implode('/',$tags_arr);
  $categories = get_the_terms( get_the_ID(), 'product_cat' );
  $cat_arr = array();
  foreach ($categories as $category) {
    $cat_arr[] = $category->name;
  }
  $cat_arr = implode('/',$cat_arr);
  ?>
  <div>
    <div class="product_thumb rounded-md border"><img style="height: 100%; width: 100%; margin: auto; object-fit: cover;" src="<?php echo the_post_thumbnail_url(); ?>"></div>
    <div class="product-data flex justify-between mt-2">
      <div>
        <div class="font-poppins font-medium text-sm text-regular-2"><?php echo $tags_arr; ?></div>
        <a class="font-poppins font-semibold text-lg leading-4 text-gray-1" href="<?php echo get_permalink(); ?>"> <?php the_title(); ?> </a>
        <div class="font-poppins font-medium text-sm text-regular-2"><?php echo $cat_arr; ?></div>
      </div>
      <div class="font-poppins font-medium text-sm leading-4 text-regular-2 mt-2">
        0.75L / 12.0*
      </div>
    </div>
    <div class="font-poppins font-semibold text-lg leading-4 text-gray-1 mt-4"><?php echo number_format($product->get_price(),2).get_woocommerce_currency_symbol(); ?></div>
  </div>
</article>