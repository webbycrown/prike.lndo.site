@php
    $orders = [
      123456 => [
        'orderId' => '123456',
        'orderTrackId' => '112233',
        'orderDate' => DateTime::createFromFormat('d-m-Y', '12-06-2021'),
        'status' => 'Pending',
        'total' => '266.47',
        'taxes' => '0.6',
        'ecoPacking' => '2',
        'delivery' => '0',
        'deliveryCo' => [
          'name' => 'Jet Express',
          'deliveryCoImg' => 'images/mock-data/my-account/delivery-jet.png',
          'shippingHours' => '14-17:00',
        ],
        'deliveryAddress' => [
          'name' => 'Mark Zuckerberg',
          'dateOfBirth' => DateTime::createFromFormat('d-m-Y', '14-05-1984'),
          'address1' => 'Viimsi tee 5000',
          'address2' => '74001 Viimsi vald',
          'county' => 'Harju maakond',
          'country' => 'Eesti',
          'email' => 'hello@helumium.com',
          'phone' => '+3725578524',
        ],
        'trackingDetails' => [
            ['date' => DateTime::createFromFormat('d-m-Y', '20-05-2021'), 'name' => 'Transport to destination', 'description' => 'Description'],
            ['date' => DateTime::createFromFormat('d-m-Y', '20-05-2021'), 'name' => 'Processed', 'description' => 'Description'],
            ['date' => DateTime::createFromFormat('d-m-Y', '19-05-2021'), 'name' => 'Arrival to facility', 'description' => 'Description'],
            ['date' => DateTime::createFromFormat('d-m-Y', '19-05-2021'), 'name' => 'Order shipped', 'description' => 'Description'],
            ['date' => DateTime::createFromFormat('d-m-Y', '19-05-2021'), 'name' => 'Order received', 'description' => 'Description'],
        ],
        'payment' => [
          'type' => 'bank',
          'name' => 'Poco pay',
          'bankImg' => 'images/mock-data/my-account/bank-poco.png'
        ],
        'currency' => 'EUR', // &euro;
        'discount' => true,
        'discountValue' => '25%',
        'productList' => [
          [
            'name' => 'Versus Savignon Blank',
            'price' => 129.99,
            'quantity' => 1,
            'um' => 'pcs',
            'weight' => '0.75ml',
            'discount' => true,
            'discountValue' => '25%',
            'img' => '/images/mock-data/my-account/versus-sauvignon-blank.png',
          ],
          [
            'name' => 'Primitivo Wine Selection',
            'price' => 84.49,
            'quantity' => 1,
            'um' => 'pcs',
            'weight' => '4.5L',
            'discount' => false,
            'img' => '/images/mock-data/my-account/primitivo-wine-selection.png',
          ],
          [
            'name' => 'Primitivo Wine Selection',
            'price' => 84.49,
            'quantity' => 1,
            'um' => 'pcs',
            'weight' => '4.5L',
            'discount' => false,
            'img' => '/images/mock-data/my-account/primitivo-wine-selection.png',
          ]
        ]
      ],
      123455 => [
        'orderId' => '123455',
        'orderTrackId' => '223344',
        'orderDate' => DateTime::createFromFormat('d-m-Y', '12-06-2021'),
        'status' => 'Completed',
        'total' => '91.79',
        'taxes' => '0.6',
        'ecoPacking' => '2',
        'delivery' => '0',
        'deliveryCo' => [
          'name' => 'Jet Express',
          'deliveryCoImg' => 'images/mock-data/my-account/delivery-jet.png',
          'shippingHours' => '14-17:00',
        ],
        'deliveryAddress' => [
          'name' => 'Mark Zuckerberg',
          'dateOfBirth' => DateTime::createFromFormat('d-m-Y', '14-05-1984'),
          'address1' => 'Viimsi tee 5000',
          'address2' => '74001 Viimsi vald',
          'county' => 'Harju maakond',
          'country' => 'Eesti',
          'email' => 'hello@helumium.com',
          'phone' => '+3725578524',
        ],
        'trackingDetails' => [
            ['date' => DateTime::createFromFormat('d-m-Y', '20-05-2021'), 'name' => 'Transport to destination', 'description' => 'Description'],
            ['date' => DateTime::createFromFormat('d-m-Y', '20-05-2021'), 'name' => 'Processed', 'description' => 'Description'],
            ['date' => DateTime::createFromFormat('d-m-Y', '19-05-2021'), 'name' => 'Arrival to facility', 'description' => 'Description'],
            ['date' => DateTime::createFromFormat('d-m-Y', '19-05-2021'), 'name' => 'Order shipped', 'description' => 'Description'],
            ['date' => DateTime::createFromFormat('d-m-Y', '19-05-2021'), 'name' => 'Order received', 'description' => 'Description'],
        ],
        'payment' => [
          'type' => 'bank',
          'name' => 'Poco pay',
          'bankImg' => 'images/mock-data/my-account/bank-poco.png'
        ],
        'currency' => 'EUR', // &euro;
        'discount' => true,
        'discountValue' => '25%',
        'productList' => [
          [
            'name' => 'Valmiermuiža Craft Brewery ...',
            'price' => 129.99,
            'quantity' => 1,
            'um' => 'pcs',
            'weight' => '0.75ml',
            'discount' => true,
            'discountValue' => '25%',
            'img' => '/images/mock-data/my-account/valmiermuza.png',
          ],
          [
            'name' => 'Primitivo Wine Selection',
            'price' => 129.99,
            'quantity' => 1,
            'um' => 'pcs',
            'weight' => '4.5l',
            'discount' => true,
            'discountValue' => '25%',
            'img' => '/images/mock-data/my-account/primitivo-wine-selection.png',
          ],
          [
            'name' => 'Primitivo Wine Selection',
            'price' => 129.99,
            'quantity' => 1,
            'um' => 'pcs',
            'weight' => '4.5l',
            'discount' => false,
            'img' => '/images/mock-data/my-account/primitivo-wine-selection.png',
          ]
        ]
      ],
    ];
@endphp

@if(isset($_GET['orderId']) && (int)$_GET['orderId'] == $_GET['orderId'])
  @if(isset($_GET['orderTrackId']) && (int)$_GET['orderTrackId'] == $_GET['orderTrackId'])
    <div class="single-order-details ml-19 p-10 w-full rounded-lg border-line border bg-white flex flex-col">
      <x-my-account.track-order :order="$orders[$_GET['orderId']]"/>
    </div>
  @else
    <div class="single-order-details ml-19 p-10 w-full rounded-lg border-line border bg-white flex flex-col">
      <x-my-account.single-order :order="$orders[$_GET['orderId']]"/>
    </div>
  @endif
@else
  <div class="my-orders-container pl-19 w-full">
    <h3 class="pb-4 text-xl leading-x1.2 font-semibold">My orders</h3>
    @foreach($orders as $order)
      <x-my-account.order-details :order="$order"/>
    @endforeach
  </div>
@endif
