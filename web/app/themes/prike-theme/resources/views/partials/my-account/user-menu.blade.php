@php
  $menuItems = [
    [
      'icon' => 'box',
      'title' => 'My Orders'
    ],
    [
      'icon' => 'user',
      'title' => 'My Details'
    ],
    [
      'icon' => 'link',
      'title' => 'Linked Accounts'
    ],
    [
      'icon' => 'star',
      'title' => 'My Reviews'
    ],
    [
      'icon' => 'lock',
      'title' => 'Change Password'
    ],
    [
      'icon' => 'truck',
      'title' => 'Delivery Methods'
    ],
    [
      'icon' => 'credit-card',
      'title' => 'Payment Preferences'
    ],
    [
      'icon' => 'bell',
      'title' => 'Notifications'
    ],
    [
      'icon' => 'users',
      'title' => 'Recommend to a Friend'
    ],
  ];
@endphp

<div class="user-menu w-90 flex flex-col border rounded-lg">
  <div class="user-welcome">
    <div class="bg-beige-1 rounded-t-lg">
      <img src="@asset('/images/mock-data/my-account/user-25-discount.png')">
    </div>
    <div class="user-name text-center bg-beige-2 h-18">
      <p class="font-poppins text-xs leading-relaxed">Welcome</p>
      <h2 class="font-poppins text-xl leading-tight">Mark Zuckerberg</h2>
    </div>
  </div>
  <div class="user-menu-content rounded-lg -mt-2.5 bg-white">
    <div class="menu-items-container mb-8">
      @foreach($menuItems as $menuItem)
        @php $active = $menuItem['icon'] == $activeMenuItem @endphp
        <x-my-account.user-menu-element :icon="$menuItem['icon']" :title="$menuItem['title']" :active=$active />
      @endforeach
    </div>

    <div class="p-4 font-poppins text-base leading-x1.6 w-full flex flex-row">
      <a href="{{ get_home_url() . '/faq' }}" class="text-grey-4">
        <h3 class="mb-2">Help & FAQ's</h3>
      </a>
    </div>

    <div class="p-4 font-poppins text-base leading-x1.6 w-full flex flex-row">
      <a href="{!! wp_logout_url(home_url()) !!}" class="text-grey-4">
        <h3 class="mb-2 font-bold text-gold-1">Logout</h3>
      </a>
    </div>
  </div>
</div>

