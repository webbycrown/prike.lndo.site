<div class="page-header">
  <?php
  if (is_search()) { ?>
    @if (have_posts())
    <?php $count = array();
    $cat_arr = array(); ?>
    @while(have_posts()) @php(the_post())
    <?php 
    $count[] = get_the_ID();
    $categories = get_the_terms( get_the_ID(), 'category' );
    $product_cats = get_the_terms( get_the_ID(), 'product_cat' );
    if($categories){
      foreach ($categories as $category) {
        if( !in_array($category->slug, $cat_arr) ){
          $cat_arr[] = $category->slug;    
        }
      }
    }
    if($product_cats){
      foreach ($product_cats as $product_cat) {
        if( !in_array($product_cat->slug, $cat_arr) ){
          $cat_arr[] = $product_cat->slug;
        }
      }
    }
    ?>
    @endwhile
    @endif
    <div class="breadcrumb mb-4">
      <ul class="flex items-center">
        <li class="mx-1 font-medium font-poppins font-medium text-sm text-regular-2"> <a href="<?php echo site_url(); ?>"  class="font-poppins font-medium text-sm text-regular-2 font-medium">Home</a> </li>
        <span class="font-poppins font-medium text-sm text-regular-2">></span> 
        <li class="mx-1 font-poppins font-medium font-medium text-sm text-regular-2"> <span>search results</span> </li>
      </ul>
      
    </div>
    <div class="flex items-center mb-6 justify-between">
      <div class="flex items-center">
        <h1 class="font-thin text-3xl">{!! $title !!}</h1> <span class="result_count bg-secondary-2 ml-2 text-regular rounded-full py-1 px-3"><?php echo count($count); ?></span>  
      </div>
      <div>
        <label class="dropdown">
          <div class="dd-button font-poppins">
            Sort by Relevance
          </div>
          <input type="checkbox" class="dd-input" id="test">
          <ul class="dd-menu font-poppins">
            <li>Action</li>
            <li>Another action</li>
            <li>Something else here</li>
            <li class="divider"></li>
          </ul>
        </label>
      </div>
    </div>
    <div class="mt-8 mb-10">
      <ul class="result_taxonomy flex items-center mb-8">
        <?php foreach ($cat_arr as $cat_val) { ?>
          <li class="result_tax_item mr-3">
            <a href="<?php echo '#'; ?>" class="border-2 border-gray-1 rounded-full text-gray-1 text-sm font-medium py-1.5 px-4 block hover:bg-gray-1 hover:text-white"><?php echo $cat_val; ?></a>
          </li>
        <?php } ?>
      </ul>
    </div>
  <?php }else{ ?>
    <h1 class="font-thin text-3xl">{!! $title !!}</h1>
  <?php } ?>
</div>
