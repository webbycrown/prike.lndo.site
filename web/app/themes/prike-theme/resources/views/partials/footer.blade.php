<footer class="footer bg-gray-1 px-4 pt-12 pb-6 xl:pt-23">
    <div class="container xl:grid xl:auto-rows-auto xl:grid-cols-12">
        <div class="footer-intro xl:col-start-1 xl:col-end-6 xl:row-span-1">
            <h1 class="text-gold xl:font-medium xl:font-epilogue font-roboto text-3xl xl:text-3.5xl leading-9 mb-4">
                Let's keep in touch
            </h1>
            <p class="text-regular-2 mb-8 xl:mb-20 xl:pr-10">
                You'll get started guide and subscriber discounts.
            </p>
        </div>
        <div class="footer-subscribe xl:col-start-6 xl:col-end-13 xl:row-span-1">
            <form action="">
                <div class="footer-form-group flex mb-3.25">
                    <input type="email" name="subscription-email" id="subscription-email"
                        placeholder="Sisesta email ja liitu"
                        class="
              form-input
              flex-1
              border-r-0
              bg-gray-special
              text-regular-2
              leading-12
              border-0
              rounded-l
              text-base
              py-0
              xl:pl-6
              font-poppins
              placeholder-regular-2
            " />
                    <button type="submit" class="bg-gold outline-none w-14 h-14 px-0.5 rounded-r-md">
                        <span class="icon-arrow-right text-2xl"></span>
                    </button>
                </div>
                <div class="footer-form-group mb-4.5 xl:mb-18">
                    <input type="checkbox" name="subscription-age-confirmation" id="subscription-age-confirmation"
                        class="form-checkbox bg-transparent xl:border-regular-2 border-line rounded-sm border-2 mr-2 w-5 h-5 align-text-top checked:text-gold checked:ring-0 active:ring-0 focus:ring-0" />
                    <label
                        class="text-white xl:text-regular-2 opacity-75 xl:text-base xl:font-semibold text-sm font-poppins"
                        for="subscription-age-confirmation">Olen vähemalt 18 ja enam noor</label>
                </div>
            </form>
        </div>

        <div class="h-px gradient-gold-black xl:hidden" style=""></div>

        <div class="footer-safe-purchase xl:col-start-12 xl:col-end-13 xl:row-start-2 xl:row-end-3">
            <div class="flex justify-center py-4">
                <img src="@asset('images/icons/safe-purchase.svg')"
                    alt="Eesti e-kaubanduse liit - Turvaline ostukoht" />
            </div>
        </div>
        <div
            class="footer-nav text-regular-3 xl:col-start-4 xl:col-end-12 xl:row-start-2 xl:row-end-4 xl:pl-16 xl:mb-14">
            <x-elements.nav-items-footer />
        </div>
        <div class="
        footer-social
        xl:col-start-1 xl:col-end-4 xl:row-start-2 xl:row-end-2
      ">
            <div class="py-8 flex justify-center items-center xl:justify-start xl:py-0">
                <h5 class="text-regular-3 leading-none mr-6">Jälgi meid</h5>
                <div class="grid grid-flow-col gap-x-4">
                    <img src="@asset('images/icons/social/fb-gold.svg')" alt="Prike - Facebook page" />
                    <img src="@asset('images/icons/social/ig-gold.svg')" alt="Prike - Instagram page" />
                    <img src="@asset('images/icons/social/yt-gold.svg')" alt="Prike - Youtube channel" />
                </div>

            </div>
        </div>
        <div class="footer-address xl:col-start-1 xl:col-end-4 xl:row-start-2 xl:row-end-3 xl:mt-12">
            <div class="text-center xl:text-left">
                <p class="text-regular-3  xl:text-regular-2 xl:mb-2 mb-3.5 xl:text-sm">
                    Peterburi tee 92g, 13816 Tallinn, Eesti
                </p>
                <p class="text-regular-3  xl:text-regular-2 xl:mb-2 mb-4 xl:text-sm">E-R: 09:00-17:00</p>
                <p class="text-regular-3 xl:text-regular-2 xl:text-sm">+372 622 4900 / epood@prike.ee</p>
            </div>
        </div>
        <div class="footer-payments-info xl:col-start-7 xl:col-end-13 xl:row-start-4 xl:row-end-4 xl:flex">
            <div class="pt-10 pb-11 text-center xl:py-0 xl:self-end">
                <img src="@asset('images/icons/payments-footer/payments-footer-mob.svg')" alt="Prike payment methods"
                    class="xl:hidden inline-block" />
                <img src="@asset('images/icons/payments-footer/payments-footer.svg')" alt="Prike payment methods"
                    class="hidden xl:block" />
            </div>
        </div>
        <div class="footer-t-and-c xl:col-start-1 xl:col-end-7 xl:row-start-4 xl:row-end-4 xl:self-end xl:inline">
            <div class="flex flex-wrap justify-center xl:justify-start">
                <div
                    class="flex justify-center pt-4 border-t border-line border-opacity-50 xl:border-0 xl:pt-0 xl:inline xl:order-last mb-2 xl:mb-0">
                    <a href=""
                        class="body-2 px-2 text-white xl:text-regular xl:text-xs font-thin font-poppins leading-6">Terms
                        &
                        conditions</a>
                    <span class="leading-3 text-white xl:text-regular">|</span>
                    <a href=""
                        class="body-2 px-2 text-white xl:text-regular xl:text-xs font-thin font-poppins leading-6">Privacy
                        policy</a>
                </div>
                <div class="footer-copyright xl:inline xl:mr-9">
                    <p class="text-sm xl:text-xs leading-x1.6 text-center text-white xl:text-regular xl:inline">
                        Copyright 2021 Prike. All rights reserved.
                    </p>
                </div>
            </div>

        </div>
    </div>
</footer>
