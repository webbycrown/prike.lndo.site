<div class="responsive-navigation h-auto bg-secondary-2 hidden lg:hidden">
  <div class="categories p-4 space-y-4">
    <x-category.category-tab
      variant="menu"
      title="Joogiekspert soovitab"
    ></x-category.category-tab>
    <x-category.category-tab
      variant="menu"
      title="Vein"
      subtitle="Üle 120 toote"
    ></x-category.category-tab>
    <x-category.category-tab
      variant="menu"
      title="Šamplanja"
      subtitle="Üle 120 toote"
    ></x-category.category-tab>
    <x-category.category-tab
      variant="menu"
      title="Kanged joogid"
      subtitle="Üle 120 toote"
    ></x-category.category-tab>
    <x-category.category-tab
      variant="menu"
      title="Lahjad joogid"
      subtitle="Üle 120 toote"
    ></x-category.category-tab>
    <x-category.category-tab
      variant="menu"
      title="Bundle tooted"
      subtitle="Üle 120 toote"
    ></x-category.category-tab>
    <x-category.category-tab
      variant="menu"
      title="Muud tooted"
      subtitle="Üle 120 toote"
    ></x-category.category-tab>
    <x-category.category-tab
      variant="vip"
      title="Muud tooted"
    ></x-category.category-tab>
  </div>
  <div class="links p-4">
    <ul class="space-y-4">
      <li>
        <a
          href="http://"
          class="text-lg text-regular font-semibold font-poppins"
          >Footer link</a
        >
      </li>
      <li>
        <a
          href="http://"
          class="text-lg text-regular font-semibold font-poppins"
          >Footer link</a
        >
      </li>
      <li>
        <a
          href="http://"
          class="text-lg text-regular font-semibold font-poppins"
          >Footer link</a
        >
      </li>
      <li>
        <a
          href="http://"
          class="text-lg text-regular font-semibold font-poppins"
          >Footer link</a
        >
      </li>
    </ul>
  </div>
  <div class="contacts border-t border-gold-2 p-4">
    <p class="body-2">Esmaspäevast reedeni: 09:00-17:00</p>
    <a href="tel:+3726224900" class="link-2">+372 622 4900</a>
    <span class="text-gold-2 mx-2">/</span>
    <a href="mailto:epood@prike.ee" class="link-2">epood@prike.ee</a>
  </div>
</div>
