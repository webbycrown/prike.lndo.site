{{--
  Template Name: Intermediary category
--}}

@extends('layouts.app')
@section('content')
  
  <div class="intermediary-cat">

    <div class="section jumbotron pb-10">
  
      <x-category.jumbotron />
    </div>

    <div class="section container pt-6 pb-12">
        <div class="cat-section-heading flex justify-between items-center pb-2">
          <h2 class="text-title font-normal text-2.5xl leading-x1.2">Trending now</h2>
          <a href="#" class="text-gold-2 text-md font-poppins font-medium ">View All</a>
        </div>
      <x-category.carousel id="1"/> 
    </div>
    <div class="section container pt-6 pb-16">
      <div class="categories-container grid grid-cols-1 gap-y-4 lg:gap-5 lg:grid-cols-12 ">
        <x-category.category-tab title="Champaigne" subtitle="Over 125 items" variant="menu" narrow />
        <x-category.category-tab title="Sparkling wine" subtitle="Over 125 items" variant="orange" narrow />
        <x-category.category-tab title="Prosecco" subtitle="Over 125 items" variant="champagne" narrow />
      </div>
    </div>
    
    <div class="section container pt-6 pb-12">
      <div class="cat-section-heading flex justify-between items-center pb-2">
        <h2 class="text-title font-normal text-2.5xl leading-x1.2">Trending now</h2>
        <a href="#" class="text-gold-2 text-md font-poppins font-medium ">View All</a>
      </div>
    <x-category.carousel id="2"/> 
  </div>
  <div class="section container pt-6 pb-12">
    <div class="categories-container grid grid-cols-1 gap-y-4 lg:gap-5 lg:grid-cols-12 ">
      <x-category.category-tab title="Champaigne" subtitle="Over 125 items" variant="menu" narrow />
      <x-category.category-tab title="Sparkling wine" subtitle="Over 125 items" variant="orange" narrow />
      <x-category.category-tab title="Prosecco" subtitle="Over 125 items" variant="champagne" narrow />
    </div>
  </div>
    
    <div class="section banner container pt-10 pb-18">
      <x-general.banner sale="20"/>
    </div>

      {{-- note: woocommerce breadcrumbs --}}
      {{-- @php
        do_action('woocommerce_before_main_content');
      @endphp --}}
    
      {{-- <header class="woocommerce-products-header">
        @if (apply_filters('woocommerce_show_page_title', true))
          <h1 class="woocommerce-products-header__title page-title">{!! woocommerce_page_title(false) !!}</h1>
        @endif
    
        @php
          do_action('woocommerce_archive_description')
        @endphp
      </header> --}}
  
    
    
      {{-- @if (woocommerce_product_loop())
        @php
          do_action('woocommerce_before_shop_loop');
          woocommerce_product_loop_start();
        @endphp
    
        @if (wc_get_loop_prop('total'))
          @while (have_posts())
            @php
              the_post();
              do_action('woocommerce_shop_loop');
              wc_get_template_part('content', 'product');
            @endphp
          @endwhile
        @endif
    
        @php
          woocommerce_product_loop_end();
          do_action('woocommerce_after_shop_loop');
        @endphp
      @else
        @php
          do_action('woocommerce_no_products_found')
        @endphp
      @endif --}}
      
   
  
      {{-- @php
        do_action('woocommerce_after_main_content');
        do_action('get_sidebar', 'shop');
        do_action('get_footer', 'shop');
      @endphp --}}
    <div class="section pt-10 ">
      <div class="bg-secondary-2">

        <x-recipes.recipes-slider title="Red Wine Cocktails to Try Now"></x-recipes.recipes-slider>
      </div>
    </div>
  </div>
@endsection
