@props(['icon' => 'info', 'message'])

<div class="list-box-item flex text-secondary-1 lg:justify-center">
  <!-- <img
    src="@asset('/images/icons/delivery.svg')"
    class="mr-3 fill-current stroke-current text-secondary-1"
  /> -->
  <span class="leading-8 align-middle mr-2 text-2.25xl xl:text-3xl {{ 'icon-'.$icon }}"></span>
  <p class="text-regular-3 leading-8 text-base xl:text-sm">{{ $message }}</p>
</div>