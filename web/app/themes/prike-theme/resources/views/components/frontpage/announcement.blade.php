<div class="
    announcement
    gradient-radial-blue
    h-38
    xl:h-132
    p-4 lg:px-22
    pb-0
    flex flex-col
    lg:flex-row
    justify-between
    rounded
  ">
  <div class="announcement-texts lg:w-5/12 lg:self-center">
    <h5 class="subtitle mb-8 text-regular-2 font-thin uppercase">15 Mai 2021</h5>
    <h1 class="font-thin text-white mb-8 font-size-2 leading-x1.1 lg:text-5xl">Maailma Viski Päev</h1>
    <p class="text-regular-3 mb-14 font-thin font-size-1 leading-relaxed lg:text-lg">
      Discover a selection of whiskies from Master Distiller Rachel Barrie and
      find a new favourite this World Whisky Day.
    </p>
    <x-controls.button variant="gold-2"> Avasta tooted </x-controls.button>
  </div>
  <div class="announcement-image flex justify-center lg:w-1/2">
    <img src="@asset('/images/mock-data/announcement-thumb.png')" class="max-w-full" />
  </div>
</div>
