<div class="bg-gray-1">
  <div class="list-box w-full lg:w-5/6 mx-auto py-10 px-4">
    <div class="grid grid-flow-row lg:grid-cols-3 gap-y-4">
      <x-frontpage.list-box-item icon="delivery" message="Fast same day delivery" />
      <x-frontpage.list-box-item icon="benefits" message="Premium beverage brands -25%" />
      <x-frontpage.list-box-item icon="bd" message="Birthday discount" />
    </div>
  </div>
</div>