<div class="py-9">
    <div class="xl:h-33 rounded pb-56 p-4 bg-black xl:flex xl:px-11 xl:py-12 bg-contain bg-no-repeat xl:bg-contain bg-right-bottom sm-background-size-200"
        style="background-image: url(@asset('/images/mock-data/open-account.png')); background-size: cover;">
        <div class="bg-temp-frontpage xl:w-1/3 p-4 xl:p-12 rounded xl:w-27 xl:min-h-27.75" style="background-color: #363436;
        box-shadow: 0 40px 30px -10px rgb(0 0 0);
        -webkit-box-shadow: 0 40px 30px -10px rgb(0 0 0);
        -moz-box-shadow: 0 40px 30px -10px rgba(0, 0, 0, 1);">
            <h2 class="xl:w-23.5 text-2.5xl xl:text-3xl leading-x1.2 text-white mb-8 font-thin">
                Tee tasuta e-poe konto ja saad koheselt <span class="text-gold-1">-25%</span> enamus toodetelt*
            </h2>
            <ul class="mb-12 list-style-diamond list-inside text-regular-3 font-poppins">
                <li class="text-lg mb-3">Logi sisse ja hinnad muutuvad</li>
                <li class="text-lg mb-3">Sünnipäeval kuni 40% ale</li>
                <li class="text-lg mb-3">Eksklusiivsed pakkumised</li>
            </ul>
            <button class="bg-white rounded px-4 py-5 w-full xl:w-auto xl:px-8">
                Tee tasuta konto
            </button>
        </div>
    </div>
</div>
