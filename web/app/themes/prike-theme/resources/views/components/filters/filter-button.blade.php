@props(['count' => 2, 'title' => 'Filter'])
<button data-hystmodal="#filters-modal" type="button" class="flex bg-gray-1 p p-4 items-center justify-between rounded-100 space-x-2 select-none outline-none">
  <span class="icon-sliders text-regular-3 text-xl"></span>
  <span class="text-white font-normal opacity-90">{{ $title }}</span>
  <span class="text-gold  ont-normal opacity-90">(+{{ $count }})</span>
</button>