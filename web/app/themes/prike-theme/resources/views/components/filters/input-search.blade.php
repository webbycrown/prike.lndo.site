@props([
  'placeholder' => 'Enter a country',
  'description',
  'type',
  'label',
  'variant' => 'default',
  'variants' => [
    'default' => 'border-line text-title focus:border-gold-2 focus:text-line-2 focus:ring-0',
  ]
])

<div class="input-search max-w-sm ">
  <input 
    type="{{ isset($type) ? $type : 'text' }}"
    class="form-input py-3.5 px-3 border-2 rounded w-full
    disabled:border-line disabled:text-line
    {{ $variant ? $variants[$variant] : $variants['default'] }}
    "
    {{ isset($placeholder) ? $attributes->merge(['placeholder' => $placeholder]) : null }}
    {{ $attributes }}
  />
  <label class="form-label hidden absolute px-2 -top-3 left-4 bg-white">{{ $label ?? null }}</label>
  
  @if(isset($description))
    <div class="{{ $variant == 'alert' ? 'text-red' : 'text-regular-2' }} text-xxs">
      {{ $description }}
    </div>
  @endif
</div>