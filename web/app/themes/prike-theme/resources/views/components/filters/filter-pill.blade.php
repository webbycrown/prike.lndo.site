@props([ 'title' => '', 'fields' ])

<div class="cat-filter lg:border-0 border-solid border-b border-line">
  <div class="cat-title lg:h-20 lg:py-5 py-4 pl-4 pr-6 items-center flex justify-between relative cursor-pointer z-20">
      <h4 class="transition-colors  duration-300 select-none font-poppins font-bold text-title text-base lg:text-sm lg:font-medium relative">{{ $title }}</h4>
      <button class="caret focus:outline-none leading-xs transition-transform ml-0.5">
        <span class="icon-down text-regular-2 duration-300 text-2xl"></span>
      </button>
  </div>
  <div class="cat-selector hidden bg-secondary-2 lg:absolute lg:top-20 left-0 w-full">
    <div class="relative">
      {{-- <div class="overlay absolute top-0 left-0 w-72 h-72 bg-gold-2 "></div> --}}
      <div class="bg-secondary-2">

        <div class="container pt-3 lg:pt-10 lg:pb-5">
            <div class="flex flex-wrap items-center justify-start -mx-2.5">
              @foreach ($fields as $collection)
                <x-filters.pill >{{ $collection['label'] }}</x-filters.pill>
              @endforeach
            </div>
        </div>
      </div>
      <div class="line absolute hidden lg:block bg-line h-px w-full left-0 top-0 "></div>
    </div>
  </div>
</div>