@props([
  'marked' => false,
  'close' => false,
  'types' => [
    'default' => 'text-title ',
    'marked' => 'text-gold-2',
  ]
  ])
<span class="pill flex items-center rounded-100 justify-between py-1 font-semibold  
          border-gray-1 bg-secondary-2 mx-2.5 mb-4  px-2.5 border-2 border-solid
            {{ $marked ? $types['marked'] : $types['default'] }}
             ">
  <span class="">{{ $slot }}</span>
  @if ($close)
      
  <button type="button" class="outline-none ml-2 focus:outline-none">
    <span class="icon-close align-middle text-gold-2"></span>
  </button>
  @endif
</span>