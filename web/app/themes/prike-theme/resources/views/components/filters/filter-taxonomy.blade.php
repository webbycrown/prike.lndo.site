@props([ 'title' => '', 'fields', 'selected' => false, 'imgSrc' => @asset('/images/backgrounds/1pixel.png') ])

<div class="cat-filter lg:border-0 border-solid border-b border-line">
  <div class="cat-title lg:h-20 py-5 pl-4 pr-6 items-center flex justify-between relative cursor-pointer z-20">
      <h4 class="transition-colors  duration-300 select-none font-poppins font-bold text-title text-base lg:text-sm lg:font-medium relative">
        {{ $title }}
        @if ($selected)
          <x-filters.filter-mark />
        @endif
      </h4>
    <button class="caret focus:outline-none leading-xs transition-transform ml-0.5">
      <span class="icon-down text-regular-2 duration-300 text-2xl"></span>
    </button>
  </div>
  <div class="cat-selector hidden bg-secondary-2 lg:absolute lg:top-20 left-0 w-full">
    <div class="relative">
      {{-- <div class="overlay absolute left-0 right-0 bottom-2 top-2 filter blur transform" style="background-image: url({{ $imgSrc }})"></div> --}}
      <div class="bg-secondary-2">

        <div class="container pt-10 ">
            <x-filters.input-search />
            <div class="flex flex-wrap items-center justify-start md:justify-between -mx-2.5">
              @foreach ($fields as $collection)
        
                <div class="w-full max-w-sm sm:w-1/2 flex-grow flex-shrink lg:w-1/3 xl:w-1/5 py-5 px-2.5">
                  @foreach ($collection as $item)
                    <div class="pb-4 flex items-center">
                      <label class="cursor-pointer flex items-center">
                        <x-controls.checkbox variant="filter"/>
                        <span class="ml-4 font-poppins text-base leading-x1.6">{{ $item['label'] }}</span>
                      </label>
                      <span class="text-regular-2 text-sm font-poppins pl-1">({{ $item['qty'] }})</span>
                    </div>
                  @endforeach
                </div>
              @endforeach
            </div>
        </div>
      </div>
      <div class="line absolute hidden lg:block bg-line h-px w-full left-0 top-0 "></div>
    </div>
  </div>
</div>

