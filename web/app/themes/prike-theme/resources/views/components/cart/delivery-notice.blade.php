@props([
  'type' => 'guest',
  'types' => [
    'guest' => 'bg-secondary-2',
    'member' => 'gradient-green'
  ]
  ])
<div class="delivery-notice {{ $type ? $types[$type] : $type['guest'] }} rounded mb-5 p-4">
  <p class="text-sm text-title">
    <span class="icon-delivery align-middle text-2.25xl"></span>
    <b class="font-semibold">Free delivery</b>
    from 
    <b class="font-semibold">39</b>
    for members only.
  </p>
</div>