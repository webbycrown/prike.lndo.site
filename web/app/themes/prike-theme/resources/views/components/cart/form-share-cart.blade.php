<form action="" class="flex flex-col items-center bg-bg3 rounded-lg pt-13 pb-10 px-22">
  <div class="flex justify-center mb-6">
    <svg width="54" height="74" viewBox="0 0 54 74" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M45.8 71.7526H8.23053C4.34797 71.7526 1.32186 68.184 1.72153 64.0731L6.3178 19.5664H47.6842L52.2804 64.0731C52.7372 68.184 49.7111 71.7526 45.8 71.7526Z" stroke="#C99238" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
      <path d="M38.3197 32.5269V13.0571C38.3197 6.80499 33.2666 1.75195 27.0146 1.75195C20.7625 1.75195 15.7095 6.80499 15.7095 13.0571V32.5269" stroke="#C99238" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
    </svg>
  </div>
  <h4 class="mb-8 font-semibold">Share my cart just copy link below</h4>
  <div class="relative flex w-full flex-wrap items-stretch mb-8 font-poppins">
    <input type="text" placeholder="https:// prike.com/sharecart/action/restore/  "
      class="pl-5 py-4 placeholder-regular-2 text-title relative text-base bg-white rounded border-2 border-line outline-none focus:outline-none focus:border-line focus:ring-0 w-full pr-10" />
    <button
      class="hidden z-10 h-full leading-snug font-normal text-center absolute bg-transparent rounded flex items-center justify-center w-8 right-0 pr-3 py-3 outline-none focus:outline-none">
      <span class="icon-close text-2xl"></span>
    </button>
  </div>
  <div class="space-x-4">
    <button class="py-4.5 w-40 border-2 active:border-0  bg-black text-white border-black hover:text-gold active:text-white disabled:bg-gray-2 disabled:text-white disabled:border-gray-2 rounded transform transition-all duration-300 text-base font-poppins font-semibold font-title">
      Copy link
    </button>
  </div>
</form>
