<div class="fixed md:static z-10 left-0 bottom-0 w-full">
  <div class="filter drop-shadow-type1 md:drop-shadow-none px-2 md:px-0 pb-2">
    <form action="" class="checkout bg-white rounded ">
      <div class="px-3 md:px-0  border-b border-line ">
        <div class="total flex justify-between items-center py-4 leading-snug">
          <div class="flex items-center">
            <h5 class="mr-3 font-semibold">Total</h5>
            <a href="#" class="font-poppins" data-pmodal="#modal-cart-promocode">Add promocode</a> 
          </div>
          <div class="total-price text-title font-semibold text-base">
            <p>357.97<span class="woocommerce-currency">€</span></p>
          </div>
        </div>
      </div>
      <div class="px-3 md:px-0 pb-2">
        <div class="flex justify-between pt-4 pb-2 mb-2.5">
          <div class="form-field w-full px-1.5">
            <label class="">
              <x-controls.radio name="type" variant="checkout" />
              <span class="ml-3">Buy as a guest</span>
            </label>
          </div>
          <div class="form-field w-full pl-1.5 flex flex-col">
            <label>
              <x-controls.radio name="type" variant="checkout" />
              <span class="ml-3">Buy as a member</span>
            </label>
            <small class="text-green pl-8 font-medium">Save -25% or more</small>
          </div>
        </div>
        <x-controls.button variant="gold">Proceed to checkout</x-controls.button>
      </div>
    </form>
  </div>
</div>