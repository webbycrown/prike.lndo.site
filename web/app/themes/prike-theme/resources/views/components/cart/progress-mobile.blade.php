<div class="progress-mobile bg-bg3 my-4">
  <div class="grid grid-cols-4 h-10  overflow-hidden">
    <div class="flex h-full bg-secondary-1 items-center pl-4 justify-between text-secondary-1">
        <a href="#" class="block text-title font-poppins">Cart(3)</a>
      <svg width="15" height="40" class="-mr-3 relative -right-0.5 fill-current z-10" viewBox="0 0 15 40" xmlns="http://www.w3.org/2000/svg">
        <path d="M1 40L14 20L1 0" stroke=""/>
      </svg>
    </div>
    <div class="h-full col-span-2">
      <div class="flex h-full relative items-center text-bg3">
        <div class="flex flex-auto justify-center">
          <a href="#" class="block text-regular-2 font-poppins">Transpordi valik</a>
        </div>
        <svg width="15" height="40" class="-mr-3 relative -right-0.5 stroke-current text-line z-10" fill="none" viewBox="0 0 15 40" xmlns="http://www.w3.org/2000/svg">
          <path d="M1 40L14 20L1 0" stroke=""/>
        </svg>
      </div>
    </div>
    <div class="h-full">
      <div class="flex h-full items-center justify-end pr-4">
          <a href="#" class="block text-regular-2 font-poppins">Makse</a>
      </div>
    </div>
  </div>
</div>