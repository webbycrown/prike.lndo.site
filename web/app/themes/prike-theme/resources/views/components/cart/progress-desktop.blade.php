<div class="mx-4 py-4">
  <div class="flex items-center">
    <div class="flex items-center">
      <div class="rounded-full flex items-center justify-center h-10 w-10 bg-secondary-1 border-2 border-secondary-1">
        <h5>1</h5>
      </div>
    </div>
    <a href="#" class="ml-4.5 text-regular-3 capitalize font-poppins">Cart</a>
    <div class="line flex-auto border-t transition duration-300 ease-in-out border border-regular-2 opacity-50 mx-6"></div>
    <div class="flex items-center text-regular-2 relative">
      <div class="rounded-full flex items-center justify-center transition duration-300 ease-in-out h-10 w-10 py-3 border-2 border-line-2 bg-line-2">
        <h5>2</h5>
      </div>
    </div>
    <a href="#" class="ml-4.5 text-regular-3 capitalize font-poppins">Detaild & delivery</a>
    <div class="line flex-auto border-t transition duration-300 ease-in-out border border-regular-2 opacity-50 mx-6"></div>
    <div class="flex items-center text-regular-2 relative">
      <div class="rounded-full flex items-center justify-center transition duration-300 ease-in-out h-10 w-10 py-3 border-2 border-line-2 bg-line-2">
        <h5>3</h5>
      </div>
    <a href="#" class="ml-4.5 text-regular-3 capitalize font-poppins">Pay</a>
    </div>
  </div>
</div>