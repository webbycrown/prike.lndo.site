@props([
  'title' => 'Ticket Wine tasting by Prike expert',
  'type' => 'Standard',
  'price' => 17.50,
  'currency' => '€',
  'person_name' => 'Artem Panfilov',
  'birthday' => '17.02.1989',
  'email' => 'email@email.com',
  'phone' => '+385685472458800',
  'qty' => 1,
])

<div class="product-name lg:flex-1 pb-8 lg:pb-0 pr-12">
  <h4 class="font-semibold text-lg text-gray-1">{{ $title }}</h4>
  <p class="text-regular text-sm">{{ $type }}</p>
  <p class="text-regular text-sm">{{ $person_name }}</p>
  <p class="text-regular text-sm">{{ $birthday }}</p>
  <p class="text-regular text-sm">{{ $email }}</p>
  <p class="text-regular text-sm">{{ $phone }}</p>
  <a href="#">Edit</a>
</div>
<div class="flex justify-between lg:justify-start items-end lg:items-center">

  <div class="product-quantity text-base flex">
    <p>{{ $qty }} <span>pcs</span></p>    
  </div>

  <div class="product-subtotal text-gray-1 lg:ml-11">
    <h5 class="amount inline font-semibold text-base">{{ $price }}<span class="woocommerce-currency">{{ $currency }}</span></h5>
  </div>
</div>