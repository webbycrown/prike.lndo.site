@props([
  'title' => 'Versus Savignon Blank',
  'price' => '89.56', 
  'volume' => '0.75l',
  'qty' => 1,
])
<div class="product-name lg:flex-1 pb-8 lg:pb-0 pr-12">
  <h4 class="font-semibold text-lg text-gray-1">{{ $title }}</h4>
  <span class="text-regular text-sm">{{ $volume }}</span>
</div>
<div class="flex justify-between lg:justify-start items-end lg:items-center">

  <div class="product-quantity text-base flex">
    <div class="quantity border border-line rounded flex justify-between items-center py-1.5 px-1.5">
      <label class="screen-reader-text" for="">{{ $title }}</label>
      <span class="product-qty icon-minus text-gray-2 text-2xl" data-qty="minus"></span>
      <input type="text" id="" class="outline-none w-10 border-0 text-title bg-white py-0 text-center" step="1" min="0" max="" name="" value="{{ $qty }}" title="Qty" inputmode="numeric">
      <span class="product-qty icon-plus text-gray-2 text-2xl" data-qty="plus"></span>
    </div>
  </div>

  <div class="product-subtotal text-gray-1 lg:ml-11">
    <h5 class="amount inline font-semibold text-base">{{ $price }}<span class="woocommerce-currency">€</span></h5>
  </div>
</div>