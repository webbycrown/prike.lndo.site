@props([
  'title' => 'Gift wrapping +1.30',
  'currency' => '€',
  'imgSrc' => @asset('/images/mock-data/gift-bag.png')
  ])
<div class="gift gradient-gift rounded lg:w-3/5 xl:w-1/2 w-full flex px-4 mb-4">
  <div class="flex justify-between w-full">
    <div class="py-5 flex-grow">
      <h5 class="font-semibold">{{ $title }}<span class="woocommerce-currency">{{ $currency }}</span></h5>
      <a href="#" class="inline-block bg-gray-1 text-white rounded py-1.5 px-4.5">Add</a>
    </div>
    <div class="gift-image">
      <img src="{{ $imgSrc }}" class="w-32 object-cover" alt="">
    </div>
  </div>
</div>