<form action="" class="flex flex-col items-center bg-bg3 rounded-lg pt-13 pb-10 px-22">
  <div class="flex justify-center mb-6">
    <img src="@asset('/images/icons/icon-cart-promocode.svg')" alt="">
    {{-- <svg width="74" height="55" viewBox="0 0 74 55" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M50.2082 13.791L22.7915 41.2077" stroke="#C99238" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
      <path d="M25.7293 21.6257C28.4332 21.6257 30.6252 19.4337 30.6252 16.7298C30.6252 14.0259 28.4332 11.834 25.7293 11.834C23.0254 11.834 20.8335 14.0259 20.8335 16.7298C20.8335 19.4337 23.0254 21.6257 25.7293 21.6257Z" stroke="#C99238" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
      <path d="M47.2708 43.1667C49.9747 43.1667 52.1667 40.9747 52.1667 38.2708C52.1667 35.5669 49.9747 33.375 47.2708 33.375C44.5669 33.375 42.375 35.5669 42.375 38.2708C42.375 40.9747 44.5669 43.1667 47.2708 43.1667Z" stroke="#C99238" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
      <mask id="path-4-inside-1_4642:66730" fill="white">
      <path fill-rule="evenodd" clip-rule="evenodd" d="M0 4C0 1.79086 1.79086 0 4 0H70C72.2091 0 74 1.79086 74 4V16C68.4771 16 64 20.4772 64 26C64 31.5228 68.4771 36 74 36V51C74 53.2091 72.2091 55 70 55H4C1.79086 55 0 53.2091 0 51V36C5.52285 36 10 31.5228 10 26C10 20.4772 5.52285 16 0 16V4Z"/>
      </mask>
      <path d="M74 16V19H77V16H74ZM74 36H77V33H74V36ZM0 36V33H-3V36H0ZM0 16H-3V19H0V16ZM4 -3C0.134007 -3 -3 0.134006 -3 4H3C3 3.44772 3.44771 3 4 3V-3ZM70 -3H4V3H70V-3ZM77 4C77 0.134005 73.866 -3 70 -3V3C70.5523 3 71 3.44772 71 4H77ZM77 16V4H71V16H77ZM67 26C67 22.134 70.134 19 74 19V13C66.8203 13 61 18.8203 61 26H67ZM74 33C70.134 33 67 29.866 67 26H61C61 33.1797 66.8203 39 74 39V33ZM77 51V36H71V51H77ZM70 58C73.866 58 77 54.866 77 51H71C71 51.5523 70.5523 52 70 52V58ZM4 58H70V52H4V58ZM-3 51C-3 54.866 0.134012 58 4 58V52C3.44771 52 3 51.5523 3 51H-3ZM-3 36V51H3V36H-3ZM7 26C7 29.866 3.86599 33 0 33V39C7.1797 39 13 33.1797 13 26H7ZM0 19C3.86599 19 7 22.134 7 26H13C13 18.8203 7.1797 13 0 13V19ZM-3 4V16H3V4H-3Z" fill="#C99238" mask="url(#path-4-inside-1_4642:66730)"/>
    </svg> --}}
  </div>
  <h4 class="mb-8 font-semibold">Edit promocode or gift card</h4>
  <div class="relative flex w-full flex-wrap items-stretch mb-8 font-poppins">
    <input type="text" placeholder="Enter code"
      class="pl-5 py-4 placeholder-regular-2 text-title relative text-base bg-white rounded border-2 border-line outline-none focus:outline-none focus:border-line focus:ring-0 w-full pr-10" />
    <button
      class="z-10 h-full leading-snug font-normal text-center absolute bg-transparent rounded flex items-center justify-center w-8 right-0 pr-3 py-3 outline-none focus:outline-none">
      <span class="icon-close text-2xl"></span>
    </button>
  </div>
  <div class="space-x-4">
    <button class="w-40 py-4.5 border-2 active:border-0  bg-white border-black hover:bg-black hover:text-gold active:text-white disabled:border-gray-1 disabled:text-gray-1 disabled:bg-white active:translate-y-0.5 active:translate-x-0.5 rounded transform transition-all duration-300 text-base font-poppins font-semibold font-title">
      Remove
  </button>
    <button class=" py-4.5 w-40 border-2 active:border-0  bg-black text-white border-black hover:text-gold active:text-white disabled:bg-gray-2 disabled:text-white disabled:border-gray-2 rounded transform transition-all duration-300 text-base font-poppins font-semibold font-title">
      Save and apply
    </button>
  </div>
</form>
