@props([
  'variant' => 'spirit',
  'imgSrc' => @asset('/images/mock-data/cart-item-thumb.png'), 
  'notes' => ['Birtday discount -40%', 'Discount -35%'],
  ])
<div class="cart-item group flex lg:items-start items-stretch relative py-4 lg:border-b border-line box-content">
  <div class="product-thumbnail flex items-center bg-bg3 mr-3 w-24 py-4 px-1 h-36">
    <img src="{{ $imgSrc }}" class="object-cover " alt="Versus Savignon" />
  </div>
  
  <div class="flex lg:items-start flex-1 justify-between flex-col lg:flex-row lg:mr-24 lg:h-auto h-full">

    

    @switch($variant)
        @case('spirit')
          <x-cart.item-type-spirit />
            @break
        @case('ticket')
            <x-cart.item-type-ticket />
            @break
        @case('virtual')
            <x-cart.item-type-virtual />
            @break
        @default
            
    @endswitch
    
  </div>
  <div class="product-remove absolute top-5 right-0 mt-0.5 -mr-1 transition lg:opacity-0 group-hover:opacity-100">
    <button type="button" class="focus:outline-none">
      <span class="hidden lg:inline text-sm font-medium text-regular-2">Remove</span>
      <span class="icon-close lg:hidden text-gray-2 text-2xl"></span>
    </button>
  </div>  
</div>     