<div class="slide relative h-145 flex flex-col">
  <!-- When BE dev, use this as image
      @image(id, 'size', 'alt tage') -->
  <div class="hero-image relative">
    <img
      src="@asset('/images/mock-data/hero-slider-image.png')"
      class="object-cover object-center h-145 lg:w-full"
    />
    <div
      class="
        gradient-hero
        absolute
        h-full
        lg:h-1/3
        inset-0
        lg:inset-y-auto lg:bottom-0
      "
    ></div>
    <div
    class="
    w-full left-0
    absolute
    bottom-6
    lg:h-full lg:flex lg:justify-center lg:flex-col
    "
    >
    <div class="container">
        <div class="lg:max-w-7xl mx-auto">
          <h1 class="text-white mb-8  lg:text-5xl leading-x1.1 font-federo font-normal">
            Premium drinks<br> for people with<br> exceptional taste
          </h1>
          <a href="#"
            class="block "
          >
            <h5 class="gradient-prike-new px-7 py-5 mb-5 rounded lg:w-max text-title font-semibold w-full">See expert suggestions</h5>
          </a>
          <div class="slick-slider-dots-wrapper h-auto bg-gray-1 lg:hidden"></div>
        </div>
      </div>
      <div
        class="
          hidden
          slick-slider-dots-wrapper
          lg:block
          absolute
          bottom-0
          mx-auto
          w-full
        "
      ></div>
    </div>
  </div>

{{--  <x-hero-slider.list-box></x-hero-slider.list-box>--}}
</div>

<script></script>
