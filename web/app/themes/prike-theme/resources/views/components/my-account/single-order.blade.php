@props(['order'])

@php
  $currency = $order['currency'] == 'EUR' ? '&euro;' : $order['currency'];
  $grandTotal = $order['total'] + $order['taxes'] + $order['ecoPacking'] + $order['delivery'];
  $deliveryTextColor = $order['delivery'] > 0 ? '' : 'text-green';
  switch ($order['status']) {
    case 'Pending':
      $statusIconColor = 'text-beige-3';
      $icon = 'fa fa-circle-notch';
      $statusText = 'Pending';
      break;
    case 'Completed':
      $statusIconColor = 'text-green';
      $icon = 'fa fa-check';
      $statusText = 'Paid & Shipped';
      break;
    default:
      $statusIconColor = '';
      $icon = '';
      $statusText = '';
  }
@endphp

<div class="back-to-all-orders flex mb-8.75">
  <a class="text-sm font-medium leading-x1.2 text-black font-poppins self-center border-0" href="{{ home_url('my-account') }}">
    <i class="fa fa-chevron-left self-center mr-4"></i>
    Back to all orders
  </a>
</div>

<div class="flex flex-row mb-6">
  <h3 class="font-semibold">Order #{{ $order['orderId'] }}</h3>
  <a class="link-2 font-normal font-poppins ml-auto" href="#">Download PDF</a>
</div>

<div class="flex flex-row">
  <div class="font-poppins font-normal leading-x1.6 text-title">Date</div>
  <a class="link-2 font-normal font-poppins ml-auto text-title" href="#">{{ $order['orderDate']->format('d-m-Y') }}</a>
</div>

<hr class="my-5.5">

<div class="flex flex-row">
  <div class="font-poppins font-normal leading-x1.6 text-gray-1">Status</div>
  <div class="ml-auto">
    <i class="mr-2 self-center {{ $icon }} {{ $statusIconColor }}"></i>
    <span class="text-sm leading-x1.6 text-gray-1">{{ $order['status'] }}</span>
  </div>
</div>

<hr class="my-5.5">

<div class="flex flex-row">
  <div class="font-poppins font-normal leading-x1.6 text-title">Returns</div>
  <a class="link-2 font-normal font-poppins ml-auto text-title" href="#"><i class="fa fa-chevron-right"></i></a>
</div>

<hr class="my-5.5">

{{-- PRODUCT LIST --}}
<div class="flex flex-col rounded-lg border-line border p-4 font-poppins">
  @foreach($order['productList'] as $product)
    @php
      $finalPrice = $product['price'];
      if ($product['discount']) {
        $finalPrice = round($product['price'] * (1 - (float)$product['discountValue']/100), 2);
      }
    @endphp

    <div class="order-product-details flex flex-row mb-4.5">
      <div class="flex justify-center items-center w-25 h-35 bg-bg3">
        <img class="self-center" src="@asset($product['img'])">
      </div>

      <div class="product-title flex flex-col items-start ml-4 flex-grow-1 max-w-1/2">
        <h5 class="font-semibold mb-2.25">{{ $product['name'] }}</h5>
        <span class="text-sm leading-x1.6 font-normal mb-2.25">{{ $product['weight'] }}</span>
        <div class="py-1.5 px-2.5 bg-bg3 rounded mb-5">
          <span class="text-xs leading-x1.2 font-medium">
            @if($product['discount'])
              Discount -{{ $product['discountValue'] }}
            @else
              Discount does not apply for this item
            @endif
          </span>
        </div>

        <div class="product-quantity">
          {{ $product['quantity'] }}pcs
        </div>
      </div>

      <div class="total-line ml-4 text-right flex flex-col justify-between flex-grow-1">
        <div>
          @if ($product['discount'])
            <div class="text-xs leading-x1.6 text-regular-2">{{ $product['price'] }}{!! $currency !!}</div>
          @endif
          <div class="text-base leading-x1.2 font-semibold">{{ $finalPrice }}{!! $currency !!}</div>
        </div>

        <div class="product-btn">
          <button type="button" class="font-base leading-x1.6 py-4 px-4.75 border-2 border-black rounded">Add to cart</button>
        </div>
      </div>
    </div>
    <hr class="my-5.5">
  @endforeach

  {{-- TOTALS --}}
  <div class="subtotal flex flex-row font-semibold text-base leading-x1.6">
    <div>Subtotal</div>
    <div class="ml-auto">{{ $order['total'] }}{!! $currency !!}</div>
  </div>

  <div class="taxes flex flex-row mb-1 text-sm leading-x1.6">
    <div>Taxes</div>
    <div class="ml-auto">{{ number_format($order['taxes'], 2) }}{!! $currency !!}</div>
  </div>

  <div class="packing flex flex-row mb-1 text-sm leading-x1.6">
    <div>Eco packing</div>
    <div class="ml-auto">{{ number_format($order['ecoPacking'], 2) }}{!! $currency !!}</div>
  </div>

  <div class="packing flex flex-row mb-1 text-sm leading-x1.6">
    <div>Delivery</div>
    <div class="ml-auto {{ $deliveryTextColor }}">
      @if($order['delivery'] > 0)
        {{ number_format($order['delivery'], 2) }}{!! $currency !!}
      @else
        Free
      @endif
    </div>
  </div>

  <hr class="my-5.5 border-dashed">

  <div class="grand-total flex flex-row font-semibold text-base leading-x1.2">
    <div>Total</div>
    <div class="ml-auto">{{ $grandTotal }}{!! $currency !!}</div>
  </div>
</div>

{{-- DELIVERY --}}
<h5 class="mt-5 mb-2 font-semibold">Delivery</h5>
<div class="flex flex-col rounded-lg border-line border p-4 font-poppins">
  <div class="flex flex-row items-center mb-14">
    <div class="flex flex-row flex-1 items-center">
      <img class="h-full mr-3.25" src="@asset($order['deliveryCo']['deliveryCoImg'])">
      <div class="flex flex-col">
        <h5 class="font-semibold leading-x1.6">{{ $order['deliveryCo']['name'] }}</h5>
        <span class="text-sm text-regular-2 leading-x1.4">Ships today {{ $order['deliveryCo']['shippingHours'] }}</span>
      </div>
    </div>

    <div class="flex-1 flex-grow-0.5">
      @if($order['delivery'] > 0)
        <span>{{ $order['delivery'] }}{!! $currency !!}</span>
      @else
        <span class="bg-green text-white py-1.75 px-2.5 rounded-100 text-sm leading-x1.2">Free</span>
      @endif
    </div>

    <div class="flex-1">
      @if($order['ecoPacking'] > 0)
        <div class="bg-bg3 rounded p-4">ECO friendly packaging <span class="font-semibold">+{{ number_format($order['ecoPacking'], 2) }}{!! $currency !!}</span></div>
      @endif
    </div>
  </div>

  <div class="flex flex-row font-poppins mb-14">
    <div class="flex flex-col flex-1">
      <h5 class="font-semibold">Delivery address:</h5>

      <span class="text-sm text-regular-2 leading-x1.4">{{ $order['deliveryAddress']['name'] }}</span>
      <span class="text-sm text-regular-2 leading-x1.4">{{ $order['deliveryAddress']['dateOfBirth']->format('d.m.Y') }}</span>
      <span class="text-sm text-regular-2 leading-x1.4">{{ $order['deliveryAddress']['address1'] }}</span>
      <span class="text-sm text-regular-2 leading-x1.4">{{ $order['deliveryAddress']['address2'] }}</span>
      <span class="text-sm text-regular-2 leading-x1.4">{{ $order['deliveryAddress']['county'] }}, {{ $order['deliveryAddress']['country'] }}</span>
      <span class="text-sm text-regular-2 leading-x1.4">{{ $order['deliveryAddress']['email'] }}</span>
      <span class="text-sm text-regular-2 leading-x1.4">{{ $order['deliveryAddress']['phone'] }}</span>
    </div>

    <div class="pt-1.5 flex-1 flex-grow-0.5">
      <span class="py-1.5 px-2.5 rounded bg-secondary-2 font-medium text-sm leading-x1.2">Private</span>
    </div>

    <div class="flex-1">
      <h5 class="font-semibold">Invoice's recipient's address:</h5>
      <span class="text-sm text-regular-2 leading-x1.4">
        Invoice recipient address is the same <br>
        delivery address
      </span>
    </div>
  </div>
</div>

{{-- Payment --}}
<h5 class="mt-5 mb-2 font-semibold">Payment</h5>
<div class="flex flex-col rounded-lg border-line border p-4 font-poppins">
  <div class="flex flex-row items-center">
    <div class="flex flex-row flex-1 items-center">
      <img class="h-full mr-3.25" src="@asset($order['payment']['bankImg'])">
      <div class="flex flex-col">
        <h5 class="font-semibold leading-x1.6">{{ ucfirst($order['payment']['type']) }}</h5>
        <span class="text-sm text-regular-2 leading-x1.4">{{ $order['payment']['name'] }}</span>
      </div>
    </div>
  </div>
</div>

<div class="flex flex-row py-6">
  @php
    global $wp;
    $url = home_url($wp->request);
    $queryArgs = [
      'orderId' => urlencode($order['orderId']),
      'orderTrackId' => urlencode($order['orderTrackId']),
    ];
  @endphp
  <a href="{{ add_query_arg($queryArgs, $url) }}" class="px-10 py-5 mr-6 w-auto border-2 active:border-0 bg-gray-1 border-gray-1 text-white rounded transform transition-all duration-300 text-base font-poppins font-semibold font-title" tabindex="0">
    <h5 class="leading-x1.2">Track my order</h5>
  </a>
  <button class="w-full gradient-prike-new px-10 py-5 rounded lg:w-max" tabindex="0">
    <h5 class="leading-x1.2">Reorder all items</h5>
  </button>
</div>
