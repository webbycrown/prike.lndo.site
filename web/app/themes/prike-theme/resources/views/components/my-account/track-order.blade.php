@props(['order'])

<div class="back-to-all-orders flex mb-8.75">
  <a class="text-sm font-medium leading-x1.2 text-black font-poppins self-center border-0" href="{{ home_url('my-account') }}">
    <i class="fa fa-chevron-left self-center mr-4"></i>
    Back
  </a>
</div>

<h3 class="font-semibold">Track Order #{{ $order['orderId'] }}</h3>

<hr class="my-4">

<div class="flex flex-col font-poppins">
  <div class="flex flex-row items-center mb-3.25 text-sm leading-x1.6 text-regular-2">
    <div class="flex flex-1">Courier</div>
    <div class="flex flex-1">Destination</div>
    <div class="flex flex-1 flex-grow-0.5 justify-end">Tracking ID</div>
  </div>

  <div class="flex flex-row items-center">
    <div class="flex flex-row flex-1 items-center">
      <img class="h-full mr-3.25" src="@asset($order['deliveryCo']['deliveryCoImg'])">
      <span class="text-sm leading-x1.6 font-basic">{{ $order['deliveryCo']['name'] }}</span>
    </div>

    <div class="flex flex-col flex-1 text-sm leading-x1.6">
      <span>{{ $order['deliveryAddress']['address1'] }}</span>
      <span>{{ $order['deliveryAddress']['address2'] }}{{ $order['deliveryAddress']['county'] }}, {{ $order['deliveryAddress']['country'] }}</span>
    </div>

    <div class="flex flex-1 flex-grow-0.5 justify-end text-sm leading-x1.4 text-gold-2 font-medium">
      {{ $order['orderTrackId'] }}
    </div>
  </div>
</div>

<hr class="my-4">

{{-- TIMELINE --}}
<div class="font-poppins">
  @php
    $firstChild = 0;
    $lastChild = count($order['trackingDetails']) - 1;
  @endphp

  @foreach($order['trackingDetails'] as $key => $trackingDetail)
    @php
      $bgColor = $key == $firstChild ? 'bg-gold' : 'bg-regular-3';
      $border = $key == $lastChild? '' : 'border-dashed border-gray-2 border-l';
    @endphp
    <div class="timeline-item-container">
      <div class="flex flex-col">
        <div class="flex flex-row items-center pl-5.5 relative">
          <div class="absolute -left-1.5 bg-white py-2">
            <div class="w-3.75 h-3.75 rounded-full {{ $bgColor }}"></div>
          </div>
          <span class="text-sm leading-x1.6 text-regular-2">{{ $trackingDetail['date']->format('d.m.Y') }}</span>
        </div>
        <div class="flex flex-col pl-5.5 {{ $border }}">
          <span class="text-base leading-x1.6 font-semibold">{{ $trackingDetail['name'] }}</span>
          <span class="text-sm leading-x1.6 pb-6">{{ $trackingDetail['description'] }}</span>
        </div>
      </div>
    </div>
  @endforeach
</div>
