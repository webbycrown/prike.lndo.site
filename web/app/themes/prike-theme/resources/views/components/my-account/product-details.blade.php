@props(['productData', 'currency'])

@php
  if ($currency == 'EUR') {
    $currency = '&euro;';
  }

  $finalPrice = $productData['price'];
  if ($productData['discount']) {
    $finalPrice = round($productData['price'] * (1 - (float)$productData['discountValue']/100), 2);
  }
@endphp

<div class="order-product-details flex flex-row mb-4.5">
  <div class="flex justify-center items-center" style="width: 109px; height: 130px; background-color: #f4f4f4;">
    <img class="self-center" src="@asset($productData['img'])">
  </div>
  <div class="product-title flex flex-col items-start ml-4 flex-grow-1 max-w-1/2">
    <h5 class="font-semibold mb-2.25">{{ $productData['name'] }}</h5>
    <span class="text-sm leading-x1.6 font-normal mb-2.25">{{ $productData['weight'] }}</span>
    <div class="py-1.5 px-2.5 bg-bg3 rounded">
      <span class="text-xs leading-x1.2">
        @if($productData['discount'])
          Discount -{{ $productData['discountValue'] }}
        @else
          Discount does not apply for this item
        @endif
      </span>
    </div>
  </div>
  <div class="product-quantity flex ml-4 flex-grow-0.5">
    {{ $productData['quantity'] }} pcs
  </div>
  <div class="total-line ml-4 text-right">
    @if ($productData['discount'])
      <div class="text-xs leading-x1.6 text-regular-2">{{ $productData['price'] }}{!! $currency !!}</div>
    @endif
    <div class="text-base leading-x1.2 font-semibold">{{ $finalPrice }}{!! $currency !!}</div>
  </div>
</div>
