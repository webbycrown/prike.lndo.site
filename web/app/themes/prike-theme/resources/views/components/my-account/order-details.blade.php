@props(['order'])

@php
  global $wp;
  $url = home_url($wp->request);
  $totalOrders = count($order['productList']);
  $currency = $order['currency'] == 'EUR' ? '&euro;' : $order['currency'];
  switch ($order['status']) {
    case 'Pending':
      $statusTextColor = 'text-regular-2';
      $statusIconColor = 'text-beige-3';
      $icon = 'fa fa-circle-notch';
      break;
    case 'Completed':
      $statusTextColor = $statusIconColor = 'text-green';
      $icon = 'fa fa-check';
      break;
    default:
      $statusTextColor = '';
      $statusIconColor = '';
      $icon = '';
  }
@endphp

<div class="order-details flex flex-col rounded-lg border-line border mb-4 p-4 w-full bg-white">
  <div class="order-header flex flex-row items-center font-poppins mb-6">
    <span class="font-base font-semibold leading-x1.6 pr-2.5">ID {{ $order['orderId'] }}</span>
    <span class="font-base font-normal leading-x1.6">{{ $order['orderDate']->format('d.m.Y') }}</span>
    <a href="{{ esc_url(add_query_arg(['orderId' => urlencode($order['orderId'])], $url)) }}" class="text-gold-2 font-normal text-sm ml-auto">View</a>
  </div>

  <div class="order-items-container font-poppins mb-1.75">
    @for($i = 0; $i < 2; $i++)
      @isset($order['productList'][$i])
        <x-my-account.product-details :productData="$order['productList'][$i]" :currency="$order['currency']"/>
      @endisset
    @endfor
  </div>

  <div class="order-other-details flex flex-row font-poppins">
    @if($totalOrders > 2)
      <span class="text-sm leading-x1.6">+{{ $totalOrders - 2 }} more item/s</span>
    @endif
    <div class="ml-auto">
      <span class="mr-2 text-sm leading-x1.6 {{ $statusTextColor }}">{{ $order['status'] }}</span>
      <i class="self-center {{ $icon }} {{ $statusIconColor }}"></i>
    </div>
  </div>

  <hr class="my-5.5">

  <div class="order-totals flex flex-row font-poppins">
    <div class="flex flex-row">
      <span class="font-base font-semibold mr-2">Total</span>
      @if($order['discount'])
        <div class="bg-secondary-2 rounded-100">
          <span class="text-sm leading-none my-2.5 mx-1.5">-{{ $order['discountValue'] }}</span>
        </div>
      @endif
    </div>
    <span class="ml-auto text-base leading-x1.4 font-semibold">{{ $order['total'] }}{!! $currency !!}</span>
  </div>
</div>
