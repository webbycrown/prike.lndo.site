@props(['icon', 'title', 'active'])

@php
  $activeColorClass = $active ? 'text-gold-1': '';
@endphp

<div class="p-4 w-full flex flex-row {{ $activeColorClass }}">
  <i class="fa fa-{{ $icon }} self-center pr-4"></i>
  <span class="font-poppins text-base font-semibold">{{ $title }}</span>
  <i class="fa fa-chevron-right self-center ml-auto"></i>
</div>
