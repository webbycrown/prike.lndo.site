@props(['date_posted' => '14 - 20 June 2021', 
        'title' => 'Taste Spain ', 
        'after_sale' => 'off for all Spanish  wine',
        'befor_sale' => '',
        'sale' => false,
        'button_label' => 'Shop now',
        'imgSrc' => @asset('/images/mock-data/banner.jpg'),
        ])
  <div class="banner-image sm:h-132 h-145 relative bg-cover bg-no-repeat rounded bg-center" style="background-image: url({{ $imgSrc }})">
    <div class="banner-body absolute inset-0 flex  sm:items-center">
      <div class="px-4 lg:px-20 sm:px-6 py-7">
        <h5 class="subtitle mb-5 text-white opacity-70 leading-x1.1 font-medium">{{ $date_posted }}</h5>
        <h1 class="text-white mb-5 font-normal lg:text-5xl text-3xl lg:leading-x1.2 leading-x1.1">{{ $title }}</h1>
        <p class="text-white mb-9 lg:text-3xl font-playfair opacity-80 lg:leading-x1.1 leading-x1.6 lg:tracking-normal tracking-wider">
          @if ($befor_sale)
              <span>{{ $before_sale }}</span>
          @endif
          @if ($sale)
          <span class="text-gold text-xl lg:text-3xl">-{{ $sale }}%</span>
          @endif
          @if ($after_sale)
              <span>{{ $after_sale }}</span>
          @endif
        </p>
        <div class="w-full md:w-auto">
          <x-controls.button variant="gold" >{{ $button_label }}</x-controls.button>

        </div>
  
      </div>
    </div>
  </div>
