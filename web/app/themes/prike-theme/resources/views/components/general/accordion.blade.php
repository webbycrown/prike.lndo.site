@props([ 'title' => '' ])

<div class="accordion border-t border-line">
  <div class="accordion-title py-5 flex justify-between cursor-pointer">
    <div class="title">
      <h4>{{ $title }}</h4>
    </div>
    <div class="caret">
      <img src="@asset('/images/icons/down.svg')" class="caret-down transition duration-500" />
    </div>
  </div>
  <div class="accordion-content hidden">
    {{ $slot }}
  </div>
</div>
