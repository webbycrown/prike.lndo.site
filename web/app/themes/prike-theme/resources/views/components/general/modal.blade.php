@props(['id' => ''])
<div class="pmodal" id="modal-{{ $id }}" aria-hidden="true">
  <div class="pmodal__wrap">
    <div class="pmodal__window rounded-lg" role="dialog" aria-modal="true">
      <button data-pclose class="pmodal__close outline-none focus:outline-none">
        <span class="icon-close text-2xl"></span>
      </button> 
      {{ $slot }} 
    </div>
  </div> 
</div>