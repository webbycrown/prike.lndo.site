@props([
    'noDefaultBackground' => false,
    'bg' => 'bg3',
    'padding' => '4',
    'paddingX',
    'paddingY',
])

@php
$cardClasses = ['rounded p-' . $padding, $noDefaultBackground ? '' : 'bg-' . $bg];
@endphp

<div {{ $attributes->merge(['class' => implode(' ', $cardClasses)]) }}>
    {{ $slot }}
</div>
