@props([
'badges' => null,
'imgSrc' => null,
'brandSrc'
])

<div class="single-product-slide">
  <div class="slide-header flex justify-between">
    <div class="badges">
      @foreach($badges as $badge)
      <x-elements.tag class="mr-2" fill="secondary-1" border="secondary-1" text="title" uppercase>{{ $badge }}</x-elements.tag>
      @endforeach
    </div>
    <div class="brand"><img src="@asset($brandSrc)" alt="Brand name" /></div>
  </div>
  <div class="slide-image flex justify-center pt-4 pb-6">
    <img src="@asset($imgSrc)" class="h-product-card-img" />
  </div>
</div>