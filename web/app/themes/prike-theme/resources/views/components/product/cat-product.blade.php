@props(['tag'=> false, 'cart' => false] )
<div class="cat-product w-full">
  <div
    class="
      product-thumb
      h-92.5
      bg-bg3
      w-full
      max-w-80
      rounded
      mb-3
      flex flex-col
      items-center
      justify-center 
      p-5
    "
  >
  @if ($tag)
      
  <div class="badges-area flex justify-between w-full">
    <div class="product-badge">
      <x-elements.tag fill="secondary-1" border="0" text="title"
        >Uus</x-elements.tag
      >
    </div>
    <div class="product-wishlist">
      <button type="button">

        <span class="icon-wishlist text-line text-2xl"></span>
      </button>
    </div>
  </div>
  @else

  <div class="badges-area text-right w-full">
    
    
    <div class="product-wishlist inline-block">
      <span class="icon-wishlist text-line text-2xl"></span>
    </div>
  </div>
  @endif
    <img
      src="@asset('/images/mock-data/product-card-thumb.png')"
      class="pl-4"
    />
  </div>
  <div class="product-data">
    <p class="product-cat body-2 text-regular-2 capitalize">šampanja</p>
    <div class="flex justify-between">

      <div class="product-data-left">
        <h3 class="mb-1 lg:font-semibold ">Zonin Primitivo Puglia</h3>
        <p class="body-2 mb-5 text-regular-2">
          <span>Italia</span>
          <span>/</span>
          <span>Colli Orientali del Friuli</span>
        </p>
        <div class="flex items-center justify-between">
  
          <h4 class="product-price text-title font-semibold text-base">155.22€</h4>
          
        </div>
      </div>
      <div class="product-data-right flex flex-col items-end justify-between">
        <p class="body-2 whitespace-nowrap pt-0.5">
          <span>0.75 L</span>
          <span>/</span>
          <span>12.5°</span>
        </p>

          @if ($cart)
            <p class="-mb-4 ">

              <x-controls.button-circle>
                <span class="icon-bag-1 text-2xl font-gray-1"></span>
              </x-controls.button-circle>
            </p>
          @endif
      </div>
    </div> 
  </div>
</div>
