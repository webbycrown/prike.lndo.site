<div class="rating flex mb-4">
    <div class="stars flex mr-2">
        <img src="@asset('/images/mock-data/star-filled.svg')" />
        <img src="@asset('/images/mock-data/star-filled.svg')" />
        <img src="@asset('/images/mock-data/star-filled.svg')" />
        <img src="@asset('/images/mock-data/star-filled.svg')" />
        <img src="@asset('/images/mock-data/star.svg')" />
    </div>
    <p class="body-2 text-regular-2">98 arvustust</p>
</div>
