<div id="review" class="mb-11">
  <h5 class="mb-3">The PERFECT wine</h5>
  <div id="rating" class="mb-3 flex">
    <img src="@asset('/images/mock-data/star-filled.svg')" />
    <img src="@asset('/images/mock-data/star-filled.svg')" />
    <img src="@asset('/images/mock-data/star-filled.svg')" />
    <img src="@asset('/images/mock-data/star-filled.svg')" />
    <img src="@asset('/images/mock-data/star-filled.svg')" />
  </div>
  <div id="name" class="flex justify-between mb-3">
    <p>Naomi</p>
    <p class="body-2 text-regular-2 flex">
      <span class="mr-1"
        ><img src="@asset('/images/icons/user-verified.svg')" /></span
      >Verifitseeritud ostja
    </p>
  </div>
  <p class="body-2 text-regular-2 mb-4"></p>
  <p class="review-text truncate max-h-22 body-2 text-regular mb-4">
    This wine is absolutely amazing!!! I have looked at this a multiple times
    but hesitated to order. I am so glad I did and I will now order it every
    Friday until the end of the year.
  </p>
  <button class="review-read-more text-link-2 mb-6 active:border-0">
    Loe edasi
  </button>
  <div class="review-feedback flex justify-between">
    <p class="body-2 text-regular-2">Kas see arvustus oli väärtuslik?</p>
    <div class="review-vote flex space-x-3">
      <button><img src="@asset('/images/icons/thumbs-up.svg')" /></button>
      <p class="body-2 text-regular-2">45</p>
    </div>
  </div>
</div>
