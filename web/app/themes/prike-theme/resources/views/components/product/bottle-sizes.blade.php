<div id="bottle-sizes" class="mb-8">
    <p class="text-regular caption mb-2">Vali suurus</p>
    <div class="sizes-wrapper flex space-x-4 overflow-visible">
        <x-product.product-size active />
        <x-product.product-size badge="wow" />
        <x-product.product-size badge="wow" />
    </div>
</div>
<div id="order-status" class="flex py-2 space-x-2">
    <img src="@asset('/images/icons/delivery.svg')" alt="Delivery status" />
    <div class="text-green caption font-medium">Laos ja saadame täna teele</div>
</div>
