@props([
'title', 'link'
])
<div class="product-slider-wrapper">
  <div class="product-slider-header flex justify-between items-end mb-6">
    @isset($title)
    <div class="title">
      <h2 class="text-title font-size-2 font-normal">{{ $title }}</h2>
    </div>
    @endisset
    @isset($link)
    <div class="link">
      <a href="{{ $link }}" class="link-2 font-semibold font-poppins">Vaata</a>
    </div>
    @endisset
  </div>
  <div class="product-slider w-full overflow-x-visible">
    <x-product.product-card outOfStock />
    <x-product.product-card />
    <x-product.product-card />
    <x-product.product-card />
    <x-product.product-card />
  </div>
</div>