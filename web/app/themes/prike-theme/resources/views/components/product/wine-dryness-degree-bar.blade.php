@props([
'wineDryness' => null
])
@isset($wineDryness)
<div id="wine-dryness-degree-bar">
  <h5>Stiil</h5>
  <div class="bar flex">
    <div class="dry w-1/3 text-center">
      <div class="color-bar h-10px rounded-l-full {{ $wineDryness === 'dry' ? 'bg-red' : 'bg-pink' }}"></div>
      <p class="text-regular py-4">Kuiv</p>
    </div>
    <div class="
        semi-sweet
        w-1/3   
        text-center
      ">
      <div class="color-bar h-10px
      {{ $wineDryness === 'semi' ? 'bg-red' : 'bg-pink' }}
      border-l-2 border-r-2 border-gray-1"></div>
      <p class="text-regular py-4">Poolkuiv</p>
    </div>
    <div class="semi-sweet  rounded-r-full w-1/3 text-center">
      <div class="color-bar rounded-r-full h-10px {{ $wineDryness === 'sweet' ? 'bg-red' : 'bg-pink' }}"></div>
      <p class="text-regular py-4">Magus või poolmagus</p>
    </div>
  </div>
</div>
@endisset