@props(['title' => null, 'simpleData' => false])



@if($title)
  <div class="description-section py-5 border-t border-line flex justify-between">
    <div class="description-section-title w-1/4">
      <h5 class="subtitle text-regular-2">
        {{ $title }}
      </h5>
    </div>
    <div class="description-section-content w-3/5 {{ $simpleData ? 'text-right' : ''}} ">
      <p class="text-title">{{ $slot }}</p>
    </div>
  </div>
@else
  <div class="description-section py-5 border-t border-line">
    {{ $slot }}
  </div>
@endif