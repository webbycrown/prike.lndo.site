{{-- Temporary arrays --}}
@php
$badges1 = ['new', 'second'];
$badges2 = ['new'];
$badges3 = ['another'];
@endphp


<div id="single-product-slider" class="container pt-4 bg-bg3">
  <x-product.single-product-slider-slide imgSrc="/images/mock-data/single-product-product-image.png" brandSrc="/images/mock-data/single-product-brand.png" :badges="$badges1" />
  <x-product.single-product-slider-slide imgSrc="/images/mock-data/single-product-product-image.png" brandSrc="/images/mock-data/single-product-brand.png" :badges="$badges2" />
  <x-product.single-product-slider-slide imgSrc="/images/mock-data/single-product-product-image.png" brandSrc="/images/mock-data/single-product-brand.png" :badges="$badges3" />
</div>
<div class="single-product-slider-dots bg-bg3 pb-3"></div>