<x-product.wine-dryness-degree-bar />
<x-product.wine-dryness-degree-bar wineDryness="dry" />
<x-product.wine-dryness-degree-bar wineDryness="semi" />
<x-product.wine-dryness-degree-bar wineDryness="sweet" />

<x-product.description-section title="Alkoholi" simpleData>
    12.5%
</x-product.description-section>
<x-product.description-section title="Piirkond" simpleData>
    Prantsusmaa
</x-product.description-section>
<x-product.description-section title="Maitse">
    Vein kehtestab end kohe oma rikkaliku, täieliku ja ulatusliku kohaloluga. Domineerib mahlasus, kuna
    taktiilsed aistingud asenduvad kiiresti aromaatsusega. Maitse kese avaneb küllusliku, kindla ja
    kontrollituna. Seejärel see aheneb, lastes veinil vibreerida vürtsika ja piprasena, kergelt soolakas
    järelmaitsese.
</x-product.description-section>
<x-product.description-section>
    This is content for accordion
</x-product.description-section>
