<div class="bg-bg3 flex flex-wrap justify-center space-x-20 border-b-2 border-regular-2">
    <button class="pb-4 border-b border-2 active:border-gold-2">Description</button>
    <button class="pb-4 border-b border-2 active:border-gold-2">Description</button>
    <button class="pb-4 border-b border-2 active:border-gold-2">Description</button>
    <button class="pb-4 border-b border-2 active:border-gold-2">Description</button>
</div>
<div class="product-data">
    <div id="product-description" class="pt-13 pb-24 flex justify-center">
        <div class="w-1/2">
            <x-product.data-description />
        </div>
    </div>
    <div id="product-history" class="hidden pt-13 pb-24"></div>
    <div id="product-reviews" class="hidden pt-13 pb-24"></div>
    <div id="product-social" class="hidden pt-13 pb-24"></div>

</div>
