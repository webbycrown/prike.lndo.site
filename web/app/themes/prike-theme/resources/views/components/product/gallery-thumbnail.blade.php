@props([
    'thumbImg' => 'images/mock-data/single-product-gallery-thumb.png',
    'productAlt' => 'Product alt description',
    'active' => false,
])

<div
    class="w-16 h-16 flex justify-center items-center {{ $active ? 'border-gold-2 border-2 bg-bg2' : 'border-line border' }}  rounded cursor-pointer">
    <img src="@asset($thumbImg)" alt="{{ $productAlt }}">
</div>
