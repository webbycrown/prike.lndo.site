<div class="flex flex-wrap items-center">
    <div class="w-28 mr-4">
        <x-general.quantity-counter />
    </div>

    <x-controls.button variant="gold" narrow class="h-16 px-13 mr-9">Add to cart</x-controls.button>

    <span class="icon-wishlist text-xl"></span>
</div>
