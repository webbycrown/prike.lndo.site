@props([ 'active' => false, ])

<div
  class="size p-3 pr-7 rounded w-auto inline-block mr-4 {{
    $active ? 'border-gold border-2 bg-bg1' : 'border-gray-2 border'
  }}"
>
  <p class="text-link-2 mb-2">1L pudel</p>
  <p class="text-regular-2">195.22€</p>
</div>
