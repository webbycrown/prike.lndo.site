@props(['outOfStock' => false])

<div class="product-card mx-2 w-9/12 cursor-pointer relative {{ $outOfStock and 'disabled' }}">
    <div class="product-thumb h-92.5 bg-bg3 w-full max-w-80 rounded mb-4 flex flex-col items-center justify-center p-5">
        <div class="badges-area flex justify-between w-full">
            <div class="product-badge">
                <x-elements.tag fill="secondary-1" border="0" text="title" uppercase>Uus</x-elements.tag>
            </div>
            <div class="product-wishlist">
                <img src="@asset('/images/icons/wishlist.svg')" class="h-4.25" />
            </div>
        </div>
        <img src="@asset('/images/mock-data/product-card-thumb.png')" class="pl-4" />
    </div>
    <div class="product-data flex justify-between group relative">
        <div class="product-data-left">
            <p class="text-xs leading-x1.6 font-normal text-regular-2">šampanja</p>
            {{-- Refactor note: Add stretched-link --}}
            <a href="{{ get_home_url() . '/product/wordpress-pennant/' }}" class="text-grey-4">
                <h3 class="mb-2 font-bold">Zonin Primitivo Puglia</h3>
            </a>

            <p class="text-sm leading-x1.4 mb-7 font-normal text-regular-2">Italia / Colli Orientali del Friuli</p>
            @if ($outOfStock)
                <h4 class="product-price text-red mb-2">Out of stock</h4>
                <x-controls.button class="relative bottom-0 right-0 !px-4 !py-2 lg:block" narrow>Notify of availability
                </x-controls.button>
            @else
                <h4 class="product-price text-title mb-4">155.22€</h4>
                <x-controls.button-circle class="lg:hidden">
                    <img src="@asset('/images/icons/bag.svg')" />
                </x-controls.button-circle>
                <x-controls.button
                    class="relative bottom-0 right-0 hidden w-min-content lg:group-hover:block lg:py-2 lg:px-6"
                    variant="gold" narrow>
                    Add to cart
                </x-controls.button>
            @endif
        </div>
        <div class="product-data-right flex flex-col justify-between items-end lg:w-28">
            <p class="text-smm leadng-x1.4 font-semibold text-regular-2">0.75 L / 12.5°</p>
        </div>
    </div>
</div>
