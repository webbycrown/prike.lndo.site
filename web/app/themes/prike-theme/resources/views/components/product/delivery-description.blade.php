<div id="delivery-selector" class="flex pt-4">
    <h5 class="delivery-tallinn border-b-4 border-gold-2 flex-grow px-6 pb-4 cursor-pointer">Tallinn ja
        ümbrus</h5>
    <h5 class="delivery-estonia text-regular-2 font-normal border-b-4 border-gray-2 flex-grow px-6 pb-4 cursor-pointer">
        Üle
        Eesti</h5>
</div>
<div id="delivery-description">
    <div class="xl:hidden">
        <div id="" class="delivery-tallinn-description text-center px-12 pt-8 pb-6">
            <img src="@asset('/images/mock-data/single-product-delivery-badge.svg')" class="mx-auto mb-4" />
            <h5 class="mb-4">Kiire tarne samal päeval</h5>
            <p class="body-2 mb-6">Tellides täna enne kl 12.00, saab
                valida tarneajaks Tallinnas täna
                kl 14-17 ja 17-20.
            </p>
            <p class="body-2">
                Tellides kl 12:00:01-15:00, saab
                valida tarneajaks Tallinnas täna
                kl 17-20.</p>
        </div>
        <div id="" class="delivery-estonia-description text-center px-12 pt-8 pb-6 hidden">
            <img src="@asset('/images/mock-data/single-product-delivery-badge.svg')" class="mx-auto mb-4" />
            <h5 class="mb-4">Kiire tarne</h5>
            <p class="body-2 mb-6">Tellides täna enne kl 12.00, saab
                valida tarneajaks üle Eesti täna
                kl 14-17 ja 17-20.
            </p>
            <p class="body-2">
                Tellides kl 12:00:01-15:00, saab
                valida tarneajaks üle Eesti täna
                kl 17-20.</p>
        </div>
    </div>
    <div class="hidden xl:block py-6">
        <div class="delivery-tallinn-description">
            <p class="body-2 mb-4"><strong>Tallinn</strong>Fast, same day delivery > If you order today before 12:00,
                you can
                choose the delivery time in Tallinn today 14-17 and 17-20. When ordering from 12:00:01-15:00, you can
                choose the delivery time in Tallinn today
                17-20.</p>

            <p class="text-regular-2">Tähelepanu! Tegemist on alkoholiga ja alkohol võib kahjustada teie tervist.</p>
        </div>
        <div id="" class="delivery-estonia-description hidden">
            <p class="body-2 mb-4"><strong>Estonia</strong>Fast, same day delivery > If you order today before 12:00,
                you can
                choose the delivery time in Tallinn today 14-17 and 17-20. When ordering from 12:00:01-15:00, you can
                choose the delivery time in Tallinn today
                17-20.</p>

            <p class="text-regular-2">Tähelepanu! Tegemist on alkoholiga ja alkohol võib kahjustada teie tervist.</p>
        </div>

    </div>

</div>
