<div class="
    flex flex-col
    bg-cover xl:bg-auto
    {{-- bg-expert-bg-gray-mob --}}
    xl:bg-cover xl:bg-no-repeat xl:bg-center
    md:px-5
    md:mt-12
    xl:px-20
    h-auto xl:h-132
    xl:grid
    xl:grid-flow-col
    xl:grid-cols-12
    xl:relative"
    style="background-image: url(@asset('/images/mock-data/expert-bg.svg'));">
    <div class="heading text-left mb-3 xl:col-start-1 xl:col-end-4 xl:row-start-1 xl:row-end-3 px-5 md:px-0 pt-7 xl:pt-0
      flex flex-col self-end items-center xl:items-baseline xl:pb-12"
        style="z-index: 3;">
        <h2 class="text-gray-1 text-2.5xl xl:font-size-3 xl:leading-x1.2 font-thin mb-5 xl:full">
            Küsi joogieksperdilt
        </h2>
        <p class="leading-x1.6 text-gray-1 mb-6 text-center xl:text-left xl:full">
            Soovid tasuta nõu oma ala proffidelt? Küsi Prike joogiekspedilt nõu ja
            leia enda jaoks see õige.
        </p>

        <x-controls.button variant="gray-2">Vaata soovitusi</x-controls.button>

        <div class="tags my-4 flex justify-center xl:justify-start xl:w-full">
            <x-elements.tag class="mx-2 mb-4 flex items-center justify-center leading-0 h-6.25" borderBold>Vein
            </x-elements.tag>
            <x-elements.tag class="mx-2 mb-4 flex items-center justify-center leading-0 h-6.25" borderBold>
                Kokteilid
            </x-elements.tag>
            <x-elements.tag class="mx-2 mb-4 flex items-center justify-center leading-0 h-6.25" borderBold>Gin
            </x-elements.tag>
            <x-elements.tag class="mx-2 mb-4 flex items-center justify-center leading-0 h-6.25" borderBold>Üllatus
            </x-elements.tag>
        </div>
    </div>

    <div class="expert text-center xl:text-left xl:col-start-10 xl:col-end-13 xl:row-start-1 xl:row-end-2 px-5 md:px-0 xl:absolute xl:top-20"
        style="z-index: 3">
        <div class="name">
            <h4 class="text-title mb-3 font-semibold">Kristjan Peäske</h4>
        </div>
        <div class="position mb-4">
            <p class="body-2 text-grey-1">
                Lore Bistroo ja Leib Restorani peremees, Kolmekordne Eesti ja kahekordne
                Baltikumi meistersommeljee
            </p>
        </div>
        <div class="awards flex justify-center xl:justify-start space-x-3 mb-9">
            <img src="@asset('/images/mock-data/expert-badge-4.svg')" />
            <img src="@asset('/images/mock-data/expert-badge-1.svg')" />
            <img src="@asset('/images/mock-data/expert-badge-2.svg')" />
            <img src="@asset('/images/mock-data/expert-badge-3.svg')" />
        </div>
    </div>


    <div class="relative xl:absolute h-96 xl:h-full w-full overflow-hidden md:overflow-auto xl:-right-8">
        <div class="inline-block ml-auto mr-auto right-0 md:block absolute h-96 w-96 xl:w-auto xl:h-full truncate expert-1"
            style="z-index: 1;">
            <img src="@asset('/images/mock-data/expert-person.png')"
                class="h-full xl:my-0 xl:mx-auto xl:right-0 expert-1 xl:absolute" />
        </div>
        <div class="inline-block ml-auto mr-auto right-0 md:block absolute h-96 w-96 xl:w-auto xl:h-full truncate expert-2"
            style="z-index: 2;">
            <img src="@asset('/images/mock-data/expert-person-2.png')"
                class="h-full xl:my-auto xl:mx-0 xl:right-0 expert-2 xl:absolute" />
        </div>
    </div>
</div>
