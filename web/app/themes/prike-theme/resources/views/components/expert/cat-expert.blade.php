<div
  class="
    flex flex-col
    bg-expert-bg-gray-mob
    lg:bg-cover lg:bg-no-repeat lg:bg-center
    px-5
    pt-7
    pb-0
    lg:p-22
    lg:grid lg:grid-flow-col lg:grid-cols-12
    lg:relative 
  "
>
  <div class="heading text-center mb-3 lg:col-start-2 lg:col-end-5 lg:row-start-2 lg:row-end-3">
    <h2 class="text-gray-1 font-normal text-2.5xl mb-5">
      Küsi joogieksperdilt
    </h2>
    <p class="leading-x1.6 text-gray-1 mb-6">
      Soovid tasuta nõu oma ala proffidelt? Küsi Prike joogiekspedilt nõu ja
      leia enda jaoks see õige.
    </p>
    <x-controls.button variant="gray-1">Vaata soovitusi</x-controls.button>
  </div>
  <div class="tags mb-4 max-w-full flex flex-wrap justify-center lg:col-start-2 lg:col-end-5 lg:row-start-3 lg:row-end-4">
    <x-elements.tag class="mx-2 mb-4">Vein</x-elements.tag>
    <x-elements.tag class="mx-2 mb-4">Kokteilid</x-elements.tag>
    <x-elements.tag class="mx-2 mb-4">Gin</x-elements.tag>
    <x-elements.tag class="mx-2 mb-4">Üllatus</x-elements.tag>
  </div>
  <div class="expert text-center lg:col-start-10 lg:col-end-13 lg:row-start-1 lg:row-end-2">
    <div class="name">
      <h4 class="text-title">Kristjan Peäske</h4>
    </div>
    <div class="position mb-4">
      <p class="body-2">
        Lore Bistroo ja Leib Restorani peremees, Kolmekordne Eesti ja kahekordne
        Baltikumi meistersommeljee
      </p>
    </div>
    <div class="awards flex justify-center space-x-3 mb-9">
      <img src="@asset('/images/mock-data/expert-badge-1.svg')" />
      <img src="@asset('/images/mock-data/expert-badge-2.svg')" />
      <img src="@asset('/images/mock-data/expert-badge-3.svg')" />
      <img src="@asset('/images/mock-data/expert-badge-4.svg')" />
    </div>
  </div>
  <div class="expert-image self-end flex justify-center lg:absolute lg:h-full lg:mx-auto lg:bottom-0 lg:left-1/2 lg:transform lg:-translate-x-1/2">
    <img src="@asset('/images/mock-data/expert-person.png')" class="w-full" />
  </div>
</div>
