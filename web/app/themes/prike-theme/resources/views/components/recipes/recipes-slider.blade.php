@props(['title' => __('Your evening will be superb')])
{{-- REFACTOR NOTE: Something must be done with one side overflow for recipes slider since overflow-x-visible for slick-list and track would make this visible both sides --}}
<div
    class="recipes-slider-wrapper py-20 xl:py-28 flex flex-col xl:flex-row justify-between xl:justify-start xl:w-auto xl:overflow-x-visible xl:pl-20 xl:pr-0">
    <div class="flex flex-col justify-between relative xl:w-1/4">
        <div class="recipes-slider-header flex justify-between mb-6 xl:flex-col xl:justify-start">
            <div class="recipes-subtitle hidden xl:block xl:mb-4">
                <h5 class="text-regular font-medium">Featured recipes</h5>
            </div>
            <div class="recipes-title xl:w-4/6 xl:mb-6">
                <h2 class="text-2.5xl xl:text-3xl xl:-ml-0.5 font-normal font-playfair">{{ $title }}</h2>
            </div>
            <div class="recipes-read-more hidden xl:block">
                <x-controls.button narrow variant="gray-1">View more recipes</x-controls.button>
            </div>

            <div class="recipes-archive-link xl:hidden pt-1.5">
                <a href="#" class="link-2 font-medium font-poppins">Vaata</a>
            </div>
        </div>
        <div class="absolute bottom-7 left-0">

            <div class="recipes-slider-arrows place-self-start relative hidden xl:flex flex-wrap space-x-4 text-gray-1">
            </div>
        </div>
    </div>
    <div class="xl:w-3/4 flex-grow">
        <div class="recipes-slider">
            <x-recipes.recipes-slide imgSrc="{{ @asset('/images/mock-data/recipe-thumbnail1.png') }}" />
            <x-recipes.recipes-slide imgSrc="{{ @asset('/images/mock-data/recipe-thumbnail2.png') }}" />
            <x-recipes.recipes-slide />
            <x-recipes.recipes-slide />
        </div>
    </div>
</div>
