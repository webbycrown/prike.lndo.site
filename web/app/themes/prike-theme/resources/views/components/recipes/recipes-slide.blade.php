@props(['imgSrc' => @asset('/images/mock-data/recipe-thumbnail.png')])
<div class="recipe-slide-wrapper group">
    <div class="recipe-thumbnail relative mb-4 overflow-hidden">
        <img src="{{ $imgSrc }}" alt="Recipe name"
            class="
        group-hover:transform group-hover:scale-120
        transition
        duration-500
        rounded
        h-80
        object-cover
        w-full
      " />
        <div class="ingredient-tags flex absolute inset-x-4 bottom-3">
            <div
                class="ingredient-tag lg:rounded-100 rounded bg-gray-1 text-white px-2.5 h-8 flex items-center mx-2 leading-1">
                <span>Martini</span>
            </div>
            <div
                class="ingredient-tag lg:rounded-100 rounded bg-gray-1 text-white px-2.5 h-8 flex items-center mx-2 leading-1">
                <span>Gin</span>
            </div>
        </div>
    </div>
    <div class="recipe-data-wrapper">
        <div class="recipe-data mb-4 flex flex-col-reverse justify-between lg:flex-row">
            <div class="recipe-name">
                <h3 class="group-hover:text-gold-2 duration-500 transition font-semibold font-poppins">
                    Gin tonic
                </h3>
            </div>
            <div class="recipe-time">
                <p class="body-2 leading-none">2 min / Easy</p>
            </div>
        </div>
        <div class="recipe-tags flex flex-wrap space-x-2">
            <x-elements.tag class="">Trending</x-elements.tag>
            <x-elements.tag class="">Summer</x-elements.tag>
        </div>
    </div>
</div>
