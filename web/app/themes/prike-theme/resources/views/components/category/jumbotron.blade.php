@props([
      'imgAlt' => 'wine',
      'imgSrc' => @asset('/images/mock-data/category-jumbotron-img.jpg'),
      'title' => 'Red Wine',
      'description' => 'Wine is good for a person\'s life if you drink it in moderation. What is life without wine? It was created for the joy of men. Joy to the heart and consolation to the soul is wine drunk in moderation at the proper time; sorrow to the soul is wine when drunk much, in irritation and quarrels.',
  ])
{{-- NOTE: image as backround --}}
<div class="section jumbotron relative">
  <img src="{{ $imgSrc }}" alt="{{ $imgAlt }}" class="max-w-full h-112.5 object-cover object-bottom w-full">
  <div class="cat-gradient absolute h-full inset-0"></div>
  <div class="absolute bottom-0 left-0 w-full h-full">
    <div class="flex flex-col lg:justify-center justify-end items-center h-full pb-8 lg:pb-0">
      <div class="container">
        <div class="lg:w-6/12 ">
          <h1 class="text-white font-playfair font-normal text-3xl lg:text-5xl leading-x1.2 lg:mb-5 mb-4">{{ $title}}</h1>
          <p class="text-white font-poppins font-normal text-base leading-x1.6 opacity-70">{{ $description }}</p>
        </div>
      </div>
    </div>
  </div>
</div>