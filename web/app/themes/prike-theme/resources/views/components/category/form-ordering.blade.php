<form class="woocommerce-ordering" method="get">
	<select name="orderby" class="orderby form-select border-0 text-title font-medium" aria-label="Shop order">
    <option value="menu_order" selected="selected">Sort by Relevance</option>
    <option value="popularity">Sort by popularity</option>
    <option value="rating">Sort by average rating</option>
    <option value="date">Sort by latest</option>
    <option value="price">Sort by price: low to high</option>
    <option value="price-desc">Sort by price: high to low</option>
  </select>
	<input type="hidden" name="paged" value="1">
</form>
<span class="clear-both"></span>