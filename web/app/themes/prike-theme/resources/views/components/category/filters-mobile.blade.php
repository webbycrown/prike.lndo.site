@php
  
  $filters = [
    [
      'title' => 'Sort By',
      'search' => ['placeholder' => 'Search ...', 'id' => 12],
      'hidden' => 'desktop',
      'type' => 'sortby',
      'fields' => [
        [ 'label' => 'Price from High to Low'],
        [ 'label' => 'Price from High to Low'], 
        ]
    ],
    [
      'title' => 'Categories',
      'hidden' => 'desktop',
      'type' => 'pill',
      'fields' => [
        [ 'label' => 'Red Wine', 'term_id' => 234],
        [ 'label' => 'Chamopagne', 'term_id' => 235],
      ]
    ],
    [
      'title' => 'Country',
      'selected' => true,
      'hidden' => '',
      'search' => ['placeholder' => 'Search ...'],
      'type' => 'taxonomy',
      'fields' => [
          [
            [ 'label' => 'Germany', 'qty' => 33, 'term_id' => 12],
            [ 'label' => 'Poland', 'qty' => 33, 'term_id' => 10], 
            [ 'label' => 'Poland', 'qty' => 33, 'term_id' => 10], 
            [ 'label' => 'Poland', 'qty' => 33, 'term_id' => 10], 
            [ 'label' => 'Poland', 'qty' => 33, 'term_id' => 10], 
          ],
          [
            [ 'label' => 'Germany', 'qty' => 33, 'id' => 12],
            [ 'label' => 'Poland', 'qty' => 33, 'id' => 10], 
            [ 'label' => 'Poland', 'qty' => 33, 'id' => 10], 
            [ 'label' => 'Poland', 'qty' => 33, 'id' => 10], 
            [ 'label' => 'Poland', 'qty' => 33, 'id' => 10], 
          ],
          [
            [ 'label' => 'Germany', 'qty' => 33, 'id' => 12],
            [ 'label' => 'Poland', 'qty' => 33, 'id' => 10], 
            [ 'label' => 'Poland', 'qty' => 33, 'id' => 10], 
            [ 'label' => 'Poland', 'qty' => 33, 'id' => 10], 
            [ 'label' => 'Poland', 'qty' => 33, 'id' => 10], 
          ],
          [
            [ 'label' => 'Germany', 'qty' => 33, 'id' => 12],
            [ 'label' => 'Poland', 'qty' => 33, 'id' => 10], 
            [ 'label' => 'Poland', 'qty' => 33, 'id' => 10], 
            [ 'label' => 'Poland', 'qty' => 33, 'id' => 10], 
            [ 'label' => 'Poland', 'qty' => 33, 'id' => 10], 
          ],
          [
            [ 'label' => 'Germany', 'qty' => 33, 'id' => 12],
            [ 'label' => 'Poland', 'qty' => 33, 'id' => 10], 
            [ 'label' => 'Poland', 'qty' => 33, 'id' => 10], 
            [ 'label' => 'Poland', 'qty' => 33, 'id' => 10], 
            [ 'label' => 'Poland', 'qty' => 33, 'id' => 10], 
          ],
        ]
    ],
    [
      'title' => 'Sweet-Dry Scale',
      'hidden' => '',
      'selected' => false,
      'type' => 'pill',
      'fields' => [
        ['label' => 'Dry', 'term_id' => 12],
        ['label' => 'Sweet', 'term_id' => 12],
        ['label' => 'Semi-Sweet', 'term_id' => 10], 
        ]
    ],
    [
      'title' => 'Features',
      'hidden' => '',
      'selected' => false,
      'type' => 'pill',
      'fields' => [
        ['label' => 'Vegan', 'term_id' => '33'],
        ['label' => 'Natural', 'term_id' => '33'],
      ]
    ],
    [
      'title' => 'Food Pairing',
      'hidden' => '',
      'selected' => false,
      'type' => 'pill',
      'fields' => [
        ['label' => 'Beef', 'id' => 12],
        ['label' => 'Chicken', 'id' => 12],
      ]
    ],
    [
      'title' => 'Volume',
      'hidden' => '',
      'selected' => false,
      'type' => 'pill',
      'fields' => [
        ['label' => '10%', 'id' => 322],
        ['label' => '20%', 'id' => 3222],
      ],
    ],
    
    [
      'title' => 'Price',
      'hidden' => '',
      'type' => 'checkbox',
      'selected' => false,
      'fields' => [
        ['label' => '0 - 10$', 'qty' => 22, 'from' => 0, 'to' => 10],
        ['label' => '10 -30 $', 'qty' => 22, 'from' => 10, 'to' => 30],
        ]
    ],
    [
      'title' => 'Brands',
      'hidden' => '',
      'selected' => false,
      'search' => ['placeholder' => 'Enter a brand'],
      'type' => 'taxonomy',
      'fields' => [
        [
          [ 'label' => 'Allram', 'qty' => 33, 'term_id' => 12],
          [ 'label' => 'Anselman', 'qty' => 33, 'term_id' => 10], 
          [ 'label' => 'Au Yun', 'qty' => 33, 'term_id' => 10], 
          [ 'label' => 'Appassinero', 'qty' => 33, 'term_id' => 10], 
          [ 'label' => 'Beringer', 'qty' => 33, 'term_id' => 10], 
        ],
      ]
    ],
  ];

@endphp
<div class="cat-filters-mobile lg:hidden">
  <div class="hystmodal" id="filters-modal" aria-hidden="true">
    <div class="hystmodal__wrap">  
      <div class="bg-secondary-2 hystmodal__window rounded-t-xl w-full" role="dialog" aria-modal="true">
    
        <h4 class="text-center pt-2 mb-3">
          <span class="text-title pt-6 inline-block font-semibold
                font-poppins relative text-base">Filter
                  <span class="absolute inline-block bg-gray-1 rounded-sm inset-x-0 top-0 h-3px"></span>
          </span>
        </h4>
        <div class="filters-selected">
          <div class="container">
            <div class="flex flex-wrap items-center justify-start -mx-2.5 pt-4">
              <x-filters.pill close>Red wine</x-filters.pill>
              <x-filters.pill close >Dry</x-filters.pill>
              <x-filters.pill close >0.75l</x-filters.pill>
              <x-filters.pill close>Red wine</x-filters.pill>
              <x-filters.pill close >Dry</x-filters.pill>
              <x-filters.pill close >0.75l</x-filters.pill>
              <x-filters.pill marked>Clear all</x-filters.pill>
              
            </div>
          </div>
        </div>
          <div class="relative">
            <div class="max-w-5xl mx-auto">
              @foreach ($filters as $filter)
              
                @switch($filter['type'])
                  @case('taxonomy')
                  <x-filters.filter-taxonomy title="{{ $filter['title'] }}" :selected="$filter['selected']" :fields="$filter['fields']" />
                  @break
                  @case('pill')
                  <x-filters.filter-pill title="{{ $filter['title'] }}" :fields="$filter['fields']" />
                  @break
                  @default
                  
                @endswitch
              
              @endforeach
            </div>
          </div>
      </div>
    </div>
  </div>
  <div class="filter-toggle lg:hidden fixed bottom-0 left-0 w-full pb-5 z-30">
    <div class="flex justify-center">
      <x-filters.filter-button count="8"/>
    </div>
  </div>
</div>