@props(['title' => 'Red wine',
'total' => 234,
'count' => 268,
'tags' => [
  [ 'id' => 1,'name' => 'Chardonnay'],['id' => 2,'name' => 'Dry'],['id' => 3,'name' => 'Sweet'],
 ]
])

<div class="flex items-start mb-8">
      
  <h2 class="font-playfair font-normal text-title text-3xl mr-3 leading-x1.1">{{ $title }}</h2>
  <small class=" inline-block py-2 px-3 bg-bg3 rounded-100 ">{{ $count }}</small>
</div>
<div class="space-x-3 mb-8">
  @foreach ($tags as $tag)
  
  <a href="#" class="py-2 px-3 rounded-100 bg-bg3 text-title">{{ $tag['name']}}</a>
  @endforeach
</div>