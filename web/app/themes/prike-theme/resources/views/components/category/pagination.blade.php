@props(['btnTitle' => 'Load 32 more', 'total' => 1345, 'viewed' => 32])
<div class="text-center pb-8 mx-auto w-80">
  <span class="">You have viewed {{ $viewed }} of {{ $total }} products</span>
  <div class="mb-7">
    <span class="progrees relative bg-secondary-2 w-52 rounded-lg h-2 inline-block">
      <span class="absolute left-0 w-9 top-0 h-2 rounded-lg bg-secondary-1 inline-block"></span>
    </span>
  </div>
  <x-controls.button>{{ $btnTitle }}</x-controls.button>
</div>