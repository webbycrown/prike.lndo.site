@props(['title', 'subtitle' => null, 'thumbnail', 'variant' => 'gold', 'narrow' => false, 'type' => 'whiskey'])

@php
$subtitleColorClass = 'text-regular-3';
$headerClasses = 'text-white mb-1 xl:mb-3';
// REFACTOR NOTE: This causes issues with purifying
$tabBackgroundClass = 'gradient-radial-' . $variant;
$tabColSpan = $narrow ? 'xl:col-span-4' : 'xl:col-span-6';
$bottleImgNarrowClass = $narrow ? 'bottom-0 right-0 xl:absolute' : '';
$img = '/images/mock-data/' . $type . '.png';
switch ($variant) {
    case 'menu':
        $subtitleColorClass = 'text-regular-2';
        $headerClasses = 'text-gray-1 -mt-1';
        $tabBackgroundClass = 'bg-secondary-1';
        break;
    case 'champagne':
        $subtitleColorClass = 'text-regular-2';
        $headerClasses = 'text-gray-1';
        break;
    case 'vip':
        $headerClasses = 'text-gold';
        $tabBackgroundClass = 'bg-gray-1';
}
@endphp

<div
    class="category-tab rounded flex justify-between pl-4 xl:pl-8 pr-6 h-34 xl:h-75 relative {{ $tabColSpan }} {{ $tabBackgroundClass }}">
    <div class="category-title flex flex-col justify-center">
        {{-- REFACTOR NOTE: Add stretched link to element afterwards --}}
        <a href="{{ get_home_url() . '/product-category/red-wine' }}">
            <h2 class="{{ $headerClasses }} {{ $subtitle ? '' : 'mb-3' }} font-normal text-2.5xl xl:text-3xl w-60">
                {{ $title }}
            </h2>
        </a>

        <p class="font-size-13px xl:text-xs font-medium {{ $subtitleColorClass }}">
            {{ $subtitle }}
        </p>
    </div>
    <div class="category-thumbnail self-end w-full absolute flex items-end flex-col">
        <img src="@asset($img)" class="h-28 xl:h-auto xl:w-auto {{ $bottleImgNarrowClass }}" />
    </div>
</div>
