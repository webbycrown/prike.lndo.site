  @props(['id' => 1])
  <div class="cat-carousel-wrapper relative -mx-2.5 pb-2">
    <div class="w-full">
      <div class="cat-carousel relative">
        @for ($i = 0; $i < 8; $i++)
        <div class="py-4 px-2.5">
          <x-product.cat-product />
        </div>    
        @endfor
      </div>

    </div>
    <div class="absolute w-full top-52">
      <div class="hidden md:flex cat-carousel-arrows-{{ $id }}
                  flex justify-between sm:-mx-4"></div>
    </div>
  </div>
        