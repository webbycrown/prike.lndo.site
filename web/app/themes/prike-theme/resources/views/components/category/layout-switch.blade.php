<div class="layout-switch flex justify-between w-10 mr-2.5">
  <label class="cursor-pointer">
    <input type="radio" class="w-0 h-0 opacity-0" name="layout" value="double"/>
    <span class="border-solid p-1 border-secondary-2 border-2 flex items-center justify-between">
      
      <span class="inline-block w-1 h-2 bg-regular-2"></span>
      <span class="ml-0.5 inline-block w-1 h-2 bg-regular-2"></span>
    </span>
  </label>
  <label class="ml-2 cursor-pointer ">
    <input type="radio" name="layout" class="w-0 h-0 opacity-0" value="single" />
    <span class="border-solid p-1 border-title border-2 flex items-center justify-between">
    
      <span class="inline-block w-2 h-2 bg-title"></span>
    </span>
  </label>
</div>