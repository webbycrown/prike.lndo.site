@php
  
  $filters = [
    [
      'title' => 'Country',
      'selected' => true,
      'hidden' => '',
      'search' => ['placeholder' => 'Search ...'],
      'type' => 'taxonomy',
      'fields' => [
          [
            [ 'label' => 'Germany', 'qty' => 33, 'term_id' => 12],
            [ 'label' => 'Poland', 'qty' => 33, 'term_id' => 10], 
            [ 'label' => 'Poland', 'qty' => 33, 'term_id' => 10], 
            [ 'label' => 'Poland', 'qty' => 33, 'term_id' => 10], 
            [ 'label' => 'Poland', 'qty' => 33, 'term_id' => 10], 
          ],
          [
            [ 'label' => 'Germany', 'qty' => 33, 'id' => 12],
            [ 'label' => 'Poland', 'qty' => 33, 'id' => 10], 
            [ 'label' => 'Poland', 'qty' => 33, 'id' => 10], 
            [ 'label' => 'Poland', 'qty' => 33, 'id' => 10], 
            [ 'label' => 'Poland', 'qty' => 33, 'id' => 10], 
          ],
          [
            [ 'label' => 'Germany', 'qty' => 33, 'id' => 12],
            [ 'label' => 'Poland', 'qty' => 33, 'id' => 10], 
            [ 'label' => 'Poland', 'qty' => 33, 'id' => 10], 
            [ 'label' => 'Poland', 'qty' => 33, 'id' => 10], 
            [ 'label' => 'Poland', 'qty' => 33, 'id' => 10], 
          ],
          [
            [ 'label' => 'Germany', 'qty' => 33, 'id' => 12],
            [ 'label' => 'Poland', 'qty' => 33, 'id' => 10], 
            [ 'label' => 'Poland', 'qty' => 33, 'id' => 10], 
            [ 'label' => 'Poland', 'qty' => 33, 'id' => 10], 
            [ 'label' => 'Poland', 'qty' => 33, 'id' => 10], 
          ],
          [
            [ 'label' => 'Germany', 'qty' => 33, 'id' => 12],
            [ 'label' => 'Poland', 'qty' => 33, 'id' => 10], 
            [ 'label' => 'Poland', 'qty' => 33, 'id' => 10], 
            [ 'label' => 'Poland', 'qty' => 33, 'id' => 10], 
            [ 'label' => 'Poland', 'qty' => 33, 'id' => 10], 
          ],
        ]
    ],
    [
      'title' => 'Sweet-Dry Scale',
      'hidden' => '',
      'selected' => false,
      'type' => 'pill',
      'fields' => [
        ['label' => 'Dry', 'term_id' => 12],
        ['label' => 'Sweet', 'term_id' => 12],
        ['label' => 'Semi-Sweet', 'term_id' => 10], 
        ]
    ],
    [
      'title' => 'Features',
      'hidden' => '',
      'selected' => false,
      'type' => 'pill',
      'fields' => [
        ['label' => 'Vegan', 'term_id' => '33'],
        ['label' => 'Natural', 'term_id' => '33'],
      ]
    ],
    [
      'title' => 'Food Pairing',
      'hidden' => '',
      'selected' => false,
      'type' => 'pill',
      'fields' => [
        ['label' => 'Beef', 'id' => 12],
        ['label' => 'Chicken', 'id' => 12],
      ]
    ],
    [
      'title' => 'Body',
      'hidden' => '',
      'selected' => false,
      'type' => 'pill',
      'fields' => [
        ['label' => '10%', 'id' => 322],
        ['label' => '20%', 'id' => 3222],
      ],
    ],
    [
      'title' => 'Volume',
      'hidden' => '',
      'selected' => false,
      'type' => 'pill',
      'fields' => [
        ['label' => '10%', 'id' => 322],
        ['label' => '20%', 'id' => 3222],
      ],
    ],
    
    [
      'title' => 'Price',
      'hidden' => '',
      'type' => 'checkbox',
      'selected' => false,
      'fields' => [
        ['label' => '0 - 10$', 'qty' => 22, 'from' => 0, 'to' => 10],
        ['label' => '10 -30 $', 'qty' => 22, 'from' => 10, 'to' => 30],
        ]
    ],
    [
      'title' => 'Brands',
      'hidden' => '',
      'selected' => false,
      'search' => ['placeholder' => 'Enter a brand'],
      'type' => 'taxonomy',
      'fields' => [
        [
          [ 'label' => 'Allram', 'qty' => 33, 'term_id' => 12],
          [ 'label' => 'Anselman', 'qty' => 33, 'term_id' => 10], 
          [ 'label' => 'Au Yun', 'qty' => 33, 'term_id' => 10], 
          [ 'label' => 'Appassinero', 'qty' => 33, 'term_id' => 10], 
          [ 'label' => 'Beringer', 'qty' => 33, 'term_id' => 10], 
        ],
      ]
    ],
  ];

@endphp
<div class="cat-filters bg-secondary-2 hidden lg:block border-solid border-b border-line">
  <div class="relative">
    <div class="flex items-center justify-center">
      @foreach ($filters as $filter)
      
        @switch($filter['type'])
          @case('taxonomy')
          <x-filters.filter-taxonomy title="{{ $filter['title'] }}" :selected="$filter['selected']" :fields="$filter['fields']" />
          @break
          @case('pill')
          <x-filters.filter-pill title="{{ $filter['title'] }}" :fields="$filter['fields']" />
          @break
          @default
          
        @endswitch
      
      @endforeach
    </div>
  </div>
</div>