@props([
  'name' => '',
  'variant' => 'default',
  'variants' => [
    'default' => 'w-4 h-4 ring-2 ring-primary ring-offset-4.8 ring-offset-white focus:ring-2 focus:ring-primary focus:ring-offset-4.8 focus:text-primary checked:bg-primary checked:text-primary',
    'checkout' => 'align-baseline h-3 w-3 ring-2 ring-line-2 ring-offset-4 ring-offset-white focus:ring-2 focus:ring-gray-1 focus:ring-offset-4 focus:text-gray-1 checked:bg-gray-1 checked:text-gray-1'
  ]
  ])
<input 
  type="radio" name="{{ $name }}"
  class="form-radio border-0 hover:bg-regular-3
  {{ $variant ? $variants[$variant] : $variants['default'] }}"  />