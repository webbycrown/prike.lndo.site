@props([
    'narrow' => false,
    'variant' => 'default',
    'variants' => [
        'default' => 'bg-white border-black hover:bg-black hover:text-gold active:text-white disabled:border-gray-1 disabled:text-gray-1 disabled:bg-white active:translate-y-0.5 active:translate-x-0.5',
        'black' => 'bg-black text-white border-black hover:text-gold active:text-white disabled:bg-gray-2 disabled:text-white disabled:border-gray-2',
        'gold' => 'gradient-gold border-transparent',
        'gray-1' => 'bg-gray-1 border-gray-1 text-white',
        'gray-2' => 'bg-gray-1 border-gray-1 text-white lg:w-auto',
        'gold' => 'gradient-gold border-transparent',
        'gold-2' => 'gradient-gold border-transparent w-full lg:w-50',
    ],
])

@php
$classes = join(' ', [$narrow ? 'px-6 py-2 xl:py-3 w-auto' : 'px-9 h-16 w-full', 'border-2 active:border-0 ', $variants[$variant], 'rounded transform transition-all duration-300 text-base font-poppins font-semibold font-title leading-none']);

@endphp

<button {{ $attributes->merge(['class' => $classes]) }}>
    {{ $slot }}
</button>
