<label class="relative inline-block w-9.5 h-5">
  <input type="checkbox" class="opacity-0 w-0 h-0">
  <span class="toggler round absolute cursor-pointer inset-0 bg-secondary-3 duration-300"></span>
</label>
