<select>
  <option value="address-1" selected>Viimsi Rimi pakiautomaat</option>
  <option value="address-2">Viru keskuse pakiautomaat</option>
  <option value="address-3">Another machine</option>
</select>

<div class="custom-select-wrapper">
  <div class="custom-select">
    <div class="custom-select__trigger">
      <span>Viimsi Rimi pakiautomaat</span>
      <span class="arrow">
        <img class="m-0" src="@asset('images/icons/dropdown-arrow-up.svg')" />
      </span>
    </div>
    <div class="custom-options">
      <div class="custom-option selected" data-value="address-1">
        <p class="parcel-location-name">Viimsi Rimi pakiautomaat</p>
        <p class="body-2">
          Harju maakond, Viimsi vald, Haabneeme alevik, Randvere tee, 9
        </p>
      </div>
      <div class="custom-option" data-value="address-2">
        <p class="parcel-location-name">Viru Keskuse pakiautomaat</p>
        <p class="parcel-location-address body-2 text-regular-2">
          Harju maakond, Viimsi vald, Haabneeme alevik, Randvere tee, 9
        </p>
      </div>
      <div class="custom-option" data-value="address-3">
        <p class="parcel-location-name">Another machine</p>
        <p class="parcel-location-address body-2 text-regular-2">
          Tartu maakond, Viimsi vald, Haabneeme alevik, Randvere tee, 9
        </p>
      </div>
    </div>
  </div>
</div>
