<button {{ $attributes }} class="rounded-full h-50px w-50px flex items-center justify-center gradient-prike-new active:bg-gold-3">
  {{ $slot }}
</button>