@props([
  'variant' => 'default',
  'variants' => [
    'default' => 'rounded',
    'filter' => 'rounded-sm bg-transparent'
  ]
  ])
<input 
  type="checkbox" 
  class="appearance-none border-regular-2 border-2 
  {{ $variant ? $variants[$variant] : $variants['default'] }}
   w-5 h-5 ring-0 outline-0
  hover:border-regular-2 hover:bg-regular-2
  checked:bg-black focus:ring-black focus-visible:bg-black focus:ring-0 checked:hover:bg-black focus:ring-offset-0
  " />