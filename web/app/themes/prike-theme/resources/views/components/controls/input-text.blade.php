@props([
  'placeholder',
  'description',
  'type',
  'label',
  'variant' => 'default',
  'variants' => [
    'default' => 'border-line text-regular-2 focus:border-line-2 focus:text-line-2 focus:ring-0',
    'alert' => 'border-red text-red placeholder-red focus:ring-0'
  ]
])

<div class="input input-text relative">
  <input 
    type="{{ isset($type) ? $type : 'text' }}"
    class="form-input p-5 border-2 rounded
    disabled:border-line disabled:text-line
    {{ $variant ? $variants[$variant] : $variants['default'] }}
    "
    {{ isset($placeholder) ? $attributes->merge(['placeholder' => $placeholder]) : null }}
    {{ $attributes }}
  />
  <label class="form-label hidden absolute px-2 -top-3 left-4 bg-white">{{ $label ?? null }}</label>
  
  @if(isset($description))
    <div class="{{ $variant == 'alert' ? 'text-red' : 'text-regular-2' }} text-xxs">
      {{ $description }}
    </div>
  @endif
</div>