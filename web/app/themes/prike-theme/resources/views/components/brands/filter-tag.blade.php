@props([ 'active' => false, ])

<button class="px-3 py-2.5 {{ $active ? 'bg-bg1 border-2 border-gold' : 'bg-bg3' }} text-title items-center justify-center flex rounded-full">
  {{ $slot }}
</button>
