<div class="container pt-1 pb-9">
  <div class="text-center">
    <h2 class="letter-line gray-1 font-playfair text-2.5xl font-normal">A</h2>
    <div class="brands grid grid-cols-2 xl:grid-cols-7 gap-x-4 gap-y-9 pt-6 pb-16">
      @for($i = 0; $i < 5; $i++)
      <div class="h-30 flex items-center justify-center">
        <a href="{{ get_home_url() . '/brands/single-brand-page' }}"><img src="@asset('/images/mock-data/brand-becherovka.png')" class="h-auto" /></a>
      </div>

      <div class="h-30 flex items-center justify-center">
        <a href="{{ get_home_url() . '/brands/single-brand-page' }}"><img src="@asset('/images/mock-data/brand-aperol.png')" class="h-auto" /></a>
      </div>
      <div class="h-30 flex items-center justify-center">
        <a href="{{ get_home_url() . '/brands/single-brand-page' }}"
          ><img src="@asset('/images/mock-data/brand-ballantines.png')" class="h-auto"
        /></a>
      </div>
      @endfor
    </div>
  </div>
</div>
