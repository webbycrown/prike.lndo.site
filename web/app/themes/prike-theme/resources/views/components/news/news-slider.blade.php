@props(['title' => __('Uudised')])

<div class="news-slider-wrapper py-20">
    <div class="news-slider-header flex justify-between mb-6 container">
        <div class="news-title">
            <h2 class="text-2.5xl xl:text-3xl font-normal">{{ $title }}</h2>
        </div>
        <div class="news-archive-link">
            <a href="#" class="link-2 font-semibold font-poppins">Vaata {{-- lähemalt --}}</a>
        </div>
    </div>

    <div class="news-slider container pr-0">
        <x-news.news-slide />
        <x-news.news-slide />
        <x-news.news-slide />
    </div>
    <div class="news-load-more flex justify-center pt-10">
        <x-controls.button narrow>View more articles</x-controls.button>
    </div>
</div>
