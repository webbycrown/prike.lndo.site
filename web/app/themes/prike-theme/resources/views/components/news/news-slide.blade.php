<div class="news-slide group lg:px-2.5 relative">
    <div class="max-w-80 mb-4 flex flex-col">
        <img src="@asset('/images/mock-data/news-thumbnail.png')" alt="News article title"
            class="mb-5 rounded lg:w-full" />
        {{-- Refactor note: Add stretched link again --}}
        <a href="#" class="mt-4">
            <h3 class="text-gray-1 mb-2 group-hover:text-gold-2">
                New Portugal wine house De Melgaco
            </h3>
        </a>

        <p class="body-2">
            Quinta de Melgaco in Vinho Verde is one of the fastest growing wine regions
            in northern ...
        </p>
    </div>
</div>
