<div class="discount-slide">
    <div
        class="
      discount-wrapper
      gradient-radial-discount
      px-4
      pt-6
      mb-4
      flex
      justify-between
      rounded
    ">
        <div class="discount-data">
            <h4 class="mb-4 leading-x1.2">Johnnie Walker Black Label -%</h4>
            <div class="discount-button mb-5">
                <x-controls.button narrow variant="gray-1">Vaata</x-controls.button>
            </div>

            <p class="text-red font-medium leading-none mb-5">Kuni 30.06</p>
        </div>
        <div class="discount-thumbnail self-end">
            <img src="@asset('/images/mock-data/discount-thumbnail.png')" class="h-40 w-auto max-w-none" />
        </div>
    </div>
</div>
