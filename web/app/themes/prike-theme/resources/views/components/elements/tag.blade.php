{{-- Refactor note: Add a class and remove cursor pointer --}}
@props([
    'fill' => 'transparent',
    'text' => 'regular-2',
    'border' => 'regular-2',
    'uppercase' => false,
    'borderBold' => false,
])

@php $default_classes = 'inline rounded-full py-1 px-2.5 w-max text-sm font-medium cursor-pointer';
$background_class = 'bg-' . $fill;
$border = 'border-' . $border;
$border_bold = $borderBold ? 'border-2' : 'border';
$text_class = 'text-' . $text;
$uppercase_class = $uppercase ? 'uppercase' : '';
$merged_tag_classes = collect([$default_classes, $background_class, $border, $border_bold, $text_class, $uppercase_class])->join(' '); @endphp

<div {{ $attributes->merge(['class' => $merged_tag_classes]) }}>

    {{ $slot }}
</div>
