<div class="search-input relative">
  <span class="absolute inset-y-0 left-0 flex items-center pl-2">
    <button type="submit" class="p-1 focus:outline-none focus:shadow-outline">
      <svg
      fill="none"
      stroke="#bdbdbd"
      stroke-linecap="round"
      stroke-linejoin="round"
      stroke-width="2"
      viewBox="0 0 24 24"
      class="w-6 h-6"
      >
      <path d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path>
    </svg>
  </button>
</span>
<input class="form-input w-full rounded bg-line-2 pl-12 border-0 py-2.5 text-line" type="text" placeholder="Search for tastes" />
<div class="lds-dual-ring hidden"></div>
<div class="wp_ajax_result form-result hidden">
  <div class="wp_ajax_result_wrapper bg-secondary-2 px-5 py-6 bg-opacity-95 absolute w-full overflow-y-scroll overflow-auto z-10" style="max-height: 500px;">
    <h1 class="query_title text-lg text-gray-1 mb-4 font-poppins font-bold"> Suggested Queries </h1>
    <ul class="result_taxonomy w-full whitespace-nowrap overflow-auto items-center pb-2 mb-6">
      <?php
      $args = array(
        'taxonomy'   => "product_cat",
      );
      $product_categories = get_terms($args);
      // print_r($product_categories);
      foreach ($product_categories as $product_cat) {
        $category_link = get_category_link($product_cat->term_id);
        ?>
        <li class="result_tax_item mr-3 inline-block"><a class="border-2 whitespace-nowrap border-gray-1 rounded-full text-gray-1 text-sm font-medium py-1.5 px-4 block hover:bg-gray-1 hover:text-white" href="<?php echo $category_link; ?>"><?php echo $product_cat->name; ?></a></li>  
      <?php } ?>
    </ul>
    <div class="wc_error"></div>
    <div class="product_result">
      <div class="_header flex justify-between pb-3">
        <div class="font-poppins">
          <span class="text-lg text-gray-1 mb-4 font-bold">Products</span><span class="result_count bg-secondary-2 ml-2 text-regular-2 rounded-full py-1 px-3">5</span></div>
          <a href="<?php echo site_url('/shop'); ?>" target="_blank" class="result_view_all font-medium text-gold-2 text-sm">View All</a>
        </div>
        <div class="_body">
          <ul class="wc_product_result">
            <?php
            global $woocommerce;
            $args = array(
              'post_type' => 'product',
              'posts_per_page' => 5,
              'post_status' => 'publish',
            );
            $the_query = new WP_Query( $args );
            if ( $the_query->have_posts() ) {
              while ( $the_query->have_posts() ) { $the_query->the_post();
                //$cart_page_url = function_exists( 'wc_get_cart_url' ) ? wc_get_cart_url() : $woocommerce->cart->get_cart_url();
                $product = wc_get_product(get_the_ID());
                $categories = get_the_terms( get_the_ID(), 'product_cat' );
                $cat_arr = array();
                foreach ($categories as $category) {
                  $cat_arr[] = $category->name;
                }
                $cat_arr = implode('/',$cat_arr);
                ?>
                <li class="product_result_item mb-4 flex items-center justify-between">
                  <div class="left flex justify-between items-center">
                    <div class="product_thumb relative w-16"><?php the_post_thumbnail(); ?><a href="<?php the_permalink(); ?>" class="absolute w-full h-full top-0 left-0"></a></div>
                    <div class="product_data pl-4">
                      <h3 class="text-sm font-semibold"><a href="<?php the_permalink(); ?>" class="text-gray-1"><?php the_title(); ?></a></h3>
                      <h3 class="text-sm mt-1 text-regular leading-3 font-normal text-gray-1"><?php echo $cat_arr; ?></h3>
                    </div>
                  </div>
                  <div class="right flex font-poppins items-center justify-between">
                    <div class="product_price font-semibold mr-12">
                      <?php echo number_format($product->get_price(),2).get_woocommerce_currency_symbol(); ?>
                    </div>
                    <div class="product_url">
                      <?php if( $product->is_type( 'simple' ) ) {
                        $url = '?add-to-cart='.$product->get_id();
                        $target = '';
                      }else{
                        $url = get_permalink( $product->get_id() );
                        $target = 'target="blank"';
                      } ?>
                      <a href="<?php echo $url; ?>" <?php echo $target; ?> class="block py-3 rounded-full px-3" style="background: linear-gradient(268.47deg, #E4BD71 -0.76%, #F1CE72 51.16%, #A1792E 98.92%);"><img src="@asset('/images/icons/bag.svg')"></a>
                    </div>
                  </div>
                </li>
              <?php } wp_reset_postdata();
            } ?>
          </ul>
        </div>
      </div>
      <div class="blogs_result mt-10">
        <div class="blogs_result_wrapper"> 
          <?php 
          $p_categories = get_categories( array(
            'orderby' => 'name',
            'order'   => 'DESC',
          ) );
          foreach ($p_categories as $p_category) { ?>
            <div class="blogs_item mt-10">
              <div class="_header flex justify-between pb-3 font-poppins">
                <h3 class="text-lg text-gray-1 mb-4 font-bold"><?php echo $p_category->name; ?>  <span class="result_count result_count bg-secondary-2 ml-2 text-regular-2 rounded-full py-1 px-3"><?php echo $p_category->count; ?></span></h3>
                <a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>" target="_blank" class="result_view_all result_view_all font-medium text-gold-2 text-sm">View All</a>
              </div>
              <div class="_body grid gap-2 font-poppins grid-cols-2">
                <?php 
                $args = array(
                  'post_type' => 'post',
                  'post_status' => 'publish',
                  'posts_per_page' => 2,
                  'tax_query' => array(
                    array(
                      'taxonomy' => 'category',
                      'field' => 'term_id',
                      'terms' => $p_category->term_id,
                    )
                  )
                );
                $the_query = new WP_Query( $args );
                if ( $the_query->have_posts() ) {
                  while ( $the_query->have_posts() ) { $the_query->the_post();
                    $posttags = get_the_tags();
                    ?>
                    <div class="blogs relative">
                      <a href="<?php the_permalink(); ?>" class="absolute top-0 bottom-0 w-full h-full"></a>
                      <div class="post_thumb relative"><?php the_post_thumbnail(); ?>
                      <?php
                      if ($posttags) {
                        foreach($posttags as $tag) { ?>
                          <span class="post_badge bg-gray-1 absolute bottom-0 left-0 ml-5 mb-5 text-white rounded-full py-1 px-3"><?php echo $tag->name; ?></span>
                        <?php }
                      }
                      ?>
                      
                    </div>
                    <div class="post_title mt-4 font-poppins text-base font-semibold"><?php the_title(); ?></div>                    
                  </div>
                <?php } ?>
                <?php wp_reset_postdata(); }
                ?> 
              </div>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>