<div class="gradient-green px-2.5 py-3.5 min-h-75px rounded flex items-center">
  <span class="pr-2">X</span>
  <p class="body-2">{{ $slot }}</p>
</div>