@if (has_nav_menu('secondary_navigation'))
    {!! wp_nav_menu([
    'theme_location' => 'secondary_navigation',
    // 'depth'          => 1,
    'container' => false,
    'fallback_cb' => false,
    'echo' => true,
    'walker' => new \App\Secondary_Menu_Walker(),
    'anchor_classes' => 'text-regular-3 font-normal text-sm font-poppins',
    'items_wrap' => '<ul class="footer-nav-menu xl:flex">%3$s</ul>',
]) !!}
@else

    <ul class="footer-nav-menu xl:flex">
        @for ($i = 0; $i < 5; $i++)
            <li
                class="
      footer-nav-menu-item
      py-5
      first:border-t
      xl:first:border-0
      first:border-line
      first:border-opacity-50
      border-b
      border-line
      border-opacity-50
      xl:border-0
      flex flex-col
      cursor-pointer
      xl:py-0 xl:pr-12 xl:last:pr-0
    ">
                <div class="flex items-center justify-between w-full">
                    <h5 class="font-semibold">Nav category</h5>
                    <p class="text-regular-3 pl-4 xl:hidden" style="transform: rotate(90deg)">></p>
                </div>

                <ul class="hidden xl:block pt-5">
                    @for ($t = 0; $t < 5; $t++)
                        <li class="py-4 xl:py-1">
                            <a href="http://" target="_blank" rel="noopener noreferrer"
                                class="text-regular-3 text-sm font-poppins">Nav link</a>
                        </li>
                    @endfor
                </ul>
            </li>
        @endfor
    </ul>
@endif
