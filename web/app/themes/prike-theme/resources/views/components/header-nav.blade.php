{{-- <ul class="grid gap-x-8 grid-flow-col mt-4">
  @for ($i = 0; $i < 8; $i++)
  <li>
    <a href="" class="text-regular-3 text-sm font-medium leading-5 font-poppins"
      >Link test</a
    >
  </li>

  @endfor
</ul> --}}

@if (has_nav_menu('primary_navigation'))
  {!! wp_nav_menu(
    [
    'theme_location' => 'primary_navigation', 
    'container' => false,
    'anchor_class' => 'text-regular-3 text-sm font-medium leading-4 font-poppins',
    'menu_class' => 'grid xl:gap-x-8 justify-between w-full xl:w-auto grid-flow-col mt-4', 
    'walker' => new \App\Primary_Menu_Walker()
    ]
    ) !!}
@endif
