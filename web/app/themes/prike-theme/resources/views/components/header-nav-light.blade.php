
{{-- note:  check menu location and  --}}
@if (has_nav_menu('light_navigation'))
  {!! wp_nav_menu(
    [
    'theme_location' => 'light_navigation', 
    'container' => false,
    'anchor_class' => 'text-regular-3 text-sm font-medium leading-4 font-poppins opacity-70',
    'menu_class' => 'grid gap-x-8 grid-flow-col mt-4', 
    'walker' => new \App\Primary_Menu_Walker()
    ]
    ) !!}
@else

<ul class="grid gap-x-8 grid-flow-col mt-4">
  @for ($i = 0; $i < 4; $i++)
  <li>
    <a href="" class="text-regular-3 text-sm font-medium leading-4 font-poppins opacity-70"
      >Link</a
    >
  </li>
  @endfor
</ul>
@endif

