@props([
'slides'
])


<div class="hero-slider">
  @foreach ($slides as $image)
  <x-hero-slider.hero-slider-slide :image='$image' />
  @endforeach
</div>
<div class="slick-slider-dots-wrapper lg:block lg:absolute lg:-bottom-40 lg:w-full h-auto bg-gray-1 lg:bg-transparent"></div>
{{--<div class="slick-slider-dots-wrapper lg:block absolute bottom-0 mx-auto w-full"></div>--}}