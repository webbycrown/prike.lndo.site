<div class="slide relative md:h-180 flex flex-col">
  <!-- When BE dev, use this as image
      @image(id, 'size', 'alt tage') -->
  <div class="hero-image relative">
    <div class="h-157.5 lg:h-180 lg:w-full md:hidden"
         style="background-image: url(@asset('/images/mock-data/hero-slider-image.png')); background-size: 1000px; background-position: 66% 0;">
    </div>

    <div class="h-180 lg:h-180 lg:w-full hidden md:block"
         style="background-image: url(@asset('/images/mock-data/hero-slider-image.png')); background-size: cover; background-position: center;">
    </div>

    <div class="gradient-hero absolute h-full lg:h-8 inset-0 lg:inset-y-auto lg:bottom-0"></div>

    <div class="container p-0">
      <div class="hero-content absolute bottom-0 m-5 w-auto lg:w-30 lg:h-full lg:flex lg:justify-center lg:flex-col">
        <h1 class="text-white mb-6 font-thin font-size-2 lg:font-size-3 full-width">
          Suurepärased joogid suurepärase maitsega inimestele
        </h1>

        <button class="w-full gradient-prike-new px-10 py-5 mb-5 rounded lg:w-max">
          <h5 class="leading-x1.2">View expert suggestions</h5>
        </button>
      </div>
    </div>
  </div>
</div>
