$(function () {
  $('.recipes-slider').slick({
    mobileFirst: true,
    slidesToShow: 1,
    arrows: false,
    centerMode: true,
    centerPadding: '24px',
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          appendArrows: $('.recipes-slider-arrows'),
          arrows: true,
        },
      },
    ],
  });
});
