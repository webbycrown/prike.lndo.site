$(function () {
  $('.news-slider').slick({
    arrows: false,
    slidesToShow: 2,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          centerMode: true,
          centerPadding: '24px',
        },
      },
    ],
  });
});
