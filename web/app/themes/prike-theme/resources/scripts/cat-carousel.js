$(function () {
  let count = 1;
  $('.cat-carousel').each(function () {
    let selector = '.cat-carousel-arrows-' + count++;
    $(this).slick({
      dots: false,
      infinite: false,
      appendArrows: $(selector),
      arrows: true,
      speed: 300,
      slidesToShow: 4,
      responsive: [
        {
          breakpoint: 1280,
          settings: {
            slidesToShow: 3,
          },
        },
        // {
        //   breakpoint: 1024,
        //   settings: {
        //     slidesToShow: 2,
        //   },
        // },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2,
            // appendDots: '',
            arrows: false,
            dots: true,
          },
        },
      ],
    });
  });
});
