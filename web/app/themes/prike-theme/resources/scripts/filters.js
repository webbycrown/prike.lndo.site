import HystModal from './general/hystmodal';
import { isDeviceMobile } from './general/util';

$(function () {
  const Mymodal = new HystModal({
    linkAttributeName: 'data-hystmodal',
  });
  const $filter = $('.cat-filters-mobile');
  const $filterDesktop = $('.cat-filters');
  if ($filter.length && isDeviceMobile()) {
    $filter.on('click', '.caret', function () {
      $(this).parents('.cat-filter').find('.cat-selector').eq(0).slideToggle();
      $(this).parents('.cat-filter').toggleClass('is-active');
    });
  }
  if ($filterDesktop.length) {
    $filterDesktop.on('click', '.cat-title', function (e) {
      console.log(e.target);
      // if (e.target.classList.contains('cat-filter')) {
      //   return;
      // }
      const $el = $(this).parent('.cat-filter');
      if ($el.attr('data-is-active') && +$el.attr('data-is-active') === 1) {
        $el.children('.cat-selector').slideUp();
        $el.removeClass('is-active');
        $el.attr('data-is-active', '');
      } else {
        const $active = $filterDesktop.find(`[data-is-active='1']`).eq(0);
        if ($active.length) {
          $active.children('.cat-selector').slideUp(function () {
            $el.children('.cat-selector').slideDown();
            $el.addClass('is-active');
            $el.attr('data-is-active', 1);
          });
          $active.removeClass('is-active');
          $active.attr('data-is-active', '');
        } else {
          $el.children('.cat-selector').slideDown();
          $el.addClass('is-active');
          $el.attr('data-is-active', 1);
        }
      }
    });
  }
});
