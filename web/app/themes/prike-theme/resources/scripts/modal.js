import PModal from './general/pmodal';
import { isDeviceMobile } from './general/util';

$(function () {
  if (isDeviceMobile()) return;
  const modal = new PModal({
    linkAttributeName: 'data-pmodal',
  });
});
