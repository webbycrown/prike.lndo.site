$(() => {
  $('#single-product-slider').slick({
    mobileFirst: true,
    arrows: false,
    dots: true,
    appendDots: $('.single-product-slider-dots'),
  });
});
