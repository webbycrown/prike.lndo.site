$(() => {
  $('.sizes-wrapper').slick({
    variableWidth: true,
    draggable: true,
    mobileFirst: true,
    arrows: false,
    infinite: false,
  });
});
