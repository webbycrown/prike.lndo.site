$(() => {
  // Toggle classes for wine dryness bar
  $('.delivery-tallinn').on('click', () => {
    console.log('clicked');
    $('.delivery-tallinn-description').removeClass('hidden');
    $('.delivery-estonia-description').addClass('hidden');
    $('.delivery-tallinn').removeClass('border-gray-2 font-normal text-regular-2').addClass('border-gold-2');
    $('.delivery-estonia').removeClass('border-gold-2').addClass('border-gray-2 font-normal text-regular-2');
  });

  // Toggle classes for wine dryness bar
  $('.delivery-estonia').on('click', () => {
    console.log('clicked');
    $('.delivery-tallinn-description').addClass('hidden');
    $('.delivery-estonia-description').removeClass('hidden');
    $('.delivery-estonia').removeClass('border-gray-2 font-normal text-regular-2').addClass('border-gold-2');
    $('.delivery-tallinn').removeClass('border-gold-2').addClass('border-gray-2 font-normal text-regular-2');
  });

  // Remove product review truncation
  $('.review-read-more').on('click', function () {
    console.log($(this).prev('review-text'));
    $(this).prev('p').removeClass('truncate');
    $(this).addClass('hidden');
  });
});
