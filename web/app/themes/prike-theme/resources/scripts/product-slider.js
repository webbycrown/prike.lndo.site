$(function () {
  $('.product-slider').slick({
    mobileFirst: true,
    slidesToShow: 1,
    arrows: false,
    appendDots: $('.single-product-slider-dots'),
    responsive: [
      {
        breakpoint: 1280,
        settings: {
          arrows: true,
          slidesToShow: 4,
          infinite: false,
        },
      },
    ],
  });
});
