$(function () {
  console.log('initialized slick slider for hero');
  $('.hero-slider').slick({
    mobileFirst: true,
    dots: true,
    arrows: false,
    slidesToShow: 1,
    // appendDots: $('.slick-slider-dots-wrapper-mobile'),
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          // appendDots: $('.slick-slider-dots-wrapper'),
        },
      },
    ],
  });
});
