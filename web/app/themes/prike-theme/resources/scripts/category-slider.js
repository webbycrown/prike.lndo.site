$(function () {
  $('.cat-slider').slick({
    mobileFirst: true,
    dots: true,
    arrows: false,
    appendDots: $('.slick-slider-dots-wrapper'),
  });
});
