$(function () {
  $('.discount-slider').slick({
    dots: true,
    arrows: false,
    slidesToShow: 3,
    // centerMode: true,
    // centerPadding: '20px',
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  });
});
