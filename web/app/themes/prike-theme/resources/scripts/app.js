/**
 * External Dependencies
 */
import 'jquery';
import 'slick-carousel/slick/slick.min';
import './footer-nav-menu';
import './hero-slider';
import './product-slider';
import './news-slider';
import './recipes-slider';
import './discount-slider';
import './custom-dropdown';
import './responsive-navigation';

/* General components */
import './general/accordion';
import './filters';

/* Single product page template */
import './single-product/gallery-slider';
import './single-product/sizes-slider';
import './single-product/page';

/* category template*/
import './category-slider';
import './cat-carousel';

// modal
import './modal';

$(() => {});
