$(() => {
  $('#responsive-navigation-toggle').on('click', () => {
    $('.responsive-navigation').toggleClass('hidden');
    $('header').toggleClass('fixed w-full z-30');
    $('.responsive-navigation').toggleClass('pt-20');
  });
});
