<?php

/**
 * Theme helpers.
 */

namespace App;

function sage_woocommerce_loop_start( $loop_start ) {
  $class[] = 'products';
  $class[] = 'columns-' . wc_get_loop_prop( 'columns' );
  $class   = implode( ' ', $class );
  ?>
  <ul class="<?php echo esc_attr( $class ); ?>">
  <?php

}

