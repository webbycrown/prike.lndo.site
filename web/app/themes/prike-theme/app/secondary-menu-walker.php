<?php

namespace App;

class Secondary_Menu_Walker extends \Walker_Nav_Menu
{

  public function start_lvl(&$output, $depth = 0, $args = null)
  {

    if (isset($args->item_spacing) && 'discard' === $args->item_spacing) {
      $t = '';
      $n = '';
    } else {
      $t = "\t";
      $n = "\n";
    }
    $indent = str_repeat($t, $depth);
    $output .= "{$n}{$indent}<ul class=\"hidden xl:block pt-3\">{$n}";
  }

  public function end_lvl(&$output, $depth = 0, $args = null)
  {
    if (isset($args->item_spacing) && 'discard' === $args->item_spacing) {
      $t = '';
      $n = '';
    } else {
      $t = "\t";
      $n = "\n";
    }
    $indent  = str_repeat($t, $depth);
    $output .= "$indent</ul>{$n}";
  }

  public function start_el(&$output, $item, $depth = 0, $args = null, $id = 0)
  {
    $anchor_classes = isset($args->anchor_classes) ? explode(' ', ($args->anchor_classes)) : explode(' ', $args->menu_class);
    $anchor_classes = is_array($anchor_classes) ? $anchor_classes : [];

    if (isset($args->item_spacing) && 'discard' === $args->item_spacing) {
      $t = '';
      $n = '';
    } else {
      $t = "\t";
      $n = "\n";
    }
    $indent = ($depth) ? str_repeat($t, $depth) : '';

    // list item
    $item_classes = [];

    if ($depth == 0) {
      $item_classes = explode(' ', 'footer-nav-menu-item py-5 first:border-t xl:border-none border-line border-b xl:border-0 flex flex-col cursor-pointer xl:py-0 xl:pr-16 xl:last:pr-0');

      $item_classes = implode(' ', array_filter($item_classes, 'trim'));

      // $args = apply_filters( 'nav_menu_item_args', $args, $item, $depth );
      $column_title = !empty($item->title) ? $item->title : '';

      $item_output = $indent . '<li class="' . $item_classes . '">';
      $item_output .= '<div class="flex items-center justify-between w-full">';
      $item_output .= '<h5 class="font-semibold filter drop-shadow-mdx">' . $column_title . '</h5>';
      $item_output .= '<span class="pl-4 xl:hidden"><span class="icon-down text-regular-3 text-xl transition duration-300 inline-block"></span></span>';
      $item_output .= '</div>';
    } elseif ($depth == 1) {
      $item_classes = explode(' ', 'py-4 xl:py-1');
      if ($item->current) {
        $item_classes[] = 'item-active';
      }
      // 
      // $classes = get_post_meta( $item->ID, '_menu_item_classes' );
      // foreach ( $classes[0] as $class ) {
      //   if ( $class ) {
      //     $item_classes[] = $class;
      //   }
      // }

      $item_classes = implode(' ', array_filter($item_classes));
      $output .= $indent . '<li class="' . $item_classes . '">';

      // link attributes
      $atts           = array();
      $atts['title']  = !empty($item->attr_title) ? $item->attr_title : '';
      $atts['target'] = !empty($item->target) ? $item->target : '';
      if ('_blank' === $item->target && empty($item->xfn)) {
        $atts['rel'] = 'noopener noreferrer';
      } else {
        $atts['rel'] = $item->xfn;
      }
      $atts['href']         = !empty($item->url) ? $item->url : '';
      $atts['aria-current'] = $item->current ? 'page' : '';

      if ($item->current) $atts['href'] = '';

      $atts = apply_filters('nav_menu_link_attributes', $atts, $item, $args, $depth);

      $attributes = '';
      foreach ($atts as $attr => $value) {
        if (is_scalar($value) && '' !== $value && false !== $value) {
          $value       = ('href' === $attr) ? esc_url($value) : esc_attr($value);
          $attributes .= ' ' . $attr . '="' . $value . '"';
        }
      }

      $title = apply_filters('the_title', $item->title, $item->ID);
      $title = apply_filters('nav_menu_item_title', $title, $item, $args, $depth);

      // link
      $item_output = $args->before;
      $item_output .= '<a class="' . implode(' ', $anchor_classes) . '"' . $attributes . '>';
      $item_output .= $args->link_before . $title . $args->link_after;
      $item_output .= '</a>';
      $item_output .= $args->after;
    }


    $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
  }

  public function end_el(&$output, $item, $depth = 0, $args = null)
  {
    $block = isset($args->bem_block) ? $args->bem_block : explode(' ', $args->menu_class);
    $block = is_array($block) ? $block[0] : $block;
    if (isset($args->item_spacing) && 'discard' === $args->item_spacing) {
      $t = '';
      $n = '';
    } else {
      $t = "\t";
      $n = "\n";
    }
    $output .= "</li>{$n}";
  }
}
