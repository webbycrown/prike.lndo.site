<?php

namespace App\View\Composers;

use Roots\Acorn\View\Composer;
use Roots\Acorn\View\Composers\Concerns\AcfFields;

class Category extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    use AcfFields;

    protected static $views = [
        '*'
    ];


    public function with()
    {
        return [
            'category_header' => $this->category_before(),
            
        ];
    }

    
    public function category_before()
    {
        if(is_product_category()) {
            global $wp_query;
            $cat = $wp_query->get_queried_object();
            $thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true );
            $image = wp_get_attachment_url( $thumbnail_id );
            $title = single_term_title( '', false );
            return [
                'hero' => isset($thumbnail_id) && '0' !== $thumbnail_id,
                'imgSrc' => $image,
                'imgAlt' => $title,
                'title' => $title,
                'description' => esc_html( $cat->description ),
                'slider' => $this->getAcfRepeaters(),
            ];
        }
      return [
          'hero' => false,
          'slider' => false,
      ];
    }

    public function getAcfRepeaters()
    {
        return collect($this->fields(['slider']));

            // ->map(function($acf_repeater) {
            //     return $acf_repeater->property->value_you_want_to_print;
            // })->toArray();
    }
}
