<?php

namespace App;

class Primary_Menu_Walker extends \Walker_Nav_Menu
{
  
  public function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
  {
    if (isset($args->item_spacing) && 'discard' === $args->item_spacing) {
        $t = '';
        $n = '';
    } else {
        $t = "\t";
        $n = "\n";
    }
    $indent = ($depth) ? str_repeat($t, $depth) : '';

    $classes = empty($item->classes) ? array() : (array) $item->classes;

    /*
    * Initialize some holder variables to store specially handled item
    * wrappers and icons.
    */
    $linkmod_classes = array();
    $icon_classes    = array();
    $anchor_classes = isset($args->anchor_class) ? $args->anchor_class : '';

    // Join any icon classes plucked from $classes into a string.
    $icon_class_string = join(' ', $icon_classes);

    /**
     * Filters the arguments for a single nav menu item.
     *
     *  WP 4.4.0
     *
     * @param stdClass $args  An object of wp_nav_menu() arguments.
     * @param WP_Post  $item  Menu item data object.
     * @param int      $depth Depth of menu item. Used for padding.
     */
    $args = apply_filters('nav_menu_item_args', $args, $item, $depth);

    // Add .dropdown or .active classes where they are needed.


    // if (isset($args->has_children) && $args->has_children) {
    //     $classes[] = 'dropdown';
    // }

    $li_classes = [];
    if (in_array('current-menu-item', $classes, true) || in_array('current-menu-parent', $classes, true)) {
        $classes[] = 'active';
        $li_classes[] = 'item-active';
    }

    // Add some additional default classes to the item.

    // Allow filtering the classes.
    $classes = apply_filters('nav_menu_css_class', array_filter($li_classes), $item, $args, $depth);

    // Form a string of classes in format: class="class_names".
    $class_names = join(' ', $li_classes);
    $class_names = $class_names ? ' class="' . esc_attr($class_names) . '"' : '';

    /**
     * Filters the ID applied to a menu item's list item element.
     *
     * @since WP 3.0.1
     * @since WP 4.1.0 The `$depth` parameter was added.
     *
     * @param string   $menu_id The ID that is applied to the menu item's `<li>` element.
     * @param WP_Post  $item    The current menu item.
     * @param stdClass $args    An object of wp_nav_menu() arguments.
     * @param int      $depth   Depth of menu item. Used for padding.
     */

    // $id = apply_filters('nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args, $depth);
    // $id = $id ? ' id="' . esc_attr($id) . '"' : '';

    $output .= $indent . '<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement"' . $class_names . '>';

    // Initialize array for holding the $atts for the link item.
    $atts = array();

    $atts['title'] = ! empty($item->attr_title) ? $item->attr_title : '';
    $atts['target'] = ! empty($item->target) ? $item->target : '';
    $atts['rel']    = ! empty($item->xfn) ? $item->xfn : '';
    // If the item has children, add atts to the <a>.


    
    $atts['href'] = ! empty($item->url) ? $item->url : '#';
    
    $atts['class'] = $anchor_classes;
    

    $atts['aria-current'] = $item->current ? 'page' : '';

    // Allow filtering of the $atts array before using it.
    $atts = apply_filters('nav_menu_link_attributes', $atts, $item, $args, $depth);

    // Build a string of html containing all the atts for the item.
    $attributes = '';
    foreach ($atts as $attr => $value) {
        if (! empty($value)) {
            $value       = ('href' === $attr) ? esc_url($value) : esc_attr($value);
            $attributes .= ' ' . $attr . '="' . $value . '"';
        }
    }


    // START appending the internal item contents to the output.
    $item_output = isset($args->before) ? $args->before : '';

    $item_output .= '<a' . $attributes . '>';
    

    /*
    * Initiate empty icon var, then if we have a string containing any
    * icon classes form the icon markup with an <i> element. This is
    * output inside of the item before the $title (the link text).
    */
    $icon_html = '';
    
    /** This filter is documented in wp-includes/post-template.php */
    $title = apply_filters('the_title', $item->title, $item->ID);

    /**
     * Filters a menu item's title.
     *
     * @since WP 4.4.0
     *
     * @param string   $title The menu item's title.
     * @param WP_Post  $item  The current menu item.
     * @param stdClass $args  An object of wp_nav_menu() arguments.
     * @param int      $depth Depth of menu item. Used for padding.
     */
    $title = apply_filters('nav_menu_item_title', $title, $item, $args, $depth);

    // If the .sr-only class was set apply to the nav items text only.
    if (in_array('sr-only', $linkmod_classes, true)) {
        $title         = self::wrap_for_screen_reader($title);
        $keys_to_unset = array_keys($linkmod_classes, 'sr-only', true);
        foreach ($keys_to_unset as $k) {
            unset($linkmod_classes[ $k ]);
        }
    }

    // Put the item contents into $output.
    $item_output .= isset($args->link_before) ? $args->link_before . $title . $args->link_after : '';
    $item_output .= '</a>';
    

    $item_output .= isset($args->after) ? $args->after : '';

    // END appending the internal item contents to the output.
    $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
  }



  
  /**
    * Wraps the passed text in a screen reader only class.
    *
    * @since 4.0.0
    *
    * @param string $text the string of text to be wrapped in a screen reader class.
    * @return string      the string wrapped in a span with the class.
    */
  private function wrap_for_screen_reader($text = '')
  {
      if ($text) {
          $text = '<span class="sr-only">' . $text . '</span>';
      }
      return $text;
  }


}

